Program verification with types and logic 2023–2024 (NWI-IMC060)
----------------------------------------------------------------

Welcome to the course "Program verification with types and logic".
This repository will be used to distribute the course material.
If you find bugs, feel free to open an [**issue**](https://gitlab.science.ru.nl/program-verification/course-2023-2024/-/issues).

More material will be added as the course progresses.
Check out a local clone of this repository and make sure you pull any remote changes before you start working on the Coq exercise for any week (especially the [library](coq/library.v) file is subject to change).

## Communication during the course

- We will use Brightspace only for sending announcements—so make that you are enrolled.

## Grading

The final grade is based on the following parts:

- A project in the Coq proof assistant.
- A written exam.

Both parts have a weight of 50%, and the grade for both parts should be at least a 5.

## Homework

For each week you are supposed to work on the following homework:

- **Coq or Rust exercises**.
  For each week, there are programming exercises that you should do in Coq or Rust.
  You should work on these exercises at home and during the practical sessions.
  During the practical sessions the teachers can help you and provide feedback.
  You do not need to submit these exercises and they are not graded.
  It is essential that you do these exercises seriously otherwise you will not be prepared for the Coq projects (which are graded)!
- **Theory exercises**.
  For each week (with the exception of week 1), there are theory exercises similar to those on the exam.
  If you submit the exercises (of week `n`) during the practical (of week `n`) or at the next lecture (of week `S n`), we will provide you with feedback at the next practical (of week `S n`).
  To submit the exercises, you need to give a printed or handwritten sheet of paper to the teachers.
  You will then receive back an annotated version.
  Submitting exercises is optional but crucial for the preparation for the exam!

## Coq project

Project descriptions: [part 1](coq/project1.v) [[part2]](coq/project2.md)

- The Coq project consists of two parts:
  + The deadline for **Part 1** is **29 March 2024, 23:59**, and it counts for 30% of your project grade.
  + The deadline for **Part 2** is **21 June 2024, 23:59**, and it counts for 70% of your project grade.
- For the first part, you have to hand in a Coq file.
- For the second part, you have to hand in a Coq file and a short report (of 5–7 pages).
- You should select your Coq project out of a list of project options we will provide.
- You should do the project in **a group of 2 students** and submit your material together.
- The report should include a 1/2 page reflection section written by each student individually (so, 1 page in total).
  This section should describe your contribution to the project and a short reflection on the collaboration.

## Exam

The exam is **closed book** and will cover the theoretical parts of this course.

- Exam 2021-2022 [[pdf]](exams/2021_2022.pdf) [[pdf with answers]](exams/2021_2022_answers.pdf) (the Iris concurrency exercise is no longer part of the course material)
- Exam 2022-2023 [[pdf]](exams/2022_2023.pdf) [[pdf with answers]](exams/2022_2023_answers.pdf)

## Course material and course outline

**Lecture material:**
The Coq files are written in the style of literate programming, which means that they contain both the code and the lecture material in comments.
Make sure to read the comments carefully!

**Coq installation and cheatsheet:**
Make sure to read [our instructions to install Coq](coq/README.md) and download our [Coq cheat sheet](coq/cheatsheet.pdf) with an overview of the tactics and commands that are used in this course.

**About line counts:**
The Coq files contain the number of lines of code for the teachers' solutions.
These line counts give an indication of the difficulty of the exercise; they are **not** intended to be a requirement for a good solution.
Our proofs generally contain multiple tactics on a single line and use introduction patterns extensively.
If your solution is an order of magnitude longer than the reference solution, it is good to ask for improvements during the practical sessions.

_(The schedule is preliminary)_

- **Week 1** (29 Jan–2 Feb): Introduction to Coq (part 1)
  [[coq]](coq/week1.v) [[coq challenges]](coq/challenges.v)
- **Week 2** (5 Feb–9 Feb): Introduction to Coq (part 2)
  [[coq]](coq/week2.v) [[coq challenges]](coq/challenges.v) [[theory (shared with week 3)]](theory/week2.pdf) [[theory template]](theory/week2_template.tex)
- **Week 3** (12 Feb–16 Feb): Equality, finite maps, finite sets, and higher-order functions in Coq
  [[coq]](coq/week3.v) [[coq challenges]](coq/challenges.v) [[theory (shared with week 2)]](theory/week2.pdf) [[theory template]](theory/week2_template.tex)\
  (*Monday is a holiday, so the lecture is on Wednesday and the practical session is on Friday*)
- **Week 4** (19 Feb–23 Feb): Basic operational semantics
  [[coq]](coq/week4.v) [[theory]](theory/week4.pdf) [[theory template]](theory/week4_template.tex)
- **Week 5** (26 Feb–1 Mar): Type systems
  [[coq]](coq/week5.v) [[theory]](theory/week5.pdf) [[theory template]](theory/week5_template.tex)
- **Week 6** (4 Mar–8 Mar): Type safety for languages with mutable references
  [[coq]](coq/week6.v) [[theory]](theory/week6.pdf) [[theory template]](theory/week6_template.tex)
- **Week 7** (11 Mar–15 Mar): Programming in Rust
  [[installation instructions]](rust/README.md)
  [[rust]](rust/src/week7.rs) [[theory]](theory/week7.pdf) [[theory template]](theory/week7_template.tex)

Exam period (20 Mar–7 Apr): No lectures and practical sessions

**[Project Part 1](coq/project1.v) deadline at 29 March, 23:59**

- **Week 8** (8 Apr–12 Apr): Concurrency in Rust
  [[rust]](rust/src/week8.rs) [[theory]](theory/week8.pdf) [[theory template]](theory/week8_template.tex)
- **Week 9** (15 Apr–19 Apr): Separation logic (propositions)
  [[lecture notes]](lectures/week9.md) [[coq]](coq/week9.v) [[theory]](theory/week9.pdf) [[theory template]](theory/week9_template.tex)
- **Week 10** (22 Apr–26 Apr): Separation logic (Hoare triples and weakest preconditions)
  [[lecture notes]](lectures/week10.md) [[coq]](coq/week10.v) [[theory (shared with week 11)]](theory/week10.pdf) [[theory template]](theory/week10_template.tex)

May holiday (29 Apr–3 May): No lectures and practical sessions

- **Week 11** (6 May–10 May): Separation logic (interactive proofs)
  [[coq]](coq/week11.v) [[theory (shared with week 10)]](theory/week10.pdf) [[theory template]](theory/week10_template.tex)
- **Week 12** (13 May–17 May): Semantic substructural typing
  [[lecture notes]](lectures/week12.md) [[coq]](coq/week12.v) [[theory]](theory/week12.pdf) [[theory template]](theory/week12_template.tex)

Optional lecture at **17 May, 10:30-12:15, Mercator 1.13** about concurrent separation logic

- **Week 13** (20 May–24 May): Work on final Coq projects\
  (*Monday is a holiday, so the practical sessions take place on Wednesday and Friday*)
- **Week 14** (27 May–31 May): Work on final Coq projects
- **Week 15** (3 Jun–7 Jun): Work on final Coq projects\
  (*Virtual practical sessions, no physical practical sessions because the teachers are abroad for a conference*)

Exam period (10 Jun–28 Jun): No lectures and practical sessions

**[Project Part 2](coq/project2.md) deadline at 21 June, 23:59**

## LaTeX

If you would like to typeset your project report in LaTeX, you could use the following packages:

- https://ctan.org/pkg/bussproofs or http://cristal.inria.fr/~remy/latex/mathpartir.html \
  For typesetting proofs, rules and derivation trees in natural deduction style
- https://www.mathstat.dal.ca/~selinger/fitch/ \
  For typesetting proofs in Fitch/flag-style
- https://gitlab.mpi-sws.org/iris/iris/-/blob/master/tex/iris.sty \
  For miscellaneous macros to write statements in separation logic
- On the internet you can find various Coq styles for the `listings` package.
  For example [the version part of the mathematical components book](https://github.com/math-comp/mcb/blob/master/tex/defManSSR.tex) or [a modified version on stackexchange](https://tex.stackexchange.com/a/620012).

\documentclass[11pt,twoside]{exam}
\usepackage{macros}
\printanswers

\begin{document}
\renewcommand{\thisweek}{8}
\maketitle

\begin{questions}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\question
To guarantee safe concurrency, Rust uses the following traits:
\begin{itemize}
\item A type is \lstinline{Send} if it is safe to send it to another thread.
\item A type is \lstinline{Sync} if it is safe to share between threads (\lstinline{T} is \lstinline{Sync} if and only if \lstinline{&T} is \lstinline{Send}).
\end{itemize}
This exercise is about the \lstinline{Send} and \lstinline{Sync} traits.

\begin{parts}
\part[4] Give an example of a type is both \lstinline{Send} and \lstinline{Sync}, and explain why.
  \begin{solution}
  YOUR SOLUTION HERE
  \end{solution}
\part[4] Give an example of a type is neither \lstinline{Send} nor \lstinline{Sync}, and explain why.
  \begin{solution}
  YOUR SOLUTION HERE
  \end{solution}
\part[4] Give an example of a type is \lstinline{Send}, but not \lstinline{Sync}, and explain why.
  \begin{solution}
  YOUR SOLUTION HERE
  \end{solution}
\end{parts}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\question
\label{ex:spawn}

We consider the semantics of a simply-typed language with $\langkw{spawn}$ and $\langkw{join}$ similar to those operations in Rust.
The formal syntax of our language is as follows:
%
\begin{samepage}
\begin{align*}
e \in \Expr \bnfdef{}& x \mid n \mid \tid \mid \Lam x. e \mid e_1\; e_2 \mid \Spawn e \mid \Joinn e \\
A \in \Type \bnfdef{}& \Tnat \mid A_1 \to A_2 \mid \JoinHandle A
\end{align*}
We let variables $x \in \mathsf{string}$, numerals $n \in \nat$, and thread IDs $\tid \in \nat$.
\end{samepage}

Aside from the usual typing rules of the simply-typed lambda calculus with natural numbers, the type system has the following typing rules for $\langkw{spawn}$ and $\langkw{join}$:
%
\begin{gather*}
\AXC{$\typed \Gamma e A$}
\UIC{$\typed \Gamma {\Spawn e} {\JoinHandle A}$}
\normalAlignProof
\DisplayProof
\qquad
\AXC{$\typed \Gamma e {\JoinHandle A}$}
\UIC{$\typed \Gamma {\Joinn e} A$}
\normalAlignProof
\DisplayProof
\end{gather*}

The intuitive semantics is that $\Spawn e$ creates new thread that runs expression $e$, and returns the thread's join handle (represented as a thread ID).
The construct $\Joinn e$ will wait for the spawned thread $e$ to terminate.
Since the type system is unrestricted (\ie not substructural), one can use a join handler zero or multiple times, possibly in different threads.
The idea is that each $\langkw{join}$ will spin until the associated thread has terminated, and then obtains the return value of that thread.

We let $\Val \subseteq \Expr$ denote the subset of expressions that are values, \ie numerals $n$, thread IDs $\tid$, and functions $\Lam x. e$.
Configurations $\cfg$ are represented as lists of threads $e_1 \ldots e_n$.
The main thread is the first element in the configuration, followed by all other threads in the order they were spawned.
Threads that have terminated (\ie have reduced to a value) are kept in the configuration.
The small-step reduction $\cfg \step[i] \cfg'$ says that thread $i$ in configuration $\cfg$ can step to configuration $\cfg'$:
%
\begin{gather*}
\AXC{$\cfg(i) = k\; ((\Lam x. e)\; v)$}
\AXC{$\ctx k$}
\BIC{\strut$\cfg \step[i] \linsert i {k\; (\subst x v e)} \cfg$}
\normalAlignProof
\DisplayProof
\qquad
\AXC{$\cfg(i) = k\;(\Spawn e)$}
\AXC{$\tid = \len \cfg$}
\AXC{$\ctx k$}
\TIC{\strut$\cfg \step[i] (\linsert i {k\; \tid} \cfg) \capp [e]$}
\normalAlignProof
\DisplayProof
\\[1em]
\AXC{$\cfg(i) = k\; (\Joinn\; \tid)$}
\AXC{$\cfg(\tid) = e$}
\AXC{$e \notin \Val$}
\AXC{$\ctx k$}
\QIC{\strut$\cfg \step[i] \cfg$}
\normalAlignProof
\DisplayProof
\\[1em]
\AXC{$\cfg(i) = k\;(\Joinn \tid)$}
\AXC{$\cfg(\tid) = v$}
\AXC{$v \in \Val$}
\AXC{$\ctx k$}
\QIC{\strut$\cfg \step[i] \linsert i {k\; v} \cfg$}
\normalAlignProof
\DisplayProof
\end{gather*}
We represent evaluation contexts $k$ as functions from expressions to expressions.
The judgment $\ctx k$ says that $k$ is a valid evaluation context.
We use the notation $\cfg(i)$ to look up the $i$th element of a list (\ie the assertion $\cfg(i) = e$ means that the index $i$ is within bounds of the list $\cfg$, and the value $e$ is stored at position $i$ in the list $\cfg$),
and the notation $\linsert i e \cfg$ to overwrite the $i$th element of the list $\cfg$ with value $e$.

The step relation $\cfg \step \cfg'$ non-deterministically lets a thread take a step:
\begin{gather*}
\AXC{$\cfg \step[i] \cfg'$}
\AXC{$i < \len \cfg$}
\BIC{$\cfg \step \cfg'$}
\normalAlignProof
\DisplayProof
\end{gather*}

Our goal is to prove type safety, that is: If $\typed \emptyset e A$, then $\safe {[e]}$.
Safety is defined as follows:
\[
\safe \cfg \eqdef
  \All \cfg',
    (\cfg \step^* \cfg')
    \to
    \All i < \len{\cfg'}. (\cfg'(i) \in \Val) \lor (\Exists \cfg''. \cfg' \step[i] \cfg'')
\]
Here, $\step^*$ is the reflexive-transitive closure of $\step$.

\begin{parts}
\part[4] \label{q:safe}
  To prove type safety, we need to define a run-time typing judgment $\cfgtyped \cfg A$ for configurations.
  We then prove the following properties of the run-time typing judgment:
  \begin{itemize}
  \item Initialization: $\typed \emptyset e A$ implies $\cfgtyped {[e]} A$
  \item Preservation: $\cfgtyped \cfg A$ and $\cfg \step \cfg'$ implies $\cfgtyped {\cfg'} A$
  \item Progress: $\cfgtyped \cfg A$ implies $\All i < \len\cfg. (\cfg(i) \in \Val) \lor (\Exists \cfg'. \cfg \step[i] \cfg')$
  \end{itemize}
  Explain how these three properties imply type safety.
  \begin{solution}
  YOUR SOLUTION HERE
  \end{solution}
\part[7] To define the run-time typing judgment $\cfgtyped \cfg A$ for configurations,
  we first define a run-time typing judgment $\rtyped \Sigma \Gamma e A$ for expressions.
  Here, $\Sigma$ is a \emph{thread typing} represented as a list containing the types of all threads in the configuration.
  Give all inference rules for the run-time typing judgment $\rtyped \Sigma \Gamma e A$ required to prove type safety.
  \begin{solution}
  YOUR SOLUTION HERE
  \end{solution}
\part[6] Give a definition of the run-time typing judgment $\cfgtyped \cfg A$ for configurations
  and briefly indicate why the three properties in \Cref{q:safe} hold.
  You do not need to give a formal proof of these properties.
  \begin{solution}
  YOUR SOLUTION HERE
  \end{solution}
\part[3]
  \label{ex:spawn:deadlock}
  Our definition of type safety does not rule out configurations that are deadlocked
  \ie configurations consisting of threads that spin indefinitely long via $\langkw{join}$ to wait on each other.
  Give an example of a configuration $\cfg$ that is deadlocked, but for which $\safe \cfg$ holds.
  Explain your answer.
  \begin{solution}
  YOUR SOLUTION HERE
  \end{solution}
\part[3] Does the type system rule out deadlocks?
  If yes, give an intuition why.
  If not, give an example of a program that is well-typed, but that deadlocks.
  \begin{solution}
  YOUR SOLUTION HERE
  \end{solution}
\end{parts}

\end{questions}
\end{document}


// This file exists to tell Cargo which files to build.
// We do this by importing week7 and/or week8 here.

// To make sure you get the right messages when running `cargo build` or
// `cargo test`, you should comment/uncomment the following lines to build/test
// the correct week. In VSCode, you can also click the "run" or "run test"
// buttons next to the functions in the files you import below (i.e., then you
// do not have to comment/uncomment a line).

mod week7;
mod week8;

// If your editor is set-up properly, you should be able to click "run" below.
fn main() {
  println!("Hello, world!");
}

// Similarly, you should be able to click "run test" on functions marked #[test].
#[test]
fn foo() {
  println!("Hello, world!");
}

Project 2: Table of contents
=============================

[[_TOC_]]

Introduction
============

**IMPORTANT:**
In the homework we have mostly practiced with proving lemmas that are given by the teachers, and practiced less with providing your own definitions.
The project is different in that regard: an essential learning objective is to come up with your own definitions.
It is likely that the first versions of your definitions are wrong and you thus get stuck in proofs, so make sure you **come to the practical sessions** and ask the teachers for feedback!

**Groups:**
You should do the project in groups of 2 students.
Preferably, it is the same group as for [the first project](project1.v), but this is not necessary.

**Deliverables:**
To complete the Coq project, you need to hand in (via Brightspace, before the deadline):

- A single Coq file with your solution.
  In this file you can `Require Import` any file from the course repository (in particular, `library.v`, `proofmode.v`, `weekX.v`), the Coq standard library, std++ or Iris.
- A single PDF file with your report.

**Report:**
The written report has to be 5 to 7 pages, should be in academic style (preferably typeset using LaTeX), written in English, and should include:

1. Student names, student numbers, and project title at the first page.
2. A small introduction with at least a description of the project you have chosen.
   You should not just copy the description from this document.
3. A description of the problems and design choices you have encountered during the project and how you have solved these.
   If you had multiple solutions or designs in mind, you should describe the trade-offs, and explain why you have picked the solution or design that you have used.
4. A conclusion with at least your experience using Coq (and, if applicable, Iris): what you did and did not like, what features you think are missing in Coq or Iris, what would improve Coq or Iris, etc.
5. A 1/2 page reflection section written by each student individually (so, 1 page in total).
   This section should describe your contribution to the project and a short reflection about the collaboration.

You should not include unnecessarily long excerpts of Coq code in the report, but only use small code excerpts to support your text.

**Grading:**
The grade of the project is based on the following criteria:

1. _Difficulty_: Depending on the difficulty of your project, you can get a higher grade.
   If you only do the basic part of the project (unless you do the very difficult step-indexing project), your maximal grade is a **7**.
   You should do at least one extension if you want to be eligible for a grade that is higher than 7.
   (Note that doing a difficult project does not guarantee a high grade, you should also score well on the other criteria.)
2. _Correctness_: Whether the Coq definitions and lemmas correctly model the problem at hand.
3. _Completeness_: Whether all lemmas are proven (i.e., no lemmas are omitted and no proofs are finished with `admit` or `Admitted`).
4. _Style_: Whether the Coq code follows sensible style guidelines (consistent indentation, proper use of parentheses, sensible variable names, proper use of implicit arguments, documentation where needed, etc.).
5. _Effectiveness_: Whether the definitions and proofs are carried out effectively (no redundant proof steps, no use of the induction tactic for lemmas that can be proved without induction).
6. _Report_: Style, presentation, language and correctness of the report.

Resources about Iris
====================

If you plan to do a project that involves concurrency in Iris or Iris's HeapLang language (e.g., option 2/extension 4, or option 4), it is recommended to read the following resources:

- https://gitlab.mpi-sws.org/iris/tutorial-popl21 \
  A small tutorial about verifying concurrent programs in Iris
- https://iris-project.org/tutorial-material.html \
  The lecture notes on Iris. These lecture notes contain corresponding examples in Coq.
- https://gitlab.mpi-sws.org/iris/iris/-/blob/master/docs/resource_algebras.md \
  The Iris documentation on how to setup your definitions for ghost state.
- https://gitlab.mpi-sws.org/iris/iris/-/blob/master/docs/heap_lang.md \
  The Iris documentation on HeapLang

Project option 1: Verification of search trees
===============================================

_The goal of this project is to verify binary search trees using separation logic._

You are allowed to use the language, separation logic and tactics from [week 11](coq/week11.v) or the HeapLang language of Iris.
In the project description, we assume you use the course's language, but all Coq code can easily be ported to HeapLang.

## Basic part

Binary search trees are commonly used to implement finite maps (i.e., dictionaries).
We consider the following implementation of the imperative functions `tree_lookup` and `tree_insert` in our deeply embedded language:

```coq
Definition tree_lookup := recclosure: "rec" "t" "n" =>
  match: !"t" with
    InjL "_" => EInjL VUnit
  | InjR "node" =>
     let: "m" := EFst (EFst "node") in
     if: "m" =: "n" then
       EInjR (ESnd (EFst "node"))
     else if: "n" <: "m" then
       "rec" (EFst (ESnd "node")) "n"
     else
       "rec" (ESnd (ESnd "node")) "n"
  end.

Definition tree_insert := recclosure: "rec" "t" "n" "x" =>
  match: !"t" with
    InjL "_" =>
     let: "left" := EAlloc (EInjL VUnit) in
     let: "right" := EAlloc (EInjL VUnit) in
     "t" <- EInjR (EPair (EPair "n" "x") (EPair "left" "right"));;
     VUnit
  | InjR "node" =>
     let: "m" := EFst (EFst "node") in
     if: "m" =: "n" then
       "t" <- EInjR (EPair (EPair "m" "x") (ESnd "node"));;
       VUnit
     else if: "n" <: "m" then
       "rec" (EFst (ESnd "node")) "n" "x"
     else
       "rec" (ESnd (ESnd "node")) "n" "x"
  end.
```

To handle failure (the key `n` is not in the domain of the tree), the function `tree_lookup` can return `None` (encoded as `VInjL VUnit`) or `Some v` (encoded as `VInjR v`).
Formally, options are encoded in our language as:

```coq
Definition option_to_val (o : option val) : val :=
  match o with None => VInjL VUnit | Some v => VInjR v end.
```

You will verify that the lookup and insertion functions correspond to the operations on finite maps in Coq by proving the following Hoare triples:

```coq
Lemma tree_lookup_hoare l m n :
  {{ is_search_tree l m }}
    tree_lookup (VRef l) (VNat n)
  {{ vret, @[ vret = option_to_val (NatMap.lookup m n) ] ** is_search_tree l m }}.

Lemma tree_insert_hoare l m n (v : val) :
  {{ is_search_tree l m }}
    tree_insert (VRef l) (VNat n) v
  {{ vret, @[ vret = VUnit ] ** is_search_tree l (NatMap.insert n v m) }}.
```

The key ingredient of the verification is defining the representation predicate:

```coq
Definition is_search_tree (l : loc) (m : natmap val) : sepProp
```
This representation predicate says that at location `l` there is a linked binary search tree representing the finite map `m`.

## Suggestions for basic part

**Step 1.** Start by implementing a purely functional version of binary search trees in Coq.
For these steps you do not need to use separation logic, just ordinary Coq.

- Define an inductive type `tree` for trees whose nodes are key/value pairs in Coq
- Define a function `tree_to_map : tree -> natmap val` that converts a tree into a finite map.
- Define a predicate `search_tree : tree -> Prop` that describes the valid search trees.
  It is recommended to use an `Inductive`, but a `Fixpoint` also works.
  _Hint_: You might need to define some auxiliary predicates, the function `tree_to_map` might be useful for those.
  (For the basic part, you do **not** need to consider balanced search trees; just regular search trees.)
- Define Coq functions `tree_lookup_coq : tree -> nat -> option val` and `tree_insert_coq : tree -> nat -> val -> tree`.

Prove that `tree_lookup_coq` and `tree_insert_coq` behave in accordance with the finite map operations `NatMap.lookup` and `NatMat.insert`.
The first lemma is as follows, the second you have to state yourself:

```coq
Lemma tree_lookup_coq_correct t n :
  search_tree t ->
  tree_lookup_coq t n = NatMap.lookup (tree_to_map t) n.
```

Prove that `tree_insert_coq` preserves the `search_tree` property.

```coq
Lemma tree_insert_coq_search_tree t n v :
  search_tree t ->
  search_tree (tree_insert_coq t n v).
```

**Step 2.** Verify the imperative version of binary search trees using separation logic.

- Define a representation predicate `is_tree : loc -> tree -> sepProp` that describes that a tree is laid out in memory.
  You need to use sums (`VInjL` and `VInjR`) and products (`VPair`) to represent the constructors of the tree.
- Define the representation predicate `is_search_tree : loc -> natmap val -> sepProp` by means of `is_tree`, `search_tree`, and `tree_to_map`.
- Use the lemmas of your pure Coq functions `tree_lookup_coq` and `tree_lookup_coq` in the proofs of `tree_lookup_hoare` and `tree_insert_hoare`.

## Extensions

1. The values are currently directly placed in the nodes of the tree.
   Make a version where there is a pointer indirection for each value, and adapt `tree_lookup` so that you obtain a pointer to the value, instead of the value itself.
   Adapt the specifications and proofs so that you can use the resulting pointer of `tree_lookup` to modify the value.
   (See `ptr_list_lookup_hoare` in [week 11](coq/week11.v).)
2. Add, specify, and verify a function `tree_max` that returns the maximal key in the tree.
3. Implement a `tree_delete` operation, and prove a Hoare triple involving `NatMap.delete`.
4. Extend your development to balanced search trees so that all tree operations have `O(log n)` worst-case running time complexity (where `n` is the size of the tree/map).
  We recommend to consider [AVL-trees](https://en.wikipedia.org/wiki/AVL_tree), because other tree representations as red-black trees quickly result in many cases in your definitions and proofs.
  You need to extend your representation of trees so that nodes store their depth or balance factor.


Project option 2: Verification of a linked list
===============================================

_The goal of this project is to verify the stack and queue operations on a linked list._

You are allowed to use the language, separation logic and tactics from [week 11](coq/week11.v) or the HeapLang language of Iris.
In the project description, we assume you use the course's language, but all Coq code can easily be ported to HeapLang.

## Basic part

Compared to the version in [week 11](week11.v), the lists in this project have a tail pointer, allowing to push an element to the back in `O(1)` time.
Nodes of the linked list can either be `VInjL VUNit` for the empty list, or `VInjR (VPair v (VRef l))` where `v` is the node's element and `l` is a reference to the next node.
For example, a singly linked list with elements `v1` and `v2` is represented as:

```
┌──────────────────────────┐    ┌──────────────────────────┐    ┌─────────────┐
│ VInjR (VPair v1 (VRef ⋅)-│--> │ VInjR (VPair v2 (VRef ⋅)-│--> │ VInjL VUNit │
└──────────────────────────┘    └──────────────────────────┘    └─────────────┘
^                                                                ^
|                                                                |
hd                                                               tl
```

We consider the functions `new_list` to create a new singly linked list, `push_front` to push an element to the front, `pop_front` to pop an element from the front, `push_back` to push an element to the back:

```coq
Definition new_list := closure: "_" =>
  let: "node" := EAlloc (EInjL VUnit) in
  let: "hd" := EAlloc "node" in
  let: "tl" := EAlloc "node" in
  EPair "hd" "tl".

Definition push_front := closure: "l" "x" =>
  let: "hd" := EFst "l" in
  let: "node" := EAlloc (EInjR (EPair "x" !"hd")) in
  "hd" <- "node";;
  VUnit.

Definition pop_front := closure: "l" =>
  let: "hd" := EFst "l" in
  match: ! !"hd" with
    InjL "_" => EInjL VUnit
  | InjR "node" =>
     EFree (!"hd");;
     "hd" <- ESnd "node";;
     EInjR (EFst "node")
  end.

Definition push_back := closure: "l" "x" =>
  let: "tl" := ESnd "l" in
  let: "node" := EAlloc (EInjL VUnit) in
  !"tl" <- EInjR (EPair "x" "node");;
  "tl" <- "node";;
  VUnit.
```

To handle failure (the list is empty), the function `pop_front` can return `None` (encoded as `EInjL VUnit`) or `Some v` (encoded as `EInjR v`).

Your goal is to prove the following Hoare triples:

```coq
Lemma new_list_hoare :
  {{ EMP }}
    new_list VUnit
  {{ vret, is_list vret [] }}.

Lemma push_front_hoare l vs (v : val) :
  {{ is_list l vs }}
    push_front l v
  {{ vret, @[ vret = VUnit ] ** is_list l (v :: vs) }}.

Lemma pop_front_hoare_nil l :
  {{ is_list l [] }}
    pop_front l
  {{ vret, @[ vret = VInjL VUnit ] ** is_list l [] }}.

Lemma pop_front_hoare_cons l vs v :
  {{ is_list l (v :: vs) }}
    pop_front l
  {{ vret, @[ vret = VInjR v ] ** is_list l vs }}.

Lemma push_back_hoare l vs (v : val) :
  {{ is_list l vs }}
    push_back l v
  {{ vret, @[ vret = VUnit ] ** is_list l (vs ++ [v]) }}.
```

The key ingredient of the verification is defining the representation predicate:

```coq
Definition is_list (l : val) (vs : list val) : sepProp :=
```

This representation predicate says the pair of head/tail pointers `l` represents a linked list with values `vs`.

## Suggestions for basic part

To define the `is_list` representation predicate, it is recommend to first define:

```coq
Fixpoint is_list_nodes (l1 l2 : loc) (vs : list val) : sepProp
```

This predicate says that between a head reference `l1` and tail reference `l2` there are nodes with values `vs`.

## Extensions

1. Adapt the data structure so that you can add a function `list_length` that computes the length of the list in `O(1)` time.
   Implement `list_length`, adapt `is_list`, adapt all other proofs, give `list_length` a suitable specification as a Hoare triple, and prove it.
2. Implement a function `pop_back` that pops an element from the back of the list.
   Since the list is not doubly linked, you need to define a recursive helper function that traverses the list.
   Give `pop_back` a suitable specification as a Hoare triple, and prove it.
3. Define a doubly-linked list so that `pop_back` has `O(1)` running-time.
   Adapt `is_list` and all proofs.
4. Our implementation of the singly linked list data structure is not thread safe.
   We can create a coarse-grained thread-safe concurrent stack by wrapping the data structure into a lock.
   Use the [spin lock library](https://gitlab.mpi-sws.org/iris/iris/-/blob/master/iris_heap_lang/lib/spin_lock.v) that comes with Iris's HeapLang.
   See also [Resources about Iris](#resources-about-iris).


Project option 3: Programming languages with exceptions
========================================================

_The goal of this project is to formalize the semantics of a language with ML/Java-style exceptions, and develop a separation logic for this language._

Use the language from [week 10](coq/week10.v) as a starting point, and extend it with the constructs `try .. catch` and `throw`:

```coq
EThrow : expr -> expr
ETryCatch : expr -> expr -> expr
```

The intuitive semantics is that `throw` throws an exception, which bubbles up, and can be caught and handled using `try .. catch`.
Some examples:

- `try (10 == 10) catch (λ e, e == 2)` results in `true`.
- `try (throw 8) catch (λ e, e == 2)` results in `false`.
- `try (10 + throw 8) catch (λ e, e == 2)` results in `false`.
- `try (throw 2 + throw 8) catch (λ e, e == 2)` results in `true` because evaluation is left-to-right, so the first `throw` is executed first.
- `try (throw (throw 2)) catch (λ e, e == 2)` results in `true` because a `throw` can bubble up over another `throw`.
- `try ((try (throw 8; λ x, x) catch h1) x1) catch h2` results in the `throw` being handled by the inner handler `h1`.
- `try ((try (λ x, throw x) catch h1) x1) h2` results in the `throw` being handled by the outer handler `h2`.
- `let l = alloc 0 in try (l <- 10; throw 8) catch (λ e, e); !l` results in `10` because `throw` does not roll back modifications to the heap.
- `try (try (throw 2) catch (λ e, throw (e + 3))) (λ e, e)` results in `5` because the exception handler throws `2 + 3`.

## Big-step operational semantics

Extend the big-step semantics with rules for `throw` and `try .. catch`, and for bubbling up exceptions,
and show that the examples above are correctly handled by your semantics.
Think carefully how to modify the type of `big_step` to account for ordinary and exceptional return.
You should also think carefully about the pattern of the rules.
Once you figured out the pattern, you should be able to generalize it to all constructs of your language.
You will likely end up with many big-step rules (our solution contains about 35 rules in total).
Do not worry about the number of rules, you never need to do induction over the big-step semantics, and `eauto` is pretty good.

If you are unsure about the definition of your semantics, it might be good to write some unit tests (see [week 4](coq/week4.v) how to do that).

## Separation logic

You need to modify the weakest precondition connective with an **exceptional postcondition** `EPhi`:

```coq
Definition wp (e : expr) (Phi EPhi : val -> sepProp) : sepProp
```

The purpose of the two postconditions becomes most evident from the rules for "ordinary" and "exceptional" return.
In case of ordinary return, you have to establish the ordinary postcondition `Phi`:

```coq
Lemma Val_wp Phi EPhi v :
  Phi v |~ wp (EVal v) Phi EPhi.
```

In case of a `throw`, you have to establish the exceptional postcondition:

```coq
Lemma Throw_wp Phi EPhi e : (* Primitive rule *)
  wp e EPhi EPhi |~ wp (EThrow e) Phi EPhi.

Lemma Throw_Val_wp Phi EPhi v : (* Can be derived from [Val_wp] and [Throw_wp] *)
  EPhi v |~ wp (EThrow (EVal v)) Phi EPhi.
```

The rule for catching an exception is:

```coq
Lemma TryCatch_wp Phi EPhi e1 e2 :
  wp e1 Phi (fun w => wp (EApp e2 (EVal w)) Phi EPhi)
  |~ wp (ETryCatch e1 e2) Phi EPhi.
```

This rule changes the exceptional postcondition to contain a weakest precondition for the handler.

## Proof rules

Prove the rules for `try .. catch` and `throw`.
You also need to adapt all other proof rules for weakest preconditions and reprove them.
Once you figured out the general pattern, most of the rules are relatively simple adaptation of the original rules, but some rules (e.g., the framing and context rule) require some more thoughts.

**You only need to adapt the WP rules, not the Hoare rules. You are of course allowed to do so to obtain additional confidence in your WP rules**

## Extensions

1. Most real-world languages have tagged exceptions, allowing one to `throw` exceptions with a tag (say, `IndexOutOfBoundsException`) and then only `catch` exceptions with a specific tag.
   Extend your language, big-step semantics, and separation logic with tagged exceptions.
2. Use your separation logic to verify a recursive program.
   For example, you can verify a program `f` that recursively goes through a linked list, and uses `throw` to jump out in an exceptional case (e.g., element not found, out of bounds, ..)
   Make sure that you also verify a program `g` that uses `f` and catches the exception.
3. Extend the linear semantic typing system in [week 12](coq/week12.v).
   The crux of this part is to extend the typing judgment and function types to account for exceptions, and to come up with suitable typing rules for `try .. catch` and `throw`.

For the last two extensions, you need to adapt the [proofmode file](coq/proofmode.v) so that you can use the Iris Proof Mode tactics.
In order to do that, we recommend you to talk to the teachers.


Project option 4: Verification of channels in Iris (difficult)
==============================================================

**Disclaimer: To do this project you need to familiarize yourself with concurrency in Iris, see [Resources about Iris](#resources-about-iris). There will also be an _optional_ lecture about concurrency in Iris, see course schedule on Gitlab.**

_The goal of this project is to use Iris to verify concurrent programs._

## Unidirectional one-shot channel

The first implementation we consider is a unidirectional one-shot channel (written in Iris's HeapLang language):

```coq
Definition newchan : val := λ: <>, ref NONE.
Definition send : val := λ: "l" "v", "l" <- SOME "v".
Definition recv : val :=
  rec: "recv" "l" :=
    match: !"l" with
      NONE => "recv" "l"
    | SOME "v" => Free "l";; "v"
    end.
```

This channel is similar to the one we have seen in the Rust exercises of [week 8](rust/week8.rs), but does not use a mutex, but is lock free.
The function `newchan` creates a new channel, which can be used by two parties.
One party can `send` a value, and the other can `recv` a value.
The `recv` will block until a value has been sent.

An example of a program that uses the channel is:

```coq
Definition example : val := λ: "x",
  let: "c" := newchan #() in
  Fork (let: "k" := ref "x" in send "c" "k");;
  !(recv "c").
```

In this example, we create a new channel, over which the forked-off thread sends a reference to the other.
The main thread receives the location and reads from it.
You should be able to prove the following specification for this example:

```coq
Lemma example_spec (v : val) :
  {{{ True }}} example v {{{ RET v; True }}}.
```

To verify examples such as the above, you will prove the following Hoare-style specifications for the channel operations:

```coq
Lemma newchan_spec Φ :
  {{{ True }}} newchan #() {{{ ch, RET ch; sendc ch Φ ∗ recvc ch Φ }}}.

Lemma send_spec ch Φ v :
  {{{ sendc ch Φ ∗ Φ v }}} send ch v {{{ RET #(); True }}}.

Lemma recv_spec ch Φ :
  {{{ recvc ch Φ }}} recv ch {{{ v, RET v; Φ v }}}.
```

Here, `sendc ch Φ` and `recvc ch Φ` are representation predicates for the sending and receiving end of the channel (similar to `Send` and `Recv` in the Rust exercises).
The predicate `Φ` describes the shape and ownership of the message that is being transferred.
You should define these predicates using Iris's mechanisms for invariants and ghost state.

## Bidirectional one-shot channel

The second implementation we consider is a **bidirectional** one-shot channel.
You need to implement functions `newbichan` and `sync` that satisfy the following Hoare-style specifications:

```coq
Lemma newbichan_spec Φ1 Φ2 :
  {{{ True }}}
    newbichan #()
  {{{ ch1 ch2, RET (ch1,ch2); bichanc Φ1 Φ2 ch1 ∗ bichanc Φ2 Φ1 ch2 }}}.

Lemma sync_spec ch Φ1 Φ2 v1 :
  {{{ bichanc Φ1 Φ2 ch ∗ Φ1 v1 }}} sync ch v1 {{{ v2, RET v2; Φ2 v2 }}}.
```

Here, `bichanc Φ1 Φ2 ch` is a representation predicate that says that `ch` is one end of the bidirectional channel.
The predicate `Φ1` describes the shape and ownership of the message that is sent, and `Φ2` dually describes the shape and ownership of the message that is received.

## Suggestions/requirements

- To implement bidirectional one-shot channels, you should make use of unidirectional channels.
- To define the representation predicate `bichanc`, you should make use of the representation predicates `sendc` and `recvc`.
- To verify the Hoare triples, you should make use of the Hoare-style specifications for the unidirectional channel.

## Extensions

Implement a **multi-shot** unidirectional channel.
This implementation should have functions `newmschan`, `mssendc`, and `msrecvc` that satisfy the following Hoare-style specifications:

```coq
Lemma newmschan_spec (Φ : val → iProp Σ) :
  {{{ True }}} newmschan #() {{{ ch, RET ch; mssendc Φ ch ∗ msrecvc Φ ch }}}.

Lemma mssend_spec ch Φ v :
  {{{ mssendc Φ ch ∗ Φ v }}} mssend ch v {{{ ch', RET ch'; mssendc Φ ch' }}}.

Lemma msrecv_spec ch Φ :
  {{{ msrecvc Φ ch }}} msrecv ch {{{ v ch', RET (v,ch'); msrecvc Φ ch' ∗ Φ v }}}.
```

There are many ways to implement a multi-shot channel.

- You can implement your multi-shot channel from your single-shot channel, similar to the way we have done that in the Rust exercises of [week 8](rust/week8.rs).
  To define the representation predicates `mssendc` and `msrecvc`, you will need to use Iris's support for guarded fixpoints.
  Read about guarded recursion in the [Iris lecture notes](https://iris-project.org/tutorial-material.html), and check out [the corresponding file in Coq](https://gitlab.mpi-sws.org/iris/examples/-/blob/master/theories/lecture_notes/lists_guarded.v).
- You can implement your multi-shot channel using a linked list in a mutex, or just a linked list provided you define the queue and enqueue operations in such a way that they are thread-safe.
  Note that there is only one sender and one receiver, so it is possible to make a lock-free implementation without CAS loops.

With a multi-shot unidirectional channel at hand, you can also implement and verify a multi-shot bidirectional channel.


Project option 5: Step-indexed separation logic (difficult)
===========================================================

**Since this project is difficult and large, you can obtain a grade higher than 9 without doing an extension.**

_The goal of this extend separation logic with the "later" modality (▷) for step-indexing to reason about potentially non-terminating programs._

## Motivation

In [week 10](coq/week10.v) until [week 12](coq/week12.v), we considered separation logic for total correctness.
Total correctness means that a closed proof of a Hoare triple guarantees that the program (1) terminates, (2) is safe (i.e., execution does not get stuck), and (3) satisfies the postcondition.
For many applications, total correctness is too strong of a property, and one should rather prove partial correctness.
Partial correctness means that a closed proof of a Hoare triple guarantees that the program (1) is safe and (2) satisfies the postcondition if it terminates.
We list some examples of applications where partial correctness is essential:

- Verification of programs that might not terminate, such as a Turing machine evaluator or an interpreter for a general-purpose programming language.
- Verification of concurrent programs that make use of busy loops (and thus might loop indefinitely with a bad scheduler).
- Semantic models of type systems of general-purpose programming languages like Rust, ML, or Scala.

To reason about partial correctness, we will make a couple of changes to our separation logic:

- To reason about partial correctness, we will use of a **small-step** operational semantics as our basis.
  A big-step operational semantics (that we have used so far) is unsuitable for partial correctness because it cannot distinguish between programs that are unsafe (i.e., get stuck) and that do not terminate.
- To get suitable proof principles for reasoning about potentially non-terminating programs, we will make use of the technique of **step-indexing**.
  Using step-indexing, we extend our logic with new logical connectives and proof principles.

The idea of step-indexing is to add a modality `▷ P`—pronounced "**later** `P`"—to our separation logic.
The later modality `▷ P` says that `P` holds after we have performed at least one step of computation.
To perform induction on the number of program steps, we can use the rule for **Löb induction**:

```
▷ P ⊢ P
-------- Löb induction
True ⊢ P
```

This rule says that to prove `P`, we can assume `▷ P`, where the later acts as a guard to that we cannot use `P` directly (which would be inconsistent).
The later modality can only be removed from the premise after we have used a WP rule that signifies a step of computation.
For example, the rule for recursive functions is:

```
------------------------------------------------------------- AppRec-wp
▷ WP e[x:=v][f:=(rec f x:=e)] { Φ } ⊢ WP (rec f x := e) { Φ }
```

Here, the premise contains a later to signify a computation step took place.

As an example, let us consider the diverging program `diverge := rec f x := f x`.
This program can be derived as follows:


```
  ----------------------------------------------------- ⊢-refl
  WP (diverge ()) { False } ⊢ WP (diverge ()) { False }
--------------------------------------------------------- ▷-mono
▷ WP (diverge ()) { False } ⊢ ▷ WP (diverge ()) { False }
--------------------------------------------------------- AppRec-wp
▷ WP (diverge ()) { False } ⊢ WP (diverge ()) { False }
------------------------------------------------------- Löb induction
                      True  ⊢ WP (diverge ()) { False}
```

We first use Löb induction to obtain an induction hypothesis guarded by a later (▷).
We then use the `AppRec-wp` rule to get a later (▷) in the goal.
Using monotonicity of ▷, we can then remove the later (▷) in both the premise and the conclusion, allowing us to finish the proof.

## Step-indexed separation-logic propositions

To add step-indexing to our separation logic, we change our model of propositions.
Instead of them being predicates over a heap, they become predicates over a heap and a step index:

```coq
Record sepProp (V : Type) : Type := SepProp {
  sepProp_holds : nat -> natmap V -> Prop;
  sepProp_closed n1 n2 h : sepProp_holds n1 h -> n2 <= n1 -> sepProp_holds n2 h;
}.
Arguments SepProp {_}.
Arguments sepProp_holds {_}.
Arguments sepProp_closed {_}.
```

Propositions should be downwards closed over the step-index.
The intuition is that if a program is safe for a number of steps, it should also be safe for a smaller number of steps.

We use a `Record` to couple the predicate with a proof that the proposition is downward closed.
This means that each time we define a proposition (using the constructor `SepProp`) we have the obligation to prove that the proposition is closed.
This can be done most conveniently in Coq using `Program`:

```coq
(** Put at the top of your file, without this declaration, [Program] will
perform magical simplifications to your obligations. *)
Global Obligation Tactic := idtac.

Program Definition sepTrue {V} : sepProp V := SepProp (fun _ _ => True) _.
Next Obligation. (* proof of closedness *) Qed.

Program Definition sepAnd {V} (P Q : sepProp V) : sepProp V :=
  SepProp (fun n h => sepProp_holds P n h /\ sepProp_holds Q n h) _.
Next Obligation. (* proof of closedness *) Qed.
```

By using `Program Definition` instead of `Definition`, we can put an underscore `_` at the place for the second argument of `SepProp` (namely, the proof of closedness).
Then, using `Next Obligation` we can use tactics to provide this proof.

You should adapt all separation logic propositions and proof rules from [week 9](coq/week9.v) to the step-indexed setting.
Most of these changes are relatively simple, but some insight is required for the implication and magic wand.
In addition, you should define the new later modality:

```coq
Program Definition sepLater {V} (P : sepProp V) : sepProp V :=
  SepProp (fun n h => (* Your definition; should do something with the [n] *)) _.
Next Obligation. (* proof of closedness *) Qed.
Notation "|> P" := (sepLater P) (at level 20).
```

For the later modality, you should prove the following rules:

```coq
Lemma sepLater_intro P :
  P |~ |> P.
Lemma sepLater_mono P Q :
  (P |~ Q) ->
  |> P |~ |> Q.
Lemma sepLater_forall {A} (Phi : A -> sepProp V) :
  (All x, |> Phi x) |~ |> (All x, Phi x).
Lemma sepLater_sep_1 P Q :
  |> P ** |> Q |~ |> (P ** Q).
Lemma sepLater_sep_2 P Q :
  |> (P ** Q) |~ |> P ** |> Q.
Lemma loeb_ind P :
  (|> P |~ P) ->
  sepTrue |~ P.
```

**Questions.**
Do we have the rules:

1. `(All x, Phi x) |~ |> (All x, |> Phi x)`
2. `|> (Ex x, Phi x) |~ (Ex x, |> Phi x)`
3. `(Ex x, |> Phi x) |~ |> (Ex x, Phi x)`
4. `(|> P /\\ |> Q) |~ |> (P /\\ Q)` and vice versa.
5. `(|> P \// |> Q) |~ |> (P \// Q)` and vice versa.

For all of these rules, give a counter example if they do not hold.
If they hold, can the rule be derived from the other rules, or does it need to established as a primitive rule?

## Operational semantics

To reason about partial program correctness, we need to use a small-step operational semantics.
You should make a small-step operational semantics for the language in [week 10](coq/week10.v) following the setup in [week 6](coq/week6.v).
That is, define a `pure_step` and `head_step`, and define a `step` using evaluation contexts.

## Step-indexed weakest preconditions

With a model of step-indexed propositions and a small-step operational semantics at hand, we can now define the model of weakest preconditions.
The key idea of the model of weakest preconditions is that `wp e Phi n h` says that `e` is safe for at least `n` steps of computations.

```coq
Fixpoint wp_pre (e : expr) (Phi : val -> sepProp) (n : nat) (h : heap) : Prop :=
  match n with
  | 0 => True
  | S n' =>
     (exists v, e = EVal v /\ sepProp_holds (Phi v) n h) \/
     forall hf,
       disjoint h hf ->
       exists e' h',
         disjoint h' hf /\
         step (e, union h hf) (e', union h' hf) /\
         wp_pre e' Phi n' h'
  end.
Program Definition wp (e : expr) (Phi : val -> sepProp) : sepProp :=
  SepProp (wp_pre e Phi) _.
Next Obligation. (* proof of closedness *) Qed.
```

The definition of WP is recursive on the step index.
If the step index is not zero, we distinguish the cases:

- The expression `e` is a value, then the postcondition `Phi` should hold.
- The expression `e` is not a value, then it should be able to step (with any frame `hf`) and WP should hold for the resulting expression `e'`.

With the definition of WP at hand, you should be able to prove all WP rules from [week 10](week10.v).
For all step-taking rules, you should weaken the precondition to include a later modality.
For example:

```coq
Lemma App_wp Phi x e v :
  |> wp (subst x v e) Phi |~ wp (EApp (EVal (VClosure x e)) (EVal v)) Phi.

Lemma Store_wp Phi l v w :
  (l ~> v ** (l ~> w -** |> Phi (VRef l)))
  |~ wp (EStore (EVal (VRef l)) (EVal w)) Phi.
```

**Hint.** Since we have a notion of a "pure step" in our small-step semantics, you can factor all pure rules into the following rule:

```coq
Lemma wp_pure_step Phi e1 e2 :
  pure_step e1 e2 ->
  |> wp e2 Phi |~ wp e1 Phi.
```

Rules like `App_wp` and `Op_wp` can be derived from this rule (and the rule `Val_wp`).

## Adequacy

Your final objective is to prove the adequacy theorem, which states that if we have a closed proof of a WP, the program is safe.
For the separation logic defined using a big-step semantics, the adequacy theorem (see [week 12](week12.v)) followed almost immediately from the definition of WP,
but for our step-indexed logic it will require considerably more work.

The formal statement you need to prove is:

```coq
Definition safe (phi : val -> Prop) (e : expr) : Prop :=
  forall n e' h',
    steps n (e, empty) (e', h') ->
    (exists v, e' = EVal v /\ phi v /\ h' = empty) \/
    can_step (e',h').

Lemma wp_adequacy phi e :
  (EMP |~ wp e (fun v => @[ phi v ])) ->
  safe phi e.
```

Here, you need the following auxiliary definitions:

```coq
Inductive steps : nat -> cfg -> cfg -> Prop :=
  | steps_refl s : steps 0 s s
  | steps_step n s1 s2 s3 : step s1 s2 -> steps n s2 s3 -> steps (S n) s1 s3.

Definition can_step (s : cfg) := exists s', step s s'.
```

The proof of `wp_adequacy` is non-trivial because WP is defined inductively using the step-index `n`.
For each iteration, we say there **exists** a `step`, whereas `safe` quantifies over all possible steps.
Think about a lemma that you can prove about the language that allows you to bridge this gap.

## Suggestions

To streamline your proofs (especially the proof of adequacy and the context rule), it is advised to define evaluation contexts in two stages.
The first stage `ctx1` is not recursive and does not include constructors for identity and composition.
The second stage `ctx` takes the reflexive-transitive closure.

```coq
Inductive ctx1 : (expr -> expr) -> Prop :=
  | App_l_ctx e : ctx1 (fun x => EApp x e)
  | App_r_ctx v : ctx1 (EApp (EVal v))
  (** etc *).

Inductive ctx : (expr -> expr) -> Prop :=
  | Id_ctx : ctx (fun x => x)
  | Cons_ctx k1 k2 : ctx1 k1 -> ctx k2 -> ctx (fun x => k1 (k2 x)).
```


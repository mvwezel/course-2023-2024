From iris.proofmode Require Import coq_tactics reduction.
From iris.proofmode Require Export proofmode.
From pv Require Export week10.

(* ########################################################################## *)
(** Tweak WP rules *)
(* ########################################################################## *)

(** We rewrite some of the WP rules with [TCEq] and [TCSimpl] premises. The
classes [TCEq] and [TCSimpl] instruct Coq to automatically solve the premises,
either by just [reflexivity] or [simpl; reflexivity]. *)

(* TODO: Remove once we move to Iris 4.2.0 *)
Class TCSimpl {A} (x x' : A) := TCSimpl_TCEq : TCEq x x'.
Global Hint Extern 0 (TCSimpl _ _) =>
  simpl; notypeclasses refine (TCEq_refl _) : typeclass_instances.
Global Hint Mode TCSimpl ! - - : typeclass_instances.

Global Hint Extern 0 (TCEq _ _) =>
  apply TCEq_eq; eassumption : typeclass_instances.

Lemma App_wp Phi x e v e' :
  TCSimpl (subst x v e) e' ->
  wp e' Phi |~ wp (EApp (EVal (VClosure x e)) (EVal v)) Phi.
Proof. intros <-. apply App_wp. Qed.

Lemma AppRec_wp Phi rec f x e v e' :
  TCEq rec (VRecClosure f x e) ->
  TCSimpl (subst x v (subst f rec e)) e' ->
  wp e' Phi |~ wp (EApp (EVal rec) (EVal v)) Phi.
Proof. intros -> <-. apply AppRec_wp. Qed.

Lemma Op_wp Phi op v1 v2 v :
  TCEq (eval_bin_op op v1 v2) (Some v) ->
  Phi v |~ wp (EOp op (EVal v1) (EVal v2)) Phi.
Proof. intros ?%TCEq_eq. by apply Op_wp. Qed.

Lemma Let_wp x e1 e2 Phi Psi :
  TCSimpl (fun vret => wp (subst x vret e2) Phi) Psi ->
  wp e1 Psi |~ wp (ELet x e1 e2) Phi.
Proof. intros <-. apply Let_wp. Qed.


(* ########################################################################## *)
(** Make the proof mode work *)
(* ########################################################################## *)

(** Iris needs a version of pure that is absorbing, whereas ours is affine. We
thus wrap it. *)

Definition sepPure' (p : Prop) : sepProp := @[ p ] ** sepTrue.

Lemma sepPure'_intro (p : Prop) (P : sepProp) : p -> P |~ sepPure' p.
Proof.
  intros. eapply sepEntails_trans; [apply sepSep_emp_l|apply sepSep_mono].
  - by apply sepPure_intro.
  - apply sepTrue_intro.
Qed.
Lemma sepPure'_elim (p : Prop) (P : sepProp) :
  (p -> sepPure' True |~ P) -> sepPure' p |~ P.
Proof.
  unfold sepPure'. intros HP. apply sepPure_elim; intros.
  eapply sepEntails_trans; [|by apply HP].
  eapply sepEntails_trans; [apply sepSep_emp_l|].
  by apply sepSep_mono_l, sepPure_intro.
Qed.

Lemma sepAnd_mono (P1 P2 Q1 Q2 : sepProp) :
  (P1 |~ P2) -> (Q1 |~ Q2) -> P1 /\\ Q1 |~ P2 /\\ Q2.
Proof.
  intros. apply sepAnd_intro;
    eauto 2 using sepAnd_elim_l, sepAnd_elim_r, sepEntails_trans.
Qed.
Lemma sepOr_mono (P1 P2 Q1 Q2 : sepProp) :
  (P1 |~ P2) -> (Q1 |~ Q2) -> P1 \// Q1 |~ P2 \// Q2.
Proof.
  intros. apply sepOr_elim;
    eauto 2 using sepOr_intro_l, sepOr_intro_r, sepEntails_trans.
Qed.
Lemma sepImpl_mono (P1 P2 Q1 Q2 : sepProp) :
  (P2 |~ P1) -> (Q1 |~ Q2) -> (P1 ->> Q1) |~ (P2 ->> Q2).
Proof.
  intros. apply sepImpl_intro. apply sepEntails_trans with Q1; [|done].
  eapply sepEntails_trans; [|apply sepImpl_elim].
  by apply sepAnd_mono, sepEntails_refl.
Qed.
Lemma sepWand_mono (P1 P2 Q1 Q2 : sepProp) :
  (P2 |~ P1) -> (Q1 |~ Q2) -> (P1 -** Q1) |~ (P2 -** Q2).
Proof.
  intros. apply sepWand_intro. apply sepEntails_trans with Q1; [|done].
  eapply sepEntails_trans; [|apply sepWand_elim].
  by apply sepSep_mono, sepEntails_refl.
Qed.
Lemma sepImpl_revert (P1 P2 Q : sepProp) :
  (P1 |~ P2 ->> Q) -> (P1 /\\ P2 |~ Q).
Proof.
  intros H. eapply sepEntails_trans; [|apply sepImpl_elim].
  apply sepAnd_intro; [apply sepAnd_elim_r|].
  by eapply sepEntails_trans; [apply sepAnd_elim_l|].
Qed.

Lemma sepEntails_emp_exist {A} (Phi : A -> sepProp) :
  (EMP |~ Ex x, Phi x) -> (exists x, EMP |~ Phi x).
Proof.
  intros HPhi. destruct (HPhi NatMap.empty) as [x ?]; [done|].
  exists x. by intros h ->.
Qed.

(** Define setoid equality and bi-entailment. *)

Global Instance sepProp_equiv : Equiv sepProp := fun P Q => (P |~ Q) /\ (Q |~ P).
Global Instance sepProp_equivalence : Equivalence (@equiv sepProp _).
Proof.
  unfold equiv, sepProp_equiv.
  split; red; intuition eauto using sepEntails_refl, sepEntails_trans.
Qed.

Canonical Structure sepPropO := discreteO sepProp.

(** Now we show that our separation logic is an instance of Iris's BI
interface. This proof is a bit clunky because 1.) some of our proof rules
slightly differently (TODO: make consistent in future years), 2.) we need to
prove all the [NonExpansive] properties, which are uninteresting in our logic
because we do not have step-indexing (TODO: add smart constructor to Iris). *)

Definition sepProp_bi_mixin :
  BiMixin
    sepEntails sepEmp sepPure' sepAnd sepOr sepImpl
    (@sepForall val) (@sepExists val) sepSep sepWand.
Proof.
  split.
  - split.
    + intros P. apply sepEntails_refl.
    + intros P Q R. by eapply sepEntails_trans.
  - done.
  - intros ? φ φ' [??]; split; apply sepPure'_elim; intros ?;
      apply sepPure'_intro; auto.
  - intros ? P P' [??] Q Q' [??]; split; by apply sepAnd_mono.
  - intros ? P P' [??] Q Q' [??]; split; by apply sepOr_mono.
  - intros ? P P' [??] Q Q' [??]; split; by apply sepImpl_mono.
  - intros A ? Φ Φ' HΦ; split; apply sepForall_intro; intros x;
      eapply sepForall_elim_alt, HΦ.
  - intros A ? Φ Φ' HΦ; split; apply sepExist_elim; intros x;
      eapply sepExist_intro_alt, HΦ.
  - intros ? P P' [??] Q Q' [??]; split; by apply sepSep_mono.
  - intros ? P P' [??] Q Q' [??]; split; by apply sepWand_mono.
  - intros φ P. apply sepPure'_intro.
  - intros φ P. apply sepPure'_elim.
  - intros P Q. apply sepAnd_elim_l.
  - intros P Q. apply sepAnd_elim_r.
  - intros P Q R. apply sepAnd_intro.
  - intros P Q. apply sepOr_intro_l.
  - intros P Q. apply sepOr_intro_r.
  - intros P Q R. apply sepOr_elim.
  - intros P Q R ?. eapply sepImpl_intro, sepEntails_trans; [|done].
    apply sepAnd_intro; auto using sepAnd_elim_l, sepAnd_elim_r.
  - intros P Q R ?. eapply sepEntails_trans; [|apply (sepImpl_elim Q)].
    apply sepAnd_intro; [by apply sepAnd_elim_r|].
    by eapply sepEntails_trans; [apply sepAnd_elim_l|].
  - intros A P Φ. apply sepForall_intro.
  - intros A Φ x. apply sepForall_elim.
  - intros A Φ x. apply sepExist_intro.
  - intros A Φ Q. apply sepExist_elim.
  - intros P P' Q Q'. apply sepSep_mono.
  - intros P. apply sepSep_emp_l.
  - intros P. apply sepSep_emp_l_inv.
  - intros P Q. apply sepSep_comm.
  - intros P Q R. apply sepSep_assoc'.
  - intros P Q R ?. apply sepWand_intro.
    by eapply sepEntails_trans; [apply sepSep_comm|].
  - intros. eapply sepEntails_trans; [apply sepSep_comm|].
    eapply sepEntails_trans; [by apply sepSep_mono_r|]. apply sepWand_elim.
Qed.

Definition sepPersistently (P : sepProp) : sepProp := sepPure' (EMP |~ P).

Definition sepProp_bi_persistently_mixin :
  BiPersistentlyMixin
    sepEntails sepEmp sepAnd
    (@sepExists val) sepSep sepPersistently.
Proof.
  eapply bi_persistently_mixin_discrete, sepProp_bi_mixin; [done|..|done].
  intros A Phi. apply sepEntails_emp_exist.
Qed.

Definition sepProp_bi_later_mixin :
  BiLaterMixin
    sepEntails sepPure' sepOr sepImpl
    (@sepForall val) (@sepExists val) sepSep sepPersistently id.
Proof. by eapply bi_later_mixin_id, sepProp_bi_mixin. Qed.

Canonical Structure sepPropI : bi :=
  {| bi_ofe_mixin := ofe_mixin_of sepProp;
     bi_bi_mixin := sepProp_bi_mixin;
     bi_bi_persistently_mixin := sepProp_bi_persistently_mixin;
     bi_bi_later_mixin := sepProp_bi_later_mixin |}.

(** Define a bunch of instances so that the Iris Proof Mode understands our
(affine) version of pure, as well as our [sepTrue] and [sepFalse] connectives
(which are instances of pure in Iris). *)

Global Instance sepPure_affine p : Affine (@[ p ] : sepProp).
Proof.
  rewrite /Affine -(right_id emp%I bi_sep (@[ p ])).
  by apply sepPure_elim.
Qed.
Global Instance sepPure_persistent p : Persistent (@[ p ] : sepProp).
Proof.
  rewrite /Persistent -(right_id emp%I bi_sep (@[ p ])).
  apply sepPure_elim; intros. apply sepPure'_intro. rewrite right_id.
  by apply sepPure_intro.
Qed.

Global Instance sepTrue_absorbing : Absorbing (sepTrue : sepProp).
Proof. rewrite /Absorbing. apply sepTrue_intro. Qed.
Global Instance sepTrue_persistent : Persistent (sepTrue : sepProp).
Proof. rewrite /Persistent. apply sepPure'_intro, sepTrue_intro. Qed.

Global Instance sepFalse_affine : Affine (sepFalse : sepProp).
Proof. rewrite /Affine. apply sepFalse_elim. Qed.
Global Instance sepFalse_absorbing : Absorbing (sepFalse : sepProp).
Proof. rewrite /Absorbing. apply bi.wand_elim_r', sepFalse_elim. Qed.
Global Instance sepFalse_persistent : Persistent (sepFalse : sepProp).
Proof. rewrite /Persistent. apply sepFalse_elim. Qed.

Global Instance sepPure_into_pure p : IntoPure (@[ p ] : sepProp) p.
Proof.
  rewrite /IntoPure /bi_pure /= -{1}(right_id emp%I bi_sep (@[ p ])).
  apply sepPure_elim, sepPure'_intro.
Qed.
Global Instance sepTrue_into_pure : IntoPure (sepTrue : sepProp) True.
Proof. rewrite /IntoPure. apply bi.True_intro. Qed.
Global Instance sepFalse_into_pure : IntoPure (sepFalse : sepProp) False.
Proof. rewrite /IntoPure. apply sepFalse_elim. Qed.

Global Instance sepPure_from_pure p : FromPure true (@[ p ] : sepProp) p.
Proof.
  rewrite /FromPure /= /bi_pure /bi_affinely /= comm.
  apply sepImpl_revert. apply sepPure_elim; intros.
  apply sepImpl_intro. rewrite bi.and_elim_l. by apply sepPure_intro.
Qed.
Global Instance sepTrue_from_pure : FromPure false (sepTrue : sepProp) True.
Proof. rewrite /FromPure. apply sepTrue_intro. Qed.
Global Instance sepFalse_from_pure : FromPure false (sepFalse : sepProp) False.
Proof. rewrite /FromPure /=. apply bi.False_elim. Qed.

(** Make sure that [auto] understands our connectives. *)

Global Hint Extern 1 (environments.envs_entails _ (_ /\\ _)) => iSplit : core.
Global Hint Extern 1 (environments.envs_entails _ (_ ** _)) => iSplit : core.
Global Hint Extern 1 (environments.envs_entails _ (Ex _, _)) => iExists _ : core.
Global Hint Extern 1 (environments.envs_entails _ (_ \// _)) => iLeft : core.
Global Hint Extern 1 (environments.envs_entails _ (_ \// _)) => iRight : core.
Global Hint Extern 2 (environments.envs_entails _ (_ ** _)) => progress iFrame : iFrame.

(** When importing lemmas into the proof mode, Iris unfolds definitions too
eagerly, and unfolds our entailment relation [|~]. This happens even if we
make the entailment opaque due to Coq bug https://github.com/coq/coq/issues/9135.
In Iris-based logics this problem does not occur because entailment (and all
logical operators) are sealed. Our logic is not sealed (because we want easy
access to its model in the lectures of week 9 and 10). We thus patch up Iris to
give our entailement [|~] precedence so that it is not unfolded. *)

Global Opaque sepEntails.

Ltac iIntoEmpValid_go ::=
  first
    [(* Case [|~] *)
     notypeclasses refine (coq_tactics.into_emp_valid_here (_ |~ _) _ _)
    |(* Case [φ -> ψ] *)
     notypeclasses refine (coq_tactics.into_emp_valid_impl _ _ _ _ _);
       [(*goal for [φ] *)|iIntoEmpValid_go]
    |(* Case [∀ x : A, φ] *)
     notypeclasses refine (coq_tactics.into_emp_valid_forall _ _ _ _); iIntoEmpValid_go
    |(* Case [∀.. x : TT, φ] *)
     notypeclasses refine (coq_tactics.into_emp_valid_tforall _ _ _ _); iIntoEmpValid_go
    |(*| Case [P ⊢ Q], [P ⊣⊢ Q], [⊢ P] *)
     notypeclasses refine (coq_tactics.into_emp_valid_here _ _ _)].

Global Instance into_wand_impl_pure (Q : sepProp) φ :
  IntoWand true false (⌜ φ ⌝ → Q) (@[ φ ]) Q.
Proof.
  rewrite /IntoWand /= bi.intuitionistically_elim.
  rewrite (bi.intuitionistic (@[ _ ])) -bi.impl_wand_intuitionistically.
  apply bi.impl_mono; [|done]. rewrite bi.persistently_into_absorbingly.
  rewrite /bi_absorbingly comm. apply bi.sep_mono; [done|]. apply sepTrue_intro.
Qed.


(* ########################################################################## *)
(** Automation for our program logic *)
(* ########################################################################## *)

(** We provide two extensions to make WP proofs easier:

- We extend Iris's [iApply] to become smarter when applying WP lemmas:
  + We let it automatically perform pure reductions.
  + We let it automatically perform the context rule [wp_ctx].
  + We let it automatically perform framing + monotonicity, i.e., let is
    generate a wand if the postconditions do not match.
  + We let it curry the postcondition.
- We provide [wp_X] tactics.

To implement these extensions, we need automation to decompose an expression
into an evaluation context and a subexpression. We implement this automation
solely using classes. Our class-based solution is less error-prone than Iris's
[Ltac]-based solution in its [reshape_expr] tactic, and works better for our
"functional evaluation contexts" since we also need to construct a proof of
[ctx k]. *)

(** The typical instances are [P := TCEq e'] to find the subterm [e'] in [e],
and [P := PureStep Phii] to find a pure redex. *)
Inductive IntoCtx (e : expr) (P : expr -> Prop) (k : expr -> expr) : Prop :=
  Mk_IntoCtx (e' : expr) (_ : ctx k) (_ : e = k e') (_ : P e').
Existing Class IntoCtx.
Global Hint Mode IntoCtx ! ! - : typeclass_instances.

(** Since we use a big-step semantics, we cannot state [PureStep] in terms of
the small-step reduction relation. We state it semantically using [wp]. (The
default cost for the other instances is 1.) *)
Class PureStep (Phii : (val -> sepProp) -> sepProp) (e : expr) := {
  pure_step Phi : Phii Phi |~ wp e Phi;
}.
Global Hint Mode PureStep - ! : typeclass_instances.

(** [IntoCtx] instances *)

(** Lowest cost, since we want to traverse outside-inside and find the first
subterm that matches. *)
Global Instance into_ctx_id e (P : expr -> Prop) :
  P e -> IntoCtx e P (fun x => x) | 0.
Proof. exists e; eauto using ctx. Qed.

Global Instance into_ctx_app_r k e1 e2 P :
  IntoCtx e2 P k ->
  IntoCtx (EApp e1 e2) P (fun x => EApp e1 (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.
Global Instance into_ctx_app_l k e1 v2 P :
  IntoCtx e1 P k ->
  IntoCtx (EApp e1 (EVal v2)) P (fun x => EApp (k x) (EVal v2)).
Proof.
  intros [e' ? -> ?]; exists e'; [|done..].
  apply (Compose_ctx (λ x, EApp x _)); auto using ctx.
Qed.
Global Instance into_ctx_op_r k op e1 e2 P :
  IntoCtx e2 P k ->
  IntoCtx (EOp op e1 e2) P (fun x => EOp op e1 (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.
Global Instance into_ctx_op_l k op e1 v2 P :
  IntoCtx e1 P k ->
  IntoCtx (EOp op e1 (EVal v2)) P (fun x => EOp op (k x) (EVal v2)).
Proof.
  intros [e' ? -> ?]; exists e'; [|done..].
  apply (Compose_ctx (λ x, EOp _ x _)); auto using ctx.
Qed.
Global Instance into_ctx_pair_r k e1 e2 P :
  IntoCtx e2 P k ->
  IntoCtx (EPair e1 e2) P (fun x => EPair e1 (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.
Global Instance into_ctx_pair_l k e1 v2 P :
  IntoCtx e1 P k ->
  IntoCtx (EPair e1 (EVal v2)) P (fun x => EPair (k x) (EVal v2)).
Proof.
  intros [e' ? -> ?]; exists e'; [|done..].
  apply (Compose_ctx (λ x, EPair x _)); auto using ctx.
Qed.
Global Instance into_ctx_fst k e P :
  IntoCtx e P k ->
  IntoCtx (EFst e) P (fun x => EFst (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.
Global Instance into_ctx_snd k e P :
  IntoCtx e P k ->
  IntoCtx (ESnd e) P (fun x => ESnd (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.
Global Instance into_ctx_if k e e1 e2 P :
  IntoCtx e P k ->
  IntoCtx (EIf e e1 e2) P (fun x => EIf (k x) e1 e2).
Proof.
  intros [e' ? -> ?]; exists e'; [|done..].
  apply (Compose_ctx (λ x, EIf x _ _)); auto using ctx.
Qed.
Global Instance into_ctx_seq k e1 e2 P :
  IntoCtx e1 P k ->
  IntoCtx (ESeq e1 e2) P (fun x => ESeq (k x) e2).
Proof.
  intros [e' ? -> ?]; exists e'; [|done..].
  apply (Compose_ctx (λ x, ESeq x _)); auto using ctx.
Qed.
Global Instance into_ctx_alloc k e P :
  IntoCtx e P k ->
  IntoCtx (EAlloc e) P (fun x => EAlloc (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.
Global Instance into_ctx_load k e P :
  IntoCtx e P k ->
  IntoCtx (ELoad e) P (fun x => ELoad (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.
Global Instance into_ctx_store_r k e1 e2 P :
  IntoCtx e2 P k ->
  IntoCtx (EStore e1 e2) P (fun x => EStore e1 (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.
Global Instance into_ctx_store_l k e1 v2 P :
  IntoCtx e1 P k ->
  IntoCtx (EStore e1 (EVal v2)) P (fun x => EStore (k x) (EVal v2)).
Proof.
  intros [e' ? -> ?]; exists e'; [|done..].
  apply (Compose_ctx (λ x, EStore x _)); auto using ctx.
Qed.
Global Instance into_ctx_free k e P :
  IntoCtx e P k ->
  IntoCtx (EFree e) P (fun x => EFree (k x)).
Proof. intros [e' ? -> ?]; exists e'; eauto using ctx. Qed.

(** [PureStep] instances *)
Global Instance pure_step_lam x e :
  PureStep (fun Phi => Phi (VClosure x e)) (ELam x e).
Proof. split=> Phi. by apply Lam_wp. Qed.

Global Instance pure_step_seq e1 e2 :
  PureStep (fun Phi => wp e1 (fun _ => wp e2 Phi)) (ESeq e1 e2).
Proof. split=> Phi. by apply Seq_wp. Qed.

Global Instance pure_step_let Phii x e1 e2 :
  TCSimpl (fun Phi vret => wp (subst x vret e2) Phi) Phii ->
  PureStep (fun Phi => wp e1 (Phii Phi)) (ELet x e1 e2) | 0.
Proof. intros <-. split=> Phi. by apply Let_wp. Qed.

(** [pure_step_app] is not an [Instance], but a [Hint Extern] to enable
syntactic matching instead of unification-based matching. This is important
because it would otherwise unfold definitions. For example, when considering
[wp (swap ...) Phi] where [Definition swap := VLam ...], it should *not* unfold
[swap] to as to preserve abstraction barriers. *)
Lemma pure_step_app x e v e' :
  TCSimpl (subst x v e) e' ->
  PureStep (wp e') (EApp (EVal (VClosure x e)) (EVal v)).
Proof. split=> Phi. by apply App_wp. Qed.
Global Hint Extern 1 (PureStep _ (EApp (EVal (VClosure _ _)) (EVal _))) =>
  notypeclasses refine (pure_step_app _ _ _ _ _) : typeclass_instances.

Global Instance pure_step_op op v1 v2 v :
  TCEq (eval_bin_op op v1 v2) (Some v) ->
  PureStep (fun Phi => Phi v) (EOp op (EVal v1) (EVal v2)).
Proof. split=> Phi. by apply Op_wp. Qed.

Global Instance pure_step_pair v1 v2 :
  PureStep (fun Phi => Phi (VPair v1 v2)) (EPair (EVal v1) (EVal v2)).
Proof. split=> Phi. by apply Pair_wp. Qed.

Global Instance pure_step_fst v1 v2 :
  PureStep (fun Phi => Phi v1) (EFst (EVal (VPair v1 v2))).
Proof. split=> Phi. by apply Fst_wp. Qed.

Global Instance pure_step_snd v1 v2 :
  PureStep (fun Phi => Phi v2) (ESnd (EVal (VPair v1 v2))).
Proof. split=> Phi. by apply Snd_wp. Qed.

Global Instance pure_step_if_true e1 e2 :
  PureStep (wp e1) (EIf (EVal (VBool true)) e1 e2).
Proof. split=> Phi. by apply If_true_wp. Qed.

Global Instance pure_step_if_false e1 e2 :
  PureStep (wp e2) (EIf (EVal (VBool false)) e1 e2).
Proof. split=> Phi. by apply If_false_wp. Qed.

(** We now extend [iApply] and [iAssumption] to automatically use [wp_ctx] and
framing/weakening. *)

(** The class [NewPost] is used to only apply the bind rule if the postcondition
is non-trivial (i.e., not [id]). This results in simpler goals. *)
Class NewPost (k : expr -> expr) (Phi Phi' : val -> sepProp) :=
  { new_post e : ctx k -> wp e Phi' |~ wp (k e) Phi }.
Global Hint Mode NewPost ! ! - : typeclass_instances.

Global Instance new_post_default k Phi Phi' :
  TCIf (TCEq k id) (TCEq Phi Phi')
                   (TCEq (fun vret => wp (k (EVal vret)) Phi) Phi') ->
  NewPost k Phi Phi' | 10.
Proof.
  intros [-> ->|<-]; [done|].
  split=> e ?. by apply wp_ctx.
Qed.

Class NormalizeWP (P : sepProp) (e' : expr) (Phi' : val -> sepProp) :=
  { normalize_wp : wp e' Phi' |~ P }.
Global Hint Mode NormalizeWP ! ! - : typeclass_instances.

Global Instance normalize_wp_here e e' k Phi Phi' :
  TCNoBackTrack (IntoCtx e (TCEq e') k) ->
  NewPost k Phi Phi' ->
  NormalizeWP (wp e Phi) e' Phi' | 0.
Proof. intros [ [?? -> <-] ] [Hpost]. split. by apply Hpost. Qed.

Global Instance normalize_wp_val v Phi e' Phi' :
  NormalizeWP (Phi v) e' Phi' ->
  NormalizeWP (wp (EVal v) Phi) e' Phi' | 1.
Proof. intros [HPhi]. split. by rewrite -Val_wp. Qed.

Global Instance normalize_wp_step e e' k Phii Phi Phi' Phi'' :
  TCNoBackTrack (IntoCtx e (PureStep Phii) k) ->
  NewPost k Phi Phi' ->
  NormalizeWP (Phii Phi') e' Phi'' ->
  NormalizeWP (wp e Phi) e' Phi'' | 10.
Proof.
  intros [ [e'' ? -> [He'']] ] [Hpost] [HPhii]; split.
  by rewrite HPhii He'' Hpost.
Qed.

Class WandPost (Φ Ψ : val -> sepProp) (P : sepProp) :=
  { wand_post : P |~ All vret, Φ vret -** Ψ vret }.

Global Instance wand_post_here Φ Ψ :
  WandPost Φ Ψ (All vret, Φ vret -** Ψ vret) | 100.
Proof. by constructor. Qed.
Lemma wand_post_pure v Ψ :
  WandPost (fun vret => @[ vret = v]) Ψ (Ψ v).
Proof. constructor. by iIntros "H" (vret ->). Qed.
Lemma wand_post_pure_sep v P Ψ :
  WandPost (fun vret => @[ vret = v ] ** P) Ψ (P -** Ψ v).
Proof. constructor. iIntros "H" (vret) "[-> ?]". by iApply "H". Qed.
Lemma wand_post_exist {A} (Φ : A -> val -> sepProp) Ψ Ψ' :
  (forall x, WandPost (Φ x) Ψ (Ψ' x)) ->
  WandPost (fun vret => Ex x, Φ x vret) Ψ (All x, Ψ' x).
Proof.
  constructor. iIntros "H" (vret) "[%x ?]". destruct (H x) as [Hx].
  by iApply (Hx with "[H]").
Qed.
(* Not instances, to avoid unfolding definitions *)
Global Hint Extern 10 (WandPost (fun _ => @[ _ ]) _ _) =>
  notypeclasses refine (wand_post_pure _ _) : typeclass_instances.
Global Hint Extern 10 (WandPost (fun _ => @[ _ ] ** _) _ _) =>
  notypeclasses refine (wand_post_pure_sep _ _ _) : typeclass_instances.
Global Hint Extern 10 (WandPost (fun _ => Ex _, _) _ _) =>
  notypeclasses refine (wand_post_exist _ _ _ _) : typeclass_instances.

(** We now extend [iApply] and [iAssumption] to automatically use [wp_ctx] and
framing/weakening. *)
Global Instance from_assumption_wp p e e' Phi Phi' :
  NormalizeWP (wp e Phi) e' Phi' ->
  FromAssumption p (wp e' Phi') (wp e Phi) | 2.
Proof.
  intros [HPhi].
  rewrite /FromAssumption bi.intuitionistically_if_elim. by rewrite HPhi.
Qed.

Global Instance into_wand_wp p e e' Phi Psi Psi' Q :
  NormalizeWP (wp e Psi) e' Psi' ->
  WandPost Phi Psi' Q ->
  IntoWand p false (wp e' Phi) Q (wp e Psi).
Proof.
  intros [HPsi] [HQ]. rewrite /IntoWand /= bi.intuitionistically_if_elim.
  apply sepWand_intro. rewrite comm wp_frame. rewrite -HPsi //.
  apply wp_mono; intros vret. by rewrite HQ (bi.forall_elim vret) bi.wand_elim_r.
Qed.

(** This instance should not be needed, but is a workaround for
https://gitlab.mpi-sws.org/iris/iris/-/issues/458 *)

Global Instance into_wand_wand_wp p q e e' P P' Phi Phi' :
  NormalizeWP (wp e Phi) e' Phi' ->
  FromAssumption q P P' ->
  IntoWand p q (P' -∗ wp e' Phi') P (wp e Phi).
Proof.
  rewrite /FromAssumption /IntoWand. intros [HPhi] ->; simpl.
  rewrite bi.intuitionistically_if_elim.
  apply bi.wand_mono; [done|]. by rewrite HPhi.
Qed.

(** The [iPures] tactic. *)
Class PureSteps (n : nat) (Pin Pout : sepProp) := { pure_steps : Pout |~ Pin }.
Global Hint Mode PureSteps - ! - : typeclass_instances.

Global Instance pure_steps_default P : PureSteps 0 P P | 100.
Proof. by split. Qed.

Global Instance pure_steps_step n e k Phii Phi Phi' P :
  IntoCtx e (PureStep Phii) k ->
  NewPost k Phi Phi' ->
  PureSteps n (Phii Phi') P ->
  PureSteps (S n) (wp e Phi) P | 10.
Proof.
  intros [e' ? -> [He']] [Hpost] [HP]; split. by rewrite HP He' Hpost.
Qed.

Global Instance pure_steps_val n v Phi P :
  PureSteps n (Phi v) P ->
  PureSteps (S n) (wp (EVal v) Phi) P | 0.
Proof. intros [HP]. split. by rewrite -Val_wp. Qed.

Inductive PureStepsIntoCtx
    (Pin : sepProp) (P : expr -> Prop) (Phi : val -> sepProp) : Prop :=
  Mk_PureStepsIntoCtx e (_ : P e) (_ : wp e Phi |~ Pin).
Existing Class PureStepsIntoCtx.
Global Hint Mode PureStepsIntoCtx ! ! - : typeclass_instances.

Global Instance pure_steps_into_ctx_here e P k Phi Phi' :
  IntoCtx e P k ->
  NewPost k Phi Phi' ->
  PureStepsIntoCtx (wp e Phi) P Phi' | 1.
Proof. intros [e' ? -> ?] [Hwp]. exists e'; auto. Qed.

Global Instance pure_steps_into_ctx_step e Phii k Phi Phi' P Phi'' :
  IntoCtx e (PureStep Phii) k ->
  NewPost k Phi Phi' ->
  PureStepsIntoCtx (Phii Phi') P Phi'' ->
  PureStepsIntoCtx (wp e Phi) P Phi'' | 10.
Proof.
  intros [e' ? -> [HPhii]] [Hwp] [e'' ? He'']. exists e''; [done|].
  by rewrite He'' HPhii Hwp.
Qed.

Global Instance pure_steps_into_ctx_val Phi v P Phi' :
  PureStepsIntoCtx (Phi v) P Phi' ->
  PureStepsIntoCtx (wp (EVal v) Phi) P Phi' | 0.
Proof. intros [e' ??]. exists e'; [done|]. by rewrite -Val_wp. Qed.

(** And finally some tactics *)

Lemma tac_wp_pure {Δ e Phi Q} n :
  PureSteps n (wp e Phi) Q ->
  envs_entails Δ Q ->
  envs_entails Δ (wp e Phi).
Proof. by rewrite envs_entails_unseal=> -[Hsteps] ->. Qed.

Class PureStepAppClosure (e2 e1 : expr) :=
  { pure_step_app_closure Phi : wp e2 Phi |~ wp e1 Phi }.

Global Instance pure_step_app_closure_nonrec x e v2 e' :
  TCSimpl (subst x v2 e) e' ->
  PureStepAppClosure e' (EApp (EVal (VClosure x e)) (EVal v2)).
Proof. intros <-. split=> Phi. iApply App_wp. Qed.
Global Instance pure_step_app_closure_rec f x e v1 v2 e' :
  TCEq v1 (VRecClosure f x e) ->
  TCSimpl (subst x v2 (subst f v1 e)) e' ->
  PureStepAppClosure e' (EApp (EVal v1) (EVal v2)).
Proof. intros -> <-. split=> Phi. iApply AppRec_wp. Qed.

Lemma tac_wp_unfold {Δ e e' Phi Phi'} :
  PureStepsIntoCtx (wp e Phi) (PureStepAppClosure e') Phi' ->
  envs_entails Δ (wp e' Phi') ->
  envs_entails Δ (wp e Phi).
Proof.
  rewrite envs_entails_unseal. iIntros ([e'' [Hwp] <-] HΔ) "HΔ".
  iApply Hwp. by iApply HΔ.
Qed.

Lemma tac_wp_alloc {Δ e v Phi Phi'} i :
  PureStepsIntoCtx (wp e Phi) (TCEq (EAlloc (EVal v))) Phi' ->
  (forall l,
    match envs_app false (Esnoc Enil i (l ~> v)) Δ with
    | Some Δ' => envs_entails Δ' (Phi' (VRef l))
    | None => False
    end) ->
  envs_entails Δ (wp e Phi).
Proof.
  rewrite envs_entails_unseal. iIntros ([e' <- <-] HΔ) "HΔ".
  iApply (Alloc_wp with "[//]"); iIntros (l) "Hl". specialize (HΔ l).
  destruct (envs_app _ _ _) as [Δ''|] eqn:?; last done. iApply HΔ.
  iApply (envs_app_sound with "[$]"); first done. by iFrame.
Qed.

Lemma tac_wp_free {Δ e l v i Phi Phi'} :
  PureStepsIntoCtx (wp e Phi) (TCEq (EFree (EVal (VRef l)))) Phi' ->
  envs_lookup i Δ = Some (false, l ~> v)%I ->
  envs_entails (envs_delete true i false Δ) (Phi' v) ->
  envs_entails Δ (wp e Phi).
Proof.
  rewrite envs_entails_unseal. iIntros ([e' <- <-] Hi HΔ) "HΔ".
  rewrite envs_lookup_sound //=. set (envs_delete true i _ Δ) as Δ' in *.
  iDestruct "HΔ" as "[Hl HΔ]". iApply (Free_wp with "Hl"). by iApply HΔ.
Qed.

Lemma tac_wp_load {Δ e l v i Phi Phi'} :
  PureStepsIntoCtx (wp e Phi) (TCEq (ELoad (EVal (VRef l)))) Phi' ->
  envs_lookup i Δ = Some (false, l ~> v)%I ->
  envs_entails Δ (Phi' v)  ->
  envs_entails Δ (wp e Phi).
Proof.
  rewrite envs_entails_unseal. iIntros ([e' <- <-] Hi HΔ) "HΔ".
  iDestruct (envs_lookup_split with "HΔ") as "[Hl HΔ]"; [done|]; simpl.
  iApply (Load_wp with "Hl"); iIntros "Hl". iApply HΔ. by iApply "HΔ".
Qed.

Lemma tac_wp_store {Δ e l v v' i Phi Phi'} :
  PureStepsIntoCtx (wp e Phi) (TCEq (EStore (EVal (VRef l)) (EVal v'))) Phi' ->
  envs_lookup i Δ = Some (false, l ~> v)%I ->
  match envs_simple_replace i false (Esnoc Enil i (l ~> v')) Δ with
  | Some Δ' => envs_entails Δ' (Phi' (VRef l))
  | None => False
  end ->
  envs_entails Δ (wp e Phi).
Proof.
  rewrite envs_entails_unseal. iIntros ([e' <- <-] Hi HΔ) "HΔ".
  destruct (envs_simple_replace _ _ _ _) as [Δ''|] eqn:?; last done.
  iDestruct (envs_simple_replace_sound with "HΔ") as "[Hl HΔ]"; [done..|]; simpl.
  iApply (Store_wp with "Hl"); iIntros "Hl". iApply HΔ. iApply "HΔ"; by iFrame.
Qed.

Tactic Notation "wp_pure" open_constr(n) :=
  iStartProof;
  lazymatch goal with
  | |- environments.envs_entails _ (wp _ _) =>
     notypeclasses refine (tac_wp_pure n _ _);
       [tc_solve || fail 1 "wp_pure: no pure steps can be performed"
       |pm_prettify]
  | |- _ => fail "wp_pure: goal not a `wp`"
  end.
Tactic Notation "wp_pure" := wp_pure 1.
Tactic Notation "wp_pure!" := wp_pure (S _).

Tactic Notation "wp_unfold" :=
  iStartProof;
  lazymatch goal with
  | |- environments.envs_entails _ (wp _ _) =>
     notypeclasses refine (tac_wp_unfold _ _);
       [tc_solve || fail 1 "wp_unfold: no beta reduction step can be performed"
       |pm_prettify]
  | |- _ => fail "wp_lam: goal not a `wp`"
  end.
Tactic Notation "wp_unfold" "!" := wp_unfold; wp_pure _.

Tactic Notation "wp_alloc" ident(l) "as" constr(H) :=
  iStartProof;
  let Htmp := iFresh in
  lazymatch goal with
  | |- environments.envs_entails _ (wp _ _) =>
     notypeclasses refine (tac_wp_alloc Htmp _ _);
       [tc_solve || fail 1 "wp_alloc: cannot find 'alloc' subexpression"
       |pm_reduce;
        first [intros l | fail 1 "wp_alloc:" l "not fresh"];
        iDestructHyp Htmp as H]
  | _ => fail "wp_alloc: goal not a 'wp'"
  end.
Tactic Notation "wp_alloc" "!" ident(l) "as" constr(H) :=
  wp_alloc l as H; wp_pure _.
Tactic Notation "wp_alloc" ident(l) := wp_alloc l as "?".
Tactic Notation "wp_alloc" "!" ident(l) := wp_alloc! l as "?".

Tactic Notation "wp_free" :=
  iStartProof;
  lazymatch goal with
  | |- environments.envs_entails _ (wp _ _) =>
     notypeclasses refine (tac_wp_free _ _ _);
       [tc_solve || fail 1 "wp_free: cannot find 'free' subexpression"
       |iAssumptionCore || fail 1 "wp_free: cannot find ~>"
       |pm_reduce]
  | _ => fail "wp_free: goal not a 'wp'"
  end.
Tactic Notation "wp_free" "!" := wp_free; wp_pure _.

Tactic Notation "wp_load" :=
  iStartProof;
  lazymatch goal with
  | |- environments.envs_entails _ (wp _ _) =>
     notypeclasses refine (tac_wp_load _ _ _);
       [tc_solve || fail 1 "wp_load: cannot find 'load' subexpression"
       |iAssumptionCore || fail 1 "wp_load: cannot find ~>"
       |pm_reduce]
  | _ => fail "wp_load: goal not a 'wp'"
  end.
Tactic Notation "wp_load" "!" := wp_load; wp_pure _.

Tactic Notation "wp_store" :=
  iStartProof;
  lazymatch goal with
  | |- environments.envs_entails _ (wp _ _) =>
     notypeclasses refine (tac_wp_store _ _ _);
       [tc_solve || fail 1 "wp_store: cannot find 'store' subexpression"
       |iAssumptionCore || fail 1 "wp_store: cannot find ~>"
       |pm_reduce]
  | _ => fail "wp_store: goal not a 'wp'"
  end.
Tactic Notation "wp_store" "!" := wp_store; wp_pure _.


(* ########################################################################## *)
(** Disable ssreflect rewrite *)
(* ########################################################################## *)

(** Iris uses ssreflect's [rewrite] tactic. We do not want to use that in this
course, so we disable it. *)

Global Unset SsrRewrite.


(* ########################################################################## *)
(** Language extensions *)
(* ########################################################################## *)

(** Our language does not have sum types, so let us encode them. *)

Definition injl_tag := true.
Definition injr_tag := false.

Notation EInjL e :=
  (EPair (EVal (VBool injl_tag)) e).
Notation EInjR e :=
  (EPair (EVal (VBool injr_tag)) e).

Notation VInjL v :=
  (VPair (VBool injl_tag) v).
Notation VInjR v :=
  (VPair (VBool injr_tag) v).

Definition sum_case :=
  VClosure "f1" (ELam "f2" (ELam "x"
    (EIf (EFst (EVar "x"))
         (EApp (EVar "f1") (ESnd (EVar "x")))
         (EApp (EVar "f2") (ESnd (EVar "x")))))).
Notation EMatch e x1 e1 x2 e2 :=
  (EApp (EApp (EApp (EVal sum_case) (ELam x1 e1)) (ELam x2 e2)) e).

Lemma InjL_wp Phi v :
  Phi (VInjL v) |~ wp (EInjL (EVal v)) Phi.
Proof. apply Pair_wp. Qed.

Lemma InjR_wp Phi v :
  Phi (VInjR v) |~ wp (EInjR (EVal v)) Phi.
Proof. apply Pair_wp. Qed.

Lemma Match_InjL_wp Phi v x1 e1 x2 e2 e' :
  TCSimpl (subst x1 v e1) e' ->
  wp e' Phi |~ wp (EMatch (EVal (VInjL v)) x1 e1 x2 e2) Phi.
Proof. iIntros (<-) "Hwp". unfold sum_case. by wp_pure!. Qed.

Lemma Match_InjR_wp Phi v x1 e1 x2 e2 e' :
  TCSimpl (subst x2 v e2) e' ->
  wp e' Phi |~ wp (EMatch (EVal (VInjR v)) x1 e1 x2 e2) Phi.
Proof. iIntros (<-) "Hwp". unfold sum_case. by wp_pure!. Qed.

(** Make sure [iPures] uses the sum rules automatically. Since [VInjL] and
[VInjR] are just notation, we do not need special instances. *)
Global Instance pure_step_match_injl v x1 e1 x2 e2 e' :
  TCSimpl (subst x1 v e1) e' ->
  PureStep (wp e') (EMatch (EVal (VInjL v)) x1 e1 x2 e2).
Proof. split=> Phi. by apply Match_InjL_wp. Qed.

Global Instance pure_step_match_injr v x1 e1 x2 e2 e' :
  TCSimpl (subst x2 v e2) e' ->
  PureStep (wp e') (EMatch (EVal (VInjR v)) x1 e1 x2 e2).
Proof. split=> Phi. by apply Match_InjR_wp. Qed.


(* ########################################################################## *)
(** Notations *)
(* ########################################################################## *)

Module language_notation.
  (** Writing plain ASTs for programs in our language becomes very annoying. We
  define some notations. These notations are inspired by Iris's HeapLang, see
  https://gitlab.mpi-sws.org/iris/iris/-/blob/master/iris_heap_lang/notation.v
  A major difference is that we avoid the use of scopes, so we add colons to more
  notations, e.g., lambdas and binary operators. *)

  Coercion EVar : string >-> expr.
  Coercion EVal : val >-> expr.
  Coercion EApp : expr >-> Funclass.

  Notation "'let:' x := e1 'in' e2" := (ELet x e1 e2)
    (at level 200, x at level 1, e1, e2 at level 200,
     format "'[' 'let:'  x  :=  '[' e1 ']'  'in'  '/' e2 ']'").
  Notation "e1 ;; e2" := (ESeq e1 e2)
    (at level 100, e2 at level 200,
     format "'[' '[hv' '[' e1 ']' ;;  ']' '/' e2 ']'").
  Notation "! e" := (ELoad e) (at level 9, right associativity, format "! e").
  Notation "e1 <- e2" := (EStore e1 e2) (at level 80).
  Notation "'if:' e1 'then' e2 'else' e3" := (EIf e1 e2 e3)
    (at level 200, e1, e2, e3 at level 200).

  Notation "'match:' e 'with' 'InjL' x1 => e1 | 'InjR' x2 => e2 'end'" :=
    (EMatch e x1 e1 x2 e2)
    (e, x1, e1, x2, e2 at level 200,
     format "'[hv' 'match:'  e  'with'  '/  ' '[' 'InjL'  x1  =>  '/  ' e1 ']'  '/' '[' |  'InjR'  x2  =>  '/  ' e2 ']'  '/' 'end' ']'").
  Notation "'match:' e 'with' 'InjR' x1 => e1 | 'InjL' x2 => e2 'end'" :=
    (EMatch e x2 e2 x1 e1)
    (e, x1, e1, x2, e2 at level 200, only parsing).

  Notation "fun: x => e" := (ELam x e)
    (at level 200, x at level 1, e at level 200,
     format "'[' 'fun:'  x  =>  '/  ' e ']'").
  Notation "fun: x y .. z => e" := (ELam x (ELam y .. (ELam z e) ..))
    (at level 200, x, y, z at level 1, e at level 200,
     format "'[' 'fun:'  x  y  ..  z  =>  '/  ' e ']'").
  Notation "closure: x => e" := (VClosure x e)
    (at level 200, x at level 1, e at level 200,
     format "'[' 'closure:'  x  =>  '/  ' e ']'").
  Notation "closure: x y .. z => e" := (VClosure x (ELam y .. (ELam z e) ..))
    (at level 200, x, y, z at level 1, e at level 200,
     format "'[' 'closure:'  x  y  ..  z  =>  '/  ' e ']'").

  Notation "'rec:' f x => e" := (ERec f x e)
    (at level 200, f at level 1, x at level 1, e at level 200,
     format "'[' 'rec:'  f  x  =>  '/  ' e ']'").
  Notation "'rec:' f x y .. z => e" := (ERec f x (ELam y .. (ELam z e) ..))
    (at level 200, f, x, y, z at level 1, e at level 200,
     format "'[' 'rec:'  f  x  y  ..  z  =>  '/  ' e ']'").

  Notation "'recclosure:' f x => e" := (VRecClosure f x e)
    (at level 200, f at level 1, x at level 1, e at level 200,
     format "'[' 'recclosure:'  f  x  =>  '/  ' e ']'").
  Notation "'recclosure:' f x y .. z => e" :=
    (VRecClosure f x (ELam y .. (ELam z e) ..))
    (at level 200, f, x, y, z at level 1, e at level 200,
     format "'[' 'recclosure:'  f  x  y  ..  z  =>  '/  ' e ']'").

  Notation "e1 +: e2" := (EOp AddOp e1 e2) (at level 50, left associativity).
  Notation "e1 -: e2" := (EOp SubOp e1 e2) (at level 50, left associativity).
  Notation "e1 =: e2" := (EOp EqOp e1 e2) (at level 70, no associativity).
  Notation "e1 <=: e2" := (EOp LeOp e1 e2) (at level 70, no associativity).
  Notation "e1 <: e2" := (EOp LtOp e1 e2) (at level 70, no associativity).
End language_notation.

(** Define notations for WP and hoare. These are inspired by Iris. *)

Module hoare_notation.
  Notation "[{ P } ] e [{ v , Q } ]" := (hoare P e (fun v => Q))
    (at level 20, P, e, Q at level 200,
     format "'[' [{  P  } ] ']' '/  '  '[' e ']'  '/' '[' [{  v ,  Q  } ] ']'").

  Notation "'WP' e [{ v , Q } ]" := (wp e (fun v => Q))
    (at level 20, e, Q at level 200,
     format "'[hv' 'WP'  e  '/' [{  '[' v ,  '/' Q  ']' } ] ']'").
End hoare_notation.

(* ########################################################################## *)
(** Substitution *)
(* ########################################################################## *)

Fixpoint subst_map (vs : stringmap val) (e : expr) : expr :=
  match e with
  | EVal _ => e
  | EVar y =>
     match StringMap.lookup vs y with
     | Some v => EVal v
     | None => EVar y
     end
  | ELam y e => ELam y (subst_map (StringMap.delete y vs) e)
  | ERec f y e =>
     ERec f y (subst_map (StringMap.delete y (StringMap.delete f vs)) e)
  | EApp e1 e2 => EApp (subst_map vs e1) (subst_map vs e2)
  | EOp op e1 e2 => EOp op (subst_map vs e1) (subst_map vs e2)
  | EPair e1 e2 => EPair (subst_map vs e1) (subst_map vs e2)
  | EFst e => EFst (subst_map vs e)
  | ESnd e => ESnd (subst_map vs e)
  | EIf e1 e2 e3 => EIf (subst_map vs e1) (subst_map vs e2) (subst_map vs e3)
  | ESeq e1 e2 => ESeq (subst_map vs e1) (subst_map vs e2)
  | EAlloc e => EAlloc (subst_map vs e)
  | EStore e1 e2 => EStore (subst_map vs e1) (subst_map vs e2)
  | ELoad e => ELoad (subst_map vs e)
  | EFree e => EFree (subst_map vs e)
  end.

Lemma subst_map_empty e :
  subst_map StringMap.empty e = e.
Proof. induction e; simpl; by f_equal. Qed.

Lemma subst_map_insert x v e env :
  subst_map (StringMap.insert x v env) e
  = subst x v (subst_map (StringMap.delete x env) e).
Proof.
  revert env. induction e; intros env; simpl; try (by f_equal).
  - (** Case var *)
    rewrite StringMap.lookup_delete, StringMap.lookup_insert.
    destruct (String.eq_dec _ _); simplify_eq.
    + by destruct (String.eq_dec _ _).
    + destruct (StringMap.lookup env s) eqn:E; simpl; eauto.
      by destruct (String.eq_dec _ _).
  - (** Case lam *)
    destruct (String.eq_dec _ _); simplify_eq.
    + f_equal. f_equal. StringMap.map_solver.
    + f_equal. rewrite StringMap.delete_delete.
      destruct (String.eq_dec _ _); [done|].
      rewrite <-IHe. f_equal. StringMap.map_solver.
  - (** Case rec *)
    destruct (String.eq_dec _ _); simplify_eq.
    { do 2 f_equal. StringMap.map_solver. }
    destruct (String.eq_dec _ _); simplify_eq.
    { do 2 f_equal. StringMap.map_solver. }
    do 2 f_equal.
    do 2 rewrite StringMap.delete_insert, String.eq_dec_neq by congruence.
    rewrite IHe. do 2 f_equal. StringMap.map_solver.
Qed.

Lemma subst_map_singleton x v e :
  subst_map (StringMap.singleton x v) e = subst x v e.
Proof.
  rewrite <-StringMap.insert_empty, subst_map_insert.
  by rewrite StringMap.delete_empty, subst_map_empty.
Qed.

Lemma subst_subst_eq x v1 v2 e :
  subst x v1 (subst x v2 e) = subst x v2 e.
Proof.
  induction e; simpl;
    repeat (destruct (String.eq_dec _ _); simplify_eq);
    f_equal; by auto.
Qed.

Lemma subst_subst_neq x1 x2 v1 v2 e :
  x1 <> x2 ->
  subst x1 v1 (subst x2 v2 e) = subst x2 v2 (subst x1 v1 e).
Proof.
  intros. induction e; simpl;
    repeat (destruct (String.eq_dec _ _); simplify_eq);
    f_equal; by auto.
Qed.

(** * Lecture 4: Basic operational semantics *)
From pv Require Export library.

(* REMINDER:

          #####################################################
          ###  PLEASE DO NOT DISTRIBUTE SOLUTIONS PUBLICLY  ###
          #####################################################

*)

(* ########################################################################## *)
(** * Introduction *)
(* ########################################################################## *)

(** In the lecture, we will explore two different, but commonly used ways of
defining a semantics:

- Big-step semantics, by a relation:

    big_step : expr -> val -> Prop

  This relation relates expressions and their values. In mathematical notation,
  [big_step e v] is usually written as [e ⇓ v].

- Small-step semantics, by a relation:

    step : expr -> expr -> Prop

  This relation performs one step of execution, of which we then take the
  reflexive-transitive closure, which allows to take 0 or many steps. In
  mathematical notation, [step e1 e2] is usually denoted as [e1 ⇒ e2] and the
  reflexive-transitive closure as [e1 ⇒* e2].

As we will see throughout this lecture, there are some trade-offs between the
different forms of semantics:

- A big-step semantics corresponds more closely to the way we would write an
  interpreter, and is thus more intuitive to think of.
- A big-step semantics cannot distinguish programs that "go wrong" (e.g.,
  [1 + true]) and those that "loop" (e.g., [(λ x, x x) (λ x, xx)]), while a
  small-step semantics can distinguish those.

Furthermore, a small-step semantics is more suitable for non-deterministic
programming languages (e.g., languages with concurrency such as Rust).

We will formalize the big-step and small-step semantics for a small pure
functional language, and prove a correspondence between both semantics.

On the Coq side, we will see how inductively-defined relations can be used to
define these relations, and we see how we can perform induction and case
analysis (via the [induction] and [destruct]) on inductively-defined relations,
as well as the concept of inversion (via the [inv] tactic). To automate our
proofs we will explore Coq's [eauto] tactic. *)


(* ########################################################################## *)
(** * Call-by-value lambda calculus *)
(* ########################################################################## *)

(** We will formalize the semantics of a very simple language: Call-by-value
lambda calculus extended with the unit value, natural numbers and Booleans as
primitives. You can think of our language as being a small subset of the pure
fragment of a functional language like OCaml. The syntax of our language is:

  Operators:    op ::= + | - | <= | < | ==
  Expressions:   e ::= x | () | b | n | λ x. e | e1 e2 |
                       e1 `op` e2 | if e1 then e2 else e3

Here, [b] ranges over Booleans, [n] over natural numbers, and [x] over
variables, which are bound by the lambda construct. *)

(** We represent the syntax in Coq by a series of inductive data types. *)

Inductive bin_op :=
  | AddOp
  | SubOp
  | LeOp
  | LtOp
  | EqOp.

Inductive expr :=
  | EVar : string -> expr
  | EUnit : expr
  | EBool : bool -> expr
  | ENat : nat -> expr
  | ELam : string -> expr -> expr
  | EApp : expr -> expr -> expr
  | EOp : bin_op -> expr -> expr -> expr
  | EIf : expr -> expr -> expr -> expr.

(** We use strings to represent variables because they keep the syntax
conceptually simple, and allow us to write down concrete terms in a human
readable manner (compared to, say, De Bruijn indices). String in Coq have type
[string] and the usual syntax ["this is a string"] can be used to denote string
literals.

In week 11 we will use Coq's expressive notation machinery to define a parser
and pretty printer for our language, so that we can use notations that are close
to those on paper. In this lecture, we will not do that. Instead we will be
very explicit about the syntax trees that we use. *)

(** Now let us take a look at some examples. *)

(** [10 + 20 == 30] *)

Definition example1 : expr :=
  EOp EqOp
    (EOp AddOp (ENat 10) (ENat 20))
    (ENat 30).

(** [λ x, x + 20] *)

Definition example2 : expr :=
  ELam "x" (EOp AddOp (EVar "x") (ENat 20)).

(** [example2 10], i.e., [(λ x, x + 20) 10] *)

Definition example3 : expr :=
  EApp example2 (ENat 10).

(** [example2 10 == example2 20] *)

Definition example4 : expr :=
  EOp EqOp (EApp example2 (ENat 10)) (EApp example2 (ENat 20)).


(* ########################################################################## *)
(** * Values, substitution, and operator semantics *)
(* ########################################################################## *)

(** A crucial ingredient of a semantics is the notion of values, i.e., the
results that programs in our language have. The values of our language include
the unit value, Booleans, natural numbers, and lambdas. Note that lambdas are
values because they do not compute anything until they are given an argument
using function application. *)

Inductive val :=
  | VUnit : val
  | VBool : bool -> val
  | VNat : nat -> val
  | VLam : string -> expr -> val.

(** Values are a subset of expressions. We formalize this by defining an
injection of values into expressions. *)

Definition of_val (v : val) : expr :=
  match v with
  | VUnit => EUnit
  | VBool b => EBool b
  | VNat n => ENat n
  | VLam x e => ELam x e
  end.

(** We will use substitution to define the semantics of lambdas. Substitution
[subst x w e] replaces variables [x] by value [w] in the expression [e]. We
define substitution by structural recursion over the expression [e]. *)

Fixpoint subst (x : string) (w : val) (e : expr) : expr :=
  match e with
  | EUnit => EUnit
  | EBool b => EBool b
  | ENat n => ENat n
  | EVar y => if String.eq_dec x y then of_val w else EVar y
  | ELam y e => if String.eq_dec x y then ELam y e else ELam y (subst x w e)
  | EApp e1 e2 => EApp (subst x w e1) (subst x w e2)
  | EOp op e1 e2 => EOp op (subst x w e1) (subst x w e2)
  | EIf e1 e2 e3 => EIf (subst x w e1) (subst x w e2) (subst x w e3)
  end.

(** The definition of the substitution function [subst x w e] is mostly
straightforward, but there are two interesting cases:

- Free variables: We replace each free variable [EVar x] by [w].
- Bound variables: We only proceed in below a [ELam y e] in the expression
  if [x ≠ y]. This is to deal with shadowing in the correct way. *)

(** Let us see two examples: *)

(** Substituting 20 for [x] in [(λ y, x <= y) x] gives [(λ y, 20 <= y) 20]. *)

Example subst_test_1 :
  subst "x" (VNat 20)
    (EApp (ELam "y" (EOp LeOp (EVar "x") (EVar "y"))) (EVar "x"))
  = EApp (ELam "y" (EOp LeOp (ENat 20) (EVar "y"))) (ENat 20).
Proof. done. Qed.

(** Substituting 20 for [x] in [(λ x, 5 <= x) x] gives [(λ x, 5 <= x) 20].
This shows that the bound variable [x] is not replaced. *)

Example subst_test_2 :
  subst "x" (VNat 20)
    (EApp (ELam "x" (EOp LeOp (ENat 5) (EVar "x"))) (EVar "x"))
  = EApp (ELam "x" (EOp LeOp (ENat 5) (EVar "x"))) (ENat 20).
Proof. done. Qed.

(** We define a function [eval_bin_op] to define the semantics of binary
operations. This function uses [None] to handle errors, and [Some] to handle
actual results. An error occurs when an operator is applied to the wrong kind of
arguments, for example, when the arithmetic operator [<=] ([LeOp]) is applied
to Boolean arguments. *)

Definition eval_bin_op (op : bin_op) (v1 v2 : val) : option val :=
  match op, v1, v2 with
  | AddOp, VNat n1, VNat n2 => Some (VNat (n1 + n2))
  | SubOp, VNat n1, VNat n2 => Some (VNat (n1 - n2))
  | LeOp, VNat n1, VNat n2 => Some (VBool (Nat.leb n1 n2))
  | LtOp, VNat n1, VNat n2 => Some (VBool (Nat.ltb n1 n2))
  | EqOp, VUnit, VUnit => Some (VBool true)
  | EqOp, VNat n1, VNat n2 => Some (VBool (Nat.eqb n1 n2))
  | EqOp, VBool n1, VBool n2 => Some (VBool (Bool.eqb n1 n2))
  | _, _, _ => None
  end.


(* ########################################################################## *)
(** * Big-step semantics *)
(* ########################################################################## *)

(** We define the *big-step semantics* as an inductively-defined relation. *)

Inductive big_step : expr -> val -> Prop :=
  | Unit_big_step :
     big_step EUnit VUnit
  | Bool_big_step b :
     big_step (EBool b) (VBool b)
  | Nat_big_step n :
     big_step (ENat n) (VNat n)
  | Lam_big_step y e :
     big_step (ELam y e) (VLam y e)
  | App_big_step y e1 e1' e2 v2 v :
     big_step e1 (VLam y e1') ->
     big_step e2 v2 ->
     big_step (subst y v2 e1') v ->
     big_step (EApp e1 e2) v
  | Op_big_step e1 e2 op v1 v2 v :
     big_step e1 v1 ->
     big_step e2 v2 ->
     eval_bin_op op v1 v2 = Some v ->
     big_step (EOp op e1 e2) v
  | If_big_step_true e1 e2 e3 v :
     big_step e1 (VBool true) ->
     big_step e2 v ->
     big_step (EIf e1 e2 e3) v
  | If_big_step_false e1 e2 e3 v :
     big_step e1 (VBool false) ->
     big_step e3 v ->
     big_step (EIf e1 e2 e3) v.

(** In the definition of an inductive relation, such as the big-step semantics
above, one should really think the constructors as _inference rules_ (we write
[e ⇓ v] for [big_step e v] to improve readability):

  -------------------(Val_big_step)
   EBool b ⇓ VBool b

  -----------------(Nat_big_step)
   ENat n ⇓ VNat n

  ---------------------(Lam_big_step)
   ELam y e ⇓ VLam y e

   e1 ⇓ VLam y e1'    e2 ⇓ v2    subst y v2 e1' ⇓ v
  --------------------------------------------------(App_big_step)
                    EApp e1 e2 ⇓ v

   e1 ⇓ v1    e2 ⇓ v2    eval_bin_op op v1 v2 = Some v
  -----------------------------------------------------(Op_big_step)
                      EOp op e1 e2 ⇓ v

   e1 ⇓ VBool true     e2 ⇓ v
  ----------------------------(If_big_step_true)
        EIf e1 e2 e3 ⇓ v


   e1 ⇓ VBool false     e3 ⇓ v
  -----------------------------(If_big_step_true)
        EIf e1 e2 e3 ⇓ v

*)

(** Now let us take a look at the example programs we have seen before. We will
use the inference rules to prove that our sample programs evaluate to a
value. *)

Lemma big_step_example1 :
  big_step example1 (VBool true).
Proof.
  (** Expose the definition of [example1]: *)
  unfold example1.

  (** The rules of the operational semantics are just lemmas, so we can use the
  tactic [apply] to proceed. The rule that applies is [Op_big_step] for operators:

    Op_big_step e1 e2 op v1 v2 v :
      big_step e1 v1 ->
      big_step e2 v2 ->
      eval_bin_op op v1 v2 = Some v ->
      big_step (EOp op e1 e2) v.

  Let us try that: *)
  apply Op_big_step with (v1:=VNat 30) (v2:=VNat 30).
  (** It is important to note that we had to give [v1] and [v2], which are the
  resulting values of evaluating the subexpressions [10 + 20] and [30]. *)

  - (** Subexpression [10 + 20] *)
    apply Op_big_step with (v1:=VNat 10) (v2:=VNat 20).
    + apply Nat_big_step.
    + apply Nat_big_step.
    + done. (** [eval_bin_op] is a function, so [done] (which performs [simpl]
      implicitly) finishes this goal. *)
  - (** Subexpression [30] *)
    apply Nat_big_step.
  - done.
Qed.

(** In the above proof, we had to manually give the values of the subexpressions
as arguments to the [apply] tactic. For bigger examples, this quickly becomes
tedious, so let us take a look at another approach. *)

Lemma big_step_example1_eapply :
  big_step example1 (VBool true).
Proof.
  (** Again, we start by exposing the definition of [example1]. *)
  unfold example1.

  (** Instead of [apply], we now use [eapply]. *)
  eapply Op_big_step.

  (** After executing this tactic, we get goals:

  1. [big_step (EOp AddOp (ENat 10) (ENat 20)) ?v1]
  2. [big_step (ENat 30) ?v2]
  3. [eval_bin_op EqOp ?v1 ?v2 = Some (VBool true)]

  What we see here is something new, _existential variables_ (or _evars_ for
  short) [?v1] and [?v2]. Evars represent holes, that have to be instantiated
  at a later point while carrying out the proof. *)

  - (** We use [eapply] again for the nested binary operator. *)
    eapply Op_big_step.
    (** After this, we get evars [?v10] and [?v20] for the values of the
    sub-expressions [ENat 10] and [ENat 20] *)
    + eapply Nat_big_step.
      (** By [Nat_big_step] we instantiate [?v10] with [VNat 10]. *)
    + eapply Nat_big_step.
      (** By using [Nat_big_step], we instantiate [?v20] with [VNat 20]. *)
    + (** The next goal is

        [eval_bin_op AddOp (VNat 10) (VNat 20) = Some ?v1].

      Since [eval_bin_op] is a function, we can perform proof by computation,
      using [simpl], which will reduce the LHS to [Some (VNat 30)]. *)
      simpl.
      (** The goal now becomes [Some (VNat 30) = Some ?v1]. Through the
      [done] tactic, [?v1] will be instantiated with [VNat 30]. *)
      done.
      (** The rest of the proof proceeds in a similar way. *)

  - eapply Nat_big_step.
  - done.
Qed.

(** Lambdas are values, so they will only compute whenever they are applied to
a value. The following lemma is thus easy to prove, but not so useful. *)

Lemma big_step_example2_value :
  big_step example2 (VLam "x" (EOp AddOp (EVar "x") (ENat 20))).
Proof. apply Lam_big_step. Qed.

(** The following lemma is more useful. It says that whenever [example2] has
a natural number as argument, it will add 20 to it. *)

Lemma big_step_example2 n :
  big_step (EApp example2 (ENat n)) (VNat (n + 20)).
Proof.
  eapply App_big_step.
  - (** The lambda *) apply Lam_big_step.
  - (** The argument *) apply Nat_big_step.
  - (** The body of the lambda *)
    (** Our goal contains:

      subst "x" (VNat n) (EOp AddOp (EVar "x") (ENat 20))

    Since [subst] is a function, we can proceed by proof by computation. *)
    simpl.
    eapply Op_big_step; [apply Nat_big_step..|done].
Qed.

(** The program [example3] consists of a single invocation of [example2]. The
following lemma is an instance of the previous one. *)

Lemma big_step_example3 :
  big_step example3 (VNat 30).
Proof.
  unfold example3.
  apply big_step_example2.
Qed.

(** The program [example4] uses [example2] twice and compares the results.
We thus use the lemma [big_step_example2] twice. *)

Lemma big_step_example4 :
  big_step example4 (VBool false).
Proof.
  unfold example4.
  eapply Op_big_step.
  - apply big_step_example2.
  - apply big_step_example2.
  - done.
Qed.


(* ########################################################################## *)
(** * Automating proofs using [eauto] *)
(* ########################################################################## *)

(** The above proofs all follow a similar pattern: in most cases, there is
exactly one rule of the big-step semantics that can be applied, so why cannot we
instruct Coq to search for these proofs automatically?

To automate these proofs, we will use the [eauto] tactic, which performs a
"brute force" depth-first search, by trying to [eapply] individual lemmas from
a "hint database" of lemmas. It does do this in a depth-first fashion, i.e., it
will backtrack whenever multiple lemmas could be [eapply]ed (for example, in the
case of the conditional expression "if .. then .. else"). *)

Create HintDb big_step.

(** The command [Create HintDb] creates a "hint database" named [big_step],
which we will fill up with a number of lemmas. These lemmas will be used by
[eauto with big_step]. *)

(** As the command [Print HintDb exec] illustrates, our hint database is
initially empty. *)

Print HintDb big_step.

(** We populate the hint database with the constructors of the inductive
relation [big_step] (i.e., all the inference rules of the big-step
semantics). *)

Global Hint Resolve Unit_big_step : big_step.
Global Hint Resolve Bool_big_step : big_step.
Global Hint Resolve Nat_big_step : big_step.
Global Hint Resolve Lam_big_step : big_step.
Global Hint Resolve App_big_step : big_step.
Global Hint Resolve Op_big_step : big_step.
Global Hint Resolve If_big_step_true : big_step.
Global Hint Resolve If_big_step_false : big_step.

(** We check that the hint database is populated. *)

Print HintDb big_step.

(** Let us see how to use [eauto] in practice. *)

Lemma big_step_example :
  big_step (EIf (EBool true) (ENat 10) (ENat 11)) (VNat 10).
Proof.
  (** We invoke [eauto] with the hint database. *)
  eauto with big_step.

  (** The above tactic solved the goal, but it gives us no idea how. To see what
  is going on, we can use the [debug] keyword. *)

Restart.
  debug eauto with big_step.

  (** Prints:

    1 depth=5
    1.1 depth=4 simple apply If_big_step_false
    1.2 depth=4 simple apply If_big_step_true
    1.2.1 depth=4 simple apply Val_big_step
    1.2.1.1 depth=4 simple apply Val_big_step *)

  (** We can see here that [eauto] performs backtracking: it first tries the
  lemma [If_big_step_false], and notices that it fails, and in turn, uses the
  lemma [If_big_step_true]. *)
Qed.

(** As we have seen in our manual proofs (e.g., [big_step_example1]), we not
just used [eapply], but we also:

- Proved goals of the shape [eval_bin_op op v1 v1 = Some ?v] by computation.
- Simplified substitutions through computation.

Using the [Hint Extern] command, we instruct [eauto] to run a tactic when the
goal is of a certain shape. The number [10] is the "cost": a lower number means
that the hint is applied more eagerly. For lemmas added with [Hint Resolve] the
cost is the number of premises (i.e., number of arrows). *)

Global Hint Extern 1 (eval_bin_op _ _ _ = Some _) =>
  done : big_step.
Global Hint Extern 1 (big_step (subst _ _ _) _) =>
  progress simpl : big_step.

(** Note that the [progress] tactic is necessary in the last hint: [simpl] is a
no-op if there is nothing to be simplified. Hence the hint can be applied
infinitely many times after each other. With [progress] we make sure that the
hint is only applied if there is actually something that can be [simpl]ed. In
particular, this makes sure the hint is not used twice after each other. *)

(** Let us test that the above [Hint Extern]s were actually registered. *)

Print HintDb big_step.

(** Let us put this machinery in action. *)

Lemma big_step_example1_eauto : big_step example1 (VBool true).
Proof.
  unfold example1.

  debug eauto with big_step.
  (**
  Prints:

    1 depth=5
    1.1 depth=4 simple eapply Op_big_step
    1.1.1 depth=3 simple eapply Op_big_step
    1.1.1.1 depth=3 simple apply Nat_big_step
    1.1.1.1.1 depth=3 simple apply Nat_big_step
    1.1.1.1.1.1 depth=3 (*external*) done
    1.1.1.1.1.1.1 depth=3 simple apply Nat_big_step
    1.1.1.1.1.1.1.1 depth=3 simple apply @eq_refl
  *)
Qed.

Lemma big_step_example2_eauto n :
  big_step (EApp example2 (ENat n)) (VNat (n + 20)).
Proof.
  unfold example2.
  debug eauto with big_step.
Qed.

Lemma big_step_example3_eauto :
  big_step example3 (VNat 30).
Proof.
  unfold example3.
  (** The [eauto] tactic will unfold definitions. It thus unfolds [example2] and
  finds the same proof as it found for the previous lemma. *)
  debug eauto with big_step.
Qed.

Lemma big_step_example4_eauto :
  big_step example4 (VBool false).
Proof.
  unfold example4.
  debug eauto with big_step.

  (** This did not do anything. The problem is that we reached the maximal
  search depth of [eauto]s depth-first search, which is 5 by default. In the
  output we see that it reaches [depth=0] and stops:

    1 depth=5
    1.1 depth=4 simple eapply Op_big_step
    1.1.1 depth=3 simple eapply App_big_step
    1.1.1.1 depth=3 simple apply Lam_big_step
    1.1.1.1.1 depth=3 simple apply Nat_big_step
    1.1.1.1.1.1 depth=2 (*external*) (progress simpl)
    1.1.1.1.1.1.1 depth=1 simple eapply Op_big_step
    1.1.1.1.1.1.1.1 depth=1 simple apply Nat_big_step
    1.1.1.1.1.1.1.1.1 depth=1 simple apply Nat_big_step
    1.1.1.1.1.1.1.1.1.1 depth=1 (*external*) done
    1.1.1.1.1.1.1.1.1.1.1 depth=0 simple eapply App_big_step
    1.1.1.1.1.2 depth=2 simple eapply Op_big_step
  *)

  (** We increase the depth via [eauto D] where [D] is the depth. *)
  debug eauto 10 with big_step.
Restart.
  (** Alternatively, we can add [big_step_example2] as a hint. We can do that
  using [Hint Resolve] to add it to the hint database, or we can use the [with]
  flag of [eauto]. When using this additional hint, the default depth of 5 is
  sufficient. *)

  debug eauto using big_step_example2 with big_step.
Qed.


(* ########################################################################## *)
(** * Exercises about syntax and big-step semantics *)
(* ########################################################################## *)

(** Most functional languages feature let-bindings. We can model these using
lambdas and function application. *)

Definition ELet (x : string) (e1 e2 : expr) : expr :=
  EApp (ELam x e2) e1.

(** Prove a big-step rule for let-expressions. First give a proof using [apply],
then use [eapply], and finally using [eauto]. *)

Lemma Let_big_step y e1 e2 v1 v2 :
  big_step e1 v1 ->
  big_step (subst y v1 e2) v2 ->
  big_step (ELet y e1 e2) v2.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** Turn the following expressions into corresponding [expr]s. *)

(** [let x = (let y = 1 + 1 in y) in x + x] *)
Definition example5 : expr. (* FILL IN HERE *) Admitted.

(** [λ x y, if y <= x then x else y] *)
Definition example6 : expr. (* FILL IN HERE *) Admitted.

(** The following lemma can also be proven via either explicit [eapply]s and
[eauto]. Make sure to practice with both. *)

Lemma big_step_example5 :
  big_step example5 (VNat 4).
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

(** The following lemma requires a manual proof, .i.e., cannot be fully solved
using [eauto]. Some hints:

- Before applying the [if]-rules, you need to make a case distinction to
  determine whether the condition is true or false. For this, you can use
  [destruct (n2 <=? n1) eqn:Hn]. See week 2 on [destruct] with equations.
- The [lia] tactic also supports goals involving the Boolean functions [=?],
  [<=?], [<?], as well as the [min] and [max] functions. The [replace] tactic
  in combination with [lia] could come in handy, see als week 2. *)

Lemma big_step_example6 n1 n2 :
  big_step (EApp (EApp example6 (ENat n1)) (ENat n2)) (VNat (max n1 n2)).
Proof. (* FILL IN HERE (13 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Determinism of the big-step semantics *)
(* ########################################################################## *)

(** The idea of the big-step semantics [e ⇓ v] is that it associates to each
expression [e] its value [v]. We will now establish that this value is
unique, i.e., that the big-step semantics is _deterministic_. *)

Lemma big_step_deterministic e v1 v2 :
  big_step e v1 ->
  big_step e v2 ->
  v1 = v2.
Proof.
  intros Hstep1.

  (** We have to generalize the induction hypothesis, because [v2] will differ
  each time we need the induction hypothesis. *)
  revert v2.

  (** Since we have defined the big-step semantics using an inductive relation,
  we can perform induction over the derivation. We do this using the [induction]
  tactic. Before doing that, let us take a look at the induction scheme for
  [big_step] *)

  Check big_step_ind.

  (** Formatted in a more readable way as an inference rule:

    P EUnit VUnit ->

    (forall b : bool,
      P (EBool b) (VBool b))

    (forall n : nat,
      P (ENat n) (VNat n))

    (forall (y : string) (e : expr),
      P (ELam y e) (VLam y e))

    (forall (y : string) (e1 e1' e2 : expr) (v2 v : val),
       big_step e1 (VLam y e1') ->
       P e1 (VLam y e1') ->
       big_step e2 v2 ->
       P e2 v2 ->
       big_step (subst y v2 e1') v ->
       P (subst y v2 e1') v ->
       P (EApp e1 e2) v)

    (forall e1 e2 op v1 v2 v,
       big_step e1 v1 ->
       P e1 v1 ->
       big_step e2 v2 ->
       P e2 v2 ->
       eval_bin_op op v1 v2 = Some v ->
       P (EOp op e1 e2) v)

    (forall e1 e2 e3 v,
       big_step e1 (VBool true) ->
       P e1 (VBool true) ->
       big_step e2 v ->
       P e2 v ->
       P (EIf e1 e2 e3) v)

    (forall e1 e2 e3 v,
       big_step e1 (VBool false) ->
       P e1 (VBool false) ->
       big_step e3 v ->
       P e3 v ->
       P (EIf e1 e2 e3) v)

   -------------------------------------------

    forall e v, big_step e v -> P e v
  **)
  induction Hstep1; intros v' Hstep2.
  - (** Base case for Booleans *)
    (** We have the following hypothesis:

      Hstep2 : big_step EUnit v'

    By looking at the constructors of [big_step], we know that the only way to
    derive [big_step EUnit v'] is by means of the constructor [Unit_big_step],
    which means that [v'] should be [VUnit]. We use the [inv] tactic to obtain
    that information via the principle of "inversion". *)
    inv Hstep2.
    done.
  - (** Base case for Booleans *)
    by inv Hstep2.
  - (** Base case for natural numbers *)
    by inv Hstep2.
  - (** Base case for lambdas *)
    by inv Hstep2.
  - (** Inductive case for application *)
    (** This case is similar to the previous ones, but more complicated. We have
    the following hypothesis:

      big_step (EApp e1 e2) v'

    How can [EApp e1 e2] result into a value [v']? Whe only way is via the
    constructor [App_big_step], which means that [e1] should evaluate to lambda,
    [e2] to its argument, and [v'] be the result. Again we use inversion to
    figure that out. *)
    inv Hstep2.
    (** We have [big_step e1 (VLam y e1')] and [big_step e1 (VLam y0 e1'0)], so
    by the induction hypothesis [VLam y e1' = VLam y0 e1'0]. By injectivity
    of constructors (via [simplify_eq]), we obtain [y = y0] and [e1' = e1'0]. *)
    apply IHHstep1_1 in H1. simplify_eq.
    (** And the same for [big_step e2 ..]. *)
    apply IHHstep1_2 in H2. simplify_eq.
    (** And once more for [big_step (subst y0 v0 e1'0) ..]. *)
    by apply IHHstep1_3.
  - (** Inductive case for operators *)
    inv Hstep2.
    apply IHHstep1_1 in H3.
    apply IHHstep1_2 in H5. by simplify_eq.
  - (** Inductive case for if-true *)
    inv Hstep2.
    + by apply IHHstep1_2.
    + by apply IHHstep1_1 in H3.
  - (** Inductive case for if-false *)
    inv Hstep2.
    + by apply IHHstep1_1 in H3.
    + by apply IHHstep1_2.
Qed.

(** In the above proof we used [induction] without [as] argument and thus relied
on names that were automatically generated. We recommended against this practice
in week 1 and 2 when we performed induction on natural numbers and lists because
it leads to fragile proofs. However, when performing induction on big inductive
types or inductive relations, naming the arguments using an [as] clause is not a
satisfactory solution either. In the above proof, you would end up with
something like:

  induction Hstep1 as [b
                      |n
                      |y e
                      |y e1 e1' e2 v2 v2 He1 IH1 He2 IH2 He IHe
                      |e1 e2 op v1 v2 v He1 IH1 He2 IH2 Hop
                      |e1 e2 e3 v He1 IH1 He2 IH2
                      |e1 e2 e3 v He1 IH1 He3 IH3]

It is hard to argue that this is less fragile than relying on automatically
generated names. Unfortunately, Coq does not have a good solution to deal with
this situation. (We will see some workarounds throughout this course.) *)


(* ########################################################################## *)
(** * Shortcomings of the big-step semantics *)
(* ########################################################################## *)

(** The big-step semantics is intuitive and relatively easy for proofs, but it
also has a major shortcoming. It cannot distinguish programs that are unsafe/go
wrong and programs that diverge. When studying the semantics of real-life
programming languages, this is not a theoretical issue, but a problem in
practice:

1. To implement certain algorithms, e.g., unbounded search algorithms or semi
   decision procedures, divergence for some inputs is a valid behavior. Such
   programs should have a distinct semantics from programs that are unsafe.
2. We do not want a compiler to translate a diverging programs into an unsafe
   program (i.e., a program that crashes). If diverging and unsafe programs have
   the same semantics, a compiler would be allowed to do this. *)

(** Let use take a look at two examples. *)

(** The first example is unsafe: The addition operator is applied to a natural
number and a Boolean. Adding arguments of different types makes no sense (unless
Javascript is your favorite language :)). *)

Definition example_unsafe : expr :=
  EOp AddOp (ENat 10) (EBool true).

(** The second example diverges. It is the well-known looping combinator
[Ω := (λ x, x x) (λ x, xx)] from lambda calculus. *)

Definition omega : expr :=
  ELam "x" (EApp (EVar "x") (EVar "x")).
Definition Omega : expr :=
  EApp omega omega.

(** For both the expressions [example_unsafe] and [Omega] we can prove that
there is no value [v] to which the expression evaluates according to the
big-step semantics. *)

(** The first result is proved using a series of inversions. *)

Lemma big_step_example_unsafe v :
  ~big_step example_unsafe v.
Proof.
  intros H. unfold example_unsafe.
  inv H.
  inv H3.
  inv H5.
Qed.

(** The second result requires a bit more work. We need to perform induction on
the big-step semantics. Making use of the fact that [Omega] reduces to itself, we
should be able to use the induction hypothesis. *)

Lemma big_step_Omega v :
  ~big_step Omega v.
Proof.
  (** Unfortunately, performing induction immediately does not work. Coq picks
  the induction predicate [P e v := ~big_step e v] and forgets that [e] should
  be [Omega]. *)
  intros Hstep. induction Hstep.
Restart.
  (** We can fix this by generalizing the induction ourselves. *)
  (** As a side note, using [assert] you can make "local" lemmas. *)
  assert (forall e, big_step e v -> e <> Omega) as help.
  { intros e Hstep.
    (** Now the induction predicate is [P e v := big_step e v -> e <> Omega]. *)
    (** Since [Omega] is an application, all cases aside from the [App_big_step]
    are contradictions; [simplify_eq] will discharge those. *)
    induction Hstep; intros He; simplify_eq.
    unfold Omega in He. simplify_eq.
    inv Hstep1.
    inv Hstep2.
    by apply IHHstep3. }
  intros Hstep. by apply (help Omega).
Qed.


(* ########################################################################## *)
(** * Small-step semantics *)
(* ########################################################################## *)

(** We will now give a small-step semantics for our language. We define the
small-step semantics in three stages: *)

(** 1. First we define the _head reduction_ relation. This relation performs a
_single_ step of computation for the top-most construct. *)

Inductive head_step : expr -> expr -> Prop :=
  | App_head_step y e1 v2 :
     head_step (EApp (ELam y e1) (of_val v2)) (subst y v2 e1)
  | Op_head_step op v1 v2 v :
     eval_bin_op op v1 v2 = Some v ->
     head_step (EOp op (of_val v1) (of_val v2)) (of_val v)
  | If_head_step_true e2 e3 :
     head_step (EIf (EBool true) e2 e3) e2
  | If_head_step_false e2 e3 :
     head_step (EIf (EBool false) e2 e3) e3.

(** 2. We then define the _whole program reduction_ relation. This relation
performs a _single_ step of computation, but this step may happen anywhere in
the term.

In the small-step operational semantics there is a clear order of which
subexpression is evaluated first. Similar to OCaml, we evaluate arguments from
right to left. For example, if we have [e1 + e2], we first evaluate [e2] and
then [e1]. For a pure language the order of evaluation is immaterial, but
when we consider a language with mutable state nextd year (or in fact, any form
of side-effects), the order of evaluation matters. *)

Inductive step : expr -> expr -> Prop :=
  | do_head_step e e' :
     head_step e e' -> step e e'
  | App_step_r e1 e2 e2' :
     step e2 e2' -> step (EApp e1 e2) (EApp e1 e2')
  | App_step_l e1 v2 e1' :
     step e1 e1' -> step (EApp e1 (of_val v2)) (EApp e1' (of_val v2))
  | Op_step_r op e1 e2 e2' :
     step e2 e2' -> step (EOp op e1 e2) (EOp op e1 e2')
  | Op_step_l op e1 v2 e1' :
     step e1 e1' -> step (EOp op e1 (of_val v2)) (EOp op e1' (of_val v2))
  | If_step e1 e1' e2 e3 :
     step e1 e1' -> step (EIf e1 e2 e3) (EIf e1' e2 e3).

(** Convince yourself why there is only a single rule for [EIf]. *)

(** 3. Finally, we define the _reflexive/transitive_ closure of the reduction
relation. This relation perform 0 or many steps. *)

Inductive steps : expr -> expr -> Prop :=
  | steps_refl e :
     steps e e
  | steps_step e1 e2 e3 :
     step e1 e2 ->
     steps e2 e3 ->
     steps e1 e3.


(* ########################################################################## *)
(** * Exercise: Correspondence between big-step and small-step semantics *)
(* ########################################################################## *)

(** In this exercise you are going to show that these two styles of semantics
coincide. Formally speaking, you will prove:

      steps e (of_val v) <-> big_step e v

This will be expressed by the lemmas for both directions of the bi-implication:

1. [steps_val_big_step : forall e v, steps e (of_val v) -> big_step e v]
2. [big_step_steps_val : forall e v, big_step e v -> steps e (of_val v)]
*)


(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *)
(** ** Direction 1 *)
(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *)

(** We first prove the left-to-right direction:

  steps e (of_val v) -> big_step e v

We will do this in several steps.

1. [head_step_big_step : head_step e1 e2 -> big_step e2 v -> big_step e1 v]
2. [step_big_step : step e1 e2 -> big_step e2 v -> big_step e1 v]
3. [steps_big_step : steps e1 e2 -> big_step e2 v -> big_step e1 v]
4. [steps_val_big_step : steps e (of_val v) -> big_step e v].

In words: we show that the big-step semantics is preserved by head reduction
(1), then by whole program reduction (2), and finally by multiple steps of
program reduction (3), to obtain the final result (4). *)

(** You should prove some helper lemmas. *)

Lemma of_val_big_step v :
  big_step (of_val v) v.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma of_val_big_step_inv v1 v2 :
  big_step (of_val v1) v2 -> v1 = v2.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** We add the last lemma to the hint database so [eauto] can use it. *)

Global Hint Resolve of_val_big_step : big_step.

(** Prove the lemmas below. It is typically a good idea---before starting to
write any Coq code---to carefully think how you would do the proof! You need a
variety of proof techniques:

- Case analysis on the head reduction relation [head_step].
- Induction on the whole program reduction relation [step].
- Induction on the reflexive/transitive closure [steps].
- Inversion on the big-step semantics [big_step].
- Generalizing the induction hypothesis.

Also, do not forget to use the lemmas that you have already proved. *)

Lemma head_step_big_step e1 e2 v :
  head_step e1 e2 ->
  big_step e2 v ->
  big_step e1 v.
Proof. (* FILL IN HERE (11 LOC proof) *) Admitted.

Lemma step_big_step e1 e2 v :
  step e1 e2 ->
  big_step e2 v ->
  big_step e1 v.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma steps_big_step e1 e2 v :
  steps e1 e2 ->
  big_step e2 v ->
  big_step e1 v.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma steps_val_big_step e v :
  steps e (of_val v) ->
  big_step e v.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.


(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *)
(** ** Direction 2 *)
(* ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ *)

(** We now prove the right-to-left direction:

  big_step_steps_val : big_step e v -> steps e (of_val v) *)

(** Prove the lemmas below. Hint: As for the direction 1, the lemmas build on
top of each other. *)

Lemma steps_trans e1 e2 e3 :
  steps e1 e2 ->
  steps e2 e3 ->
  steps e1 e3.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma App_steps_r e1 e2 e2' :
  steps e2 e2' ->
  steps (EApp e1 e2) (EApp e1 e2').
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma App_steps_l e1 v2 e1' :
  steps e1 e1' ->
  steps (EApp e1 (of_val v2)) (EApp e1' (of_val v2)).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Op_steps_r op e1 e2 e2' :
  steps e2 e2' ->
  steps (EOp op e1 e2) (EOp op e1 e2').
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Op_steps_l op e1 v2 e1' :
  steps e1 e1' ->
  steps (EOp op e1 (of_val v2)) (EOp op e1' (of_val v2)).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma If_steps e1 e1' e2 e3 :
  steps e1 e1' ->
  steps (EIf e1 e2 e3) (EIf e1' e2 e3).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma big_step_steps_val e v :
  big_step e v ->
  steps e (of_val v).
Proof. (* FILL IN HERE (24 LOC proof) *) Admitted.

Lemma steps_val_unique e v1 v2 :
  steps e (of_val v1) ->
  steps e (of_val v2) ->
  v1 = v2.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Challenge exercise: Unsafe and diverging programs *)
(* ########################################################################## *)

(** With the small-step semantics we can define what it means for a program to
be unsafe and to diverge. *)

(** An expression [e] is said to be "unsafe" if it reduces to an expression [e']
that is neither a value nor can reduce further. *)

Definition unsafe (e : expr) :=
  exists e',
    steps e e' /\
    ~(exists v, e' = of_val v) /\
    ~(exists e'', step e' e'').

(** An expression [e] is said to "diverge" if there exists an infinite reduction
sequence [e ⇒ e1 ⇒ e2 ⇒ e3 ⇒ ..]. *)

Definition diverges (e : expr) :=
  exists (f : nat -> expr),
    f 0 = e /\
    forall i, step (f i) (f (S i)).

(** Prove that [example_unsafe] is unsafe. *)

Lemma unsafe_example_unsafe :
  unsafe example_unsafe.
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

(** Prove that [Omega] diverges. *)

Lemma diverges_Omega:
  diverges Omega.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

(** Prove that programs in our language cannot be unsafe and diverge at the
same time. This is a very tricky result to prove, which might require a number
of helper lemmas. Some of these lemmas involve many cases. (You can try to
search the Coq reference manual for the [Ltac] construct [match goal] to
automate your proofs.

To figure out what helper lemmas you need, convince yourself that this is not a
property of every programming language. It might be helpful to come up with a
program that can both diverge and lead to unsafety in a language such as C or
C++. Convince yourself that you cannot write such programs in our language and
figure out due to which property is responsible for that. You might need to
prove that property. *)

Lemma diverges_unsafe e :
  unsafe e ->
  diverges e ->
  False.
Proof. (* FILL IN HERE (43 LOC helpers and 3 LOC proof) *) Admitted.

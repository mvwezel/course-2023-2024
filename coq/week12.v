(** * Lecture 12: Semantic substructural typing *)
From pv Require Import proofmode.

(* REMINDER:

          #####################################################
          ###  PLEASE DO NOT DISTRIBUTE SOLUTIONS PUBLICLY  ###
          #####################################################

*)

(* ########################################################################## *)
(** * Introduction *)
(* ########################################################################## *)

(** See the file [week12.md] in the repository for the theoretical background
of this lecture. *)


(* ########################################################################## *)
(** * Language extensions *)
(* ########################################################################## *)

(** As discussed in the lecture, we need a pairing-let construct and load
operation that gives back the reference. We define both as functions in our
language, provide a convenient notation, and prove the WP rules. *)

Definition pair_elim :=
  VClosure "f" (ELam "x"
    (EApp (EApp (EVar "f") (EFst (EVar "x"))) (ESnd (EVar "x")))).

Definition lin_load :=
  VClosure "l" (EPair (EVar "l") (ELoad (EVar "l"))).

Notation ELetPair x1 x2 e1 e2 :=
  (EApp (EApp (EVal pair_elim) (ELam x1 (ELam x2 e2))) e1).
Notation ELinLoad e :=
  (EApp (EVal lin_load) e).

Lemma LetPair_wp Phi x1 x2 v1 v2 e :
  wp (subst x1 v1 (subst x2 v2 e)) Phi |~
  wp (ELetPair x1 x2 (EVal (VPair v1 v2)) e) Phi.
Proof.
  iIntros "Hwp". unfold pair_elim. wp_pure!.
  destruct (String.eq_dec x2 x1) as [->|?].
  - wp_pure!. by rewrite subst_subst_eq.
  - wp_pure!. by rewrite subst_subst_neq by congruence.
Qed.

Lemma LinLoad_wp Phi l v :
  l ~> v ** (l ~> v -** Phi (VPair (VRef l) v)) |~
  wp (ELinLoad (EVal (VRef l))) Phi.
Proof.
  iIntros "[Hl HPhi]". unfold lin_load. wp_pure!. wp_load!. by iApply "HPhi".
Qed.


(* ########################################################################## *)
(** * Semantic definitions *)
(* ########################################################################## *)

(** The semantic definition of types type formers as explained in the lecture. *)

Definition ty := val -> sepProp.

Definition TMoved : ty :=
  fun v => EMP.

Definition TUnit : ty :=
  fun v => @[ v = VUnit ].
Definition TNat : ty :=
  fun v => Ex n, @[ v = VNat n ].
Definition TBool : ty :=
  fun v => Ex b, @[ v = VBool b ].

Definition TRef (A : ty) : ty :=
  fun v => Ex l w, @[ v = VRef l ] ** l ~> w ** A w.

Definition TProd (A1 A2 : ty) : ty :=
  fun v => Ex w1 w2, @[ v = VPair w1 w2 ] ** A1 w1 ** A2 w2.

Definition TFun (A1 A2 : ty) : ty :=
  fun v => @[ forall w, A1 w |~ wp (EApp (EVal v) (EVal w)) A2 ].
Definition TFunOnce (A1 A2 : ty) : ty :=
  fun v => All w, A1 w -** wp (EApp (EVal v) (EVal w)) A2.

(** Intuitively, contexts are multisets of bindings (since variables can occur
multiple times). We formalize contexts in Coq as lists. The lemma [subctx_swap]
below shows that the order does not matter. *)

Notation ty_ctx := (list (string * ty)).

(** The domain of the context. *)

Definition ctx_dom : ty_ctx -> list string := map fst.

(** Similar to week 5, we define semantic typing for contexts,
[ctx_typed Gamma vs] to say that all the variables in an environment [vs] have
types given by the type context [Gamma]. Since our types are predicates in
separation logic, [ctx_typed] is a [sepProp]. We use the separating conjunction
to ensure the ownership of all variables in the context is separate. *)

Fixpoint ctx_typed (Gamma : ty_ctx) (vs : stringmap val) : sepProp :=
  match Gamma with
  | [] => EMP
  | (x,A) :: Gamma =>
     (Ex v, @[ StringMap.lookup vs x = Some v ] ** A v) **
     ctx_typed Gamma vs
  end.

(** The semantic typing judgments are defined similarly to the ones from week 5,
but using separation logic entailment [|~] instead of a Coq implication. *)

Definition val_typed (v : val) (A : ty) : Prop :=
  EMP |~ A v.
  
Definition typed (Gamma : ty_ctx) (e : expr) (A : ty) : Prop :=
  forall vs,
    ctx_typed Gamma vs |~ wp (subst_map vs e) A.

Definition copy (A : ty) : Prop :=
  forall v, A v |~ @[ EMP |~ A v ].

Definition subty (A1 A2 : ty) : Prop :=
  forall v, A1 v |~ A2 v.

(** We lift subtyping to contexts. This allows us to:

1. Have a stronger subsumption rule that also allows subtyping of the variable
   context, see [subsumption].
2. Formalize that the list is in fact a set (i.e., the order is not relevant),
   see [subctx_swap].
3. Lift the weakening and contraction rules to context subtyping, see
   [subctx_copy_weakening] and [subctx_copy_contraction]. *)

Definition subctx (Gamma1 Gamma2 : ty_ctx) : Prop :=
  forall vs, ctx_typed Gamma1 vs |~ ctx_typed Gamma2 vs.


(* ########################################################################## *)
(** * Semantic typing rules *)
(* ########################################################################## *)

(** With the definition of the semantic type system at hand, we will prove all
typing rules as lemmas. In this section, we show a number of sample proofs. Go
carefully through them, since they contain a number of tricks that you will need
for the exercises. *)

(** But first, we need to prove some basic properties of context typing. We
prove that we can split the context using [++] iff we can take context typing
apart using [**]. These lemmas will be used in the proofs of the typing rules
the binary language constructor. *)

Lemma ctx_typed_app Gamma1 Gamma2 vs :
  ctx_typed Gamma1 vs ** ctx_typed Gamma2 vs |~
  ctx_typed (Gamma1 ++ Gamma2) vs.
Proof.
  iIntros "Hctx". iInduction Gamma1 as [|[x A] Gamma] "IH"; simpl.
  { by iDestruct "Hctx" as "[_ ?]". }
  iDestruct "Hctx" as "[[$ Hctx1] Hctx2]". iApply "IH". iFrame.
Qed.

Lemma ctx_typed_app_inv Gamma1 Gamma2 vs :
  ctx_typed (Gamma1 ++ Gamma2) vs |~
  ctx_typed Gamma1 vs ** ctx_typed Gamma2 vs.
Proof.
  iIntros "Hctx". iInduction Gamma1 as [|[x A] Gamma] "IH"; simpl.
  { by iFrame. }
  iDestruct "Hctx" as "[$ Hctx]". by iApply "IH".
Qed.


(** We can extend the environment [vs] with a new binding, as long as it is
fresh. *)

Lemma ctx_typed_insert Gamma vs x v :
  ~In x (ctx_dom Gamma) ->
  ctx_typed Gamma vs |~ ctx_typed Gamma (StringMap.insert x v vs).
Proof.
  iIntros (Hfresh) "Hctx".
  iInduction Gamma as [|[y A] Gamma] "IH"; simpl in *; [done|].
  iDestruct "Hctx" as "[(%w & %Hlookup & HA) Hctx]". iSplitL "HA".
  - iExists w. iFrame "HA". iPureIntro.
    rewrite StringMap.lookup_insert.
    destruct (String.eq_dec _ _) as [->|]; [|done].
    destruct Hfresh; auto.
  - iApply ("IH" with "[%] Hctx"). tauto.
Qed.

Lemma Closure_typed x e A1 A2 :
  typed [(x,A1)] e A2 ->
  val_typed (VClosure x e) (TFun A1 A2).
Proof.
  unfold typed, val_typed.
  iIntros (He) "_".
  unfold TFun.
  (** We have a [ @[ ... ] ] in an Iris Proof Mode goal. We can use [iPureIntro]
  to introduce it. A shorter way is to use the [ !% ] introduction pattern. *)
  iIntros "!% %w HA1". wp_pure!.
  (** We need to turn the [subst] into a [substmap] so that we can [iApply]
  the hypothesis [He]. *)
  rewrite <-subst_map_singleton. iApply He. simpl.
  rewrite StringMap.lookup_singleton, String.eq_dec_eq. eauto.
Qed.

Lemma App_typed Gamma1 Gamma2 e1 e2 A B :
  typed Gamma1 e1 (TFunOnce A B) ->
  typed Gamma2 e2 A ->
  typed (Gamma1 ++ Gamma2) (EApp e1 e2) B.
Proof.
  (** Always start your proof by unfolding the typing judgments. Then it becomes
  clear what to introduce/eliminate based on your goal and hypotheses. *)
  unfold typed.
  iIntros (He1 He2 vs) "Hctx".
  (* The next step is to always use [simpl] to simplify the [subst_map]. *)
  simpl.
  (** Then take [ctx_typed] apart using [ctx_typed_app_inv] *)
  iDestruct (ctx_typed_app_inv with "Hctx") as "[Hctx1 Hctx2]".
  (** Apply the WPs for the subexpressions *)
  iApply (He2 with "Hctx2"); iIntros (v2) "HA".
  iApply (He1 with "Hctx1"); iIntros (v1) "Hv1".
  (** And finish your proof. *)
  unfold TFunOnce.
  by iApply "Hv1".
Qed.

Lemma Lam_typed Gamma e x A1 A2 :
  ~In x (ctx_dom Gamma) ->
  typed ((x,A1) :: Gamma) e A2 ->
  typed Gamma (ELam x e) (TFunOnce A1 A2).
Proof.
  unfold typed. iIntros (? He vs) "Hctx"; simpl. wp_pure!.
  iIntros (w) "HA". wp_pure!.
  (** We need to turn the [subst] into a [substmap] so that we can [iApply]
  the hypothesis [He]. *)
  rewrite <-subst_map_insert.
  iApply He; simpl. iSplitL "HA".
  { iExists w. iFrame. iPureIntro. StringMap.map_solver. }
  by iApply ctx_typed_insert.
Qed.

Lemma If_typed Gamma1 Gamma2 e1 e2 e3 B :
  typed Gamma1 e1 TBool ->
  typed Gamma2 e2 B ->
  typed Gamma2 e3 B ->
  typed (Gamma1 ++ Gamma2) (EIf e1 e2 e3) B.
Proof.
  unfold typed. iIntros (He1 He2 He3 vs) "Hctx"; simpl.
  iDestruct (ctx_typed_app_inv with "Hctx") as "[Hctx1 Hctx2]".
  iApply (He1 with "Hctx1"); iIntros (v1) "[%b ->]". destruct b.
  - wp_pure!. by iApply He2.
  - wp_pure!. by iApply He3.
Qed.

Lemma Seq_typed Gamma1 Gamma2 e1 e2 A B :
  copy A ->
  typed Gamma1 e1 A ->
  typed Gamma2 e2 B ->
  typed (Gamma1 ++ Gamma2) (ESeq e1 e2) B.
Proof.
  unfold typed. iIntros (HcopyA He1 He2 vs) "Hctx". simpl.
  iDestruct (ctx_typed_app_inv with "Hctx") as "[Hctx1 Hctx2]".
  wp_pure!. iApply (He1 with "Hctx1"); iIntros (v1) "HA".
  iDestruct (HcopyA with "HA") as "_".
  iApply (He2 with "Hctx2").
Qed.

(** The semantic interpretation of copyable types entails [EMP] and is
duplicable. These properties are used the proofs of nearly any lemma that
involves copyable types. *)

Lemma copy_emp A v :
  copy A ->
  A v |~ EMP.
Proof.
  iIntros (Hcopy) "HA". unfold copy in *.
  (** Turn [A v] into [@[ EMP |~ A v ]] *)
  iDestruct (Hcopy with "HA") as "HA".
  (** Move [EMP |~ A v] to the Coq context *)
  iDestruct "HA" as %HA.
  (** Now we have to prove [EMP] in the empty context, which is trivial. *)
  done.
Qed.

Lemma copy_dup A v :
  copy A ->
  A v |~ A v ** A v.
Proof.
  iIntros (Hcopy) "HA". unfold copy in *. 
  iDestruct (Hcopy with "HA") as %HA.
  iFrame "HA". by iApply HA.
Qed.

Lemma Moved_copy :
  copy TMoved.
Proof.
  unfold copy.
  (** We have a [ @[ ... ] ] in an Iris Proof Mode goal. We can use [iPureIntro]
  to introduce it. A shorter way is to use the [ !% ] introduction pattern. *)
  iIntros (v) "_ !%".
  iIntros "_". unfold TMoved. done.
Qed.

Lemma Unit_copy :
  copy TUnit.
Proof.
  iIntros (v) "Hunit".
  iDestruct "Hunit" as %->.
  iIntros "!% _ //".
Qed.

Lemma Bool_copy :
  copy TBool.
Proof. iIntros (v) "[%b ->] !% _". by iExists b. Qed.

Lemma Nat_copy :
  copy TNat.
Proof. iIntros (v) "[%n ->] !% _". by iExists n. Qed.

Lemma Prod_copy A1 A2 :
  copy A1 ->
  copy A2 ->
  copy (TProd A1 A2).
Proof.
  unfold copy.
  iIntros (Hcopy1 Hcopy2 v) "(%w1 & %w2 & -> & HA1 & HA2)".
  (* Use the fact that [A1] and [A2] are copyable to move [HA1] and [HA2] to the
  Coq context. *)
  iDestruct (Hcopy1 with "HA1") as "HA1"; iDestruct "HA1" as %HA1.
  iDestruct (Hcopy2 with "HA2") as "HA2"; iDestruct "HA2" as %HA2.
  (** Now that the Iris Proof Mode context is empty, we can introduce the
  pure embedding [ @[ ... ] ]. *)
  iIntros "!% _".
  iExists w1, w2. iSplitR; [done|].
  iSplitR; [by iApply HA1|by iApply HA2].
Qed.

Lemma Fun_copy A1 A2 :
  copy (TFun A1 A2).
Proof.
  unfold copy.
  iIntros (v) "Hfun". iDestruct "Hfun" as %Hfun.
  iPureIntro. iIntros "_". done.
Qed.

Lemma subty_refl A :
  subty A A.
Proof. iIntros (v) "HA //". Qed.

Lemma subty_trans A1 A2 A3 :
  subty A1 A2 ->
  subty A2 A3 ->
  subty A1 A3.
Proof. iIntros (HA12 HA23 v) "HA". iApply HA23. by iApply HA12. Qed.

Lemma FunOnce_subty A1 A2 B1 B2 :
  subty A2 A1 ->
  subty B1 B2 ->
  subty (TFunOnce A1 B1) (TFunOnce A2 B2).
Proof.
  iIntros (HA HB v) "Hfun %w HA2". iApply ("Hfun" with "[HA2]").
  { by iApply HA. }
  iIntros (vret) "HB1". by iApply HB.
Qed.

Lemma Fun_subty A1 A2 B1 B2 :
  subty A2 A1 ->
  subty B1 B2 ->
  subty (TFun A1 B1) (TFun A2 B2).
Proof.
  iIntros (HA HB v) "Hfun". iDestruct "Hfun" as %Hfun.
  iIntros "!% %w HA2". iApply (Hfun with "[HA2]").
  { by iApply HA. }
  iIntros (vret) "HB1". by iApply HB.
Qed.


(* ########################################################################## *)
(** * Exercise: Typing rules *)
(* ########################################################################## *)

(** Prove the remaining rules for subtyping. If you use introduction patterns,
most proofs are just a couple of lines. *)

Lemma Prod_subty A1 A2 B1 B2 :
  subty A1 A2 ->
  subty B1 B2 ->
  subty (TProd A1 B1) (TProd A2 B2).
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

Lemma Ref_subty A1 A2 :
  subty A1 A2 ->
  subty (TRef A1) (TRef A2).
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Hint: If you have ["Hfun" TFun A1 A2 v] in your Iris Proof Mode context, you
can use [iDestruct "Hfun" as %Hfun] to move it to the Coq context. *)

Lemma Fun_to_FunOnce_subty A1 A2 :
  subty (TFun A1 A2) (TFunOnce A1 A2).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma subty_copy A :
  copy A ->
  subty A TMoved.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** Prove the remaining rules for context subtyping. If you use introduction
patterns, most proofs are just a couple of lines. *)

Lemma subctx_refl Gamma :
  subctx Gamma Gamma.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma subctx_trans Gamma1 Gamma2 Gamma3 :
  subctx Gamma1 Gamma2 ->
  subctx Gamma2 Gamma3 ->
  subctx Gamma1 Gamma3.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma subctx_cons Gamma1 Gamma2 x A1 A2 :
  subty A1 A2 ->
  subctx Gamma1 Gamma2 ->
  subctx ((x,A1) :: Gamma1) ((x,A2) :: Gamma2).
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

Lemma subctx_app Gamma1 Gamma2 Gamma3 Gamma4 :
  subctx Gamma1 Gamma2 ->
  subctx Gamma3 Gamma4 ->
  subctx (Gamma1 ++ Gamma3) (Gamma2 ++ Gamma4).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

(** The following lemma states that the order of the context does not matter. *)

Lemma subctx_swap Gamma1 Gamma2 :
  subctx (Gamma1 ++ Gamma2) (Gamma2 ++ Gamma1).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

(** The following two lemmas factor out the "weakening" and "contraction"
properties via context subtyping. To prove these lemmas, [copy_emp] and
[copy_dup] could be helpful. Concretely, you can use

  iDestruct (copy_emp with "HA") as "_"
  iDestruct (copy_dup with "HA") as "[HA1 HA2]"

To discard/duplicate the ownership of a copyable type from your Iris Proof Mode
goal. *)

Lemma subctx_copy_weakening Gamma x A :
  copy A ->
  subctx ((x,A) :: Gamma) Gamma.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma subctx_copy_contraction Gamma x A :
  copy A ->
  subctx ((x,A) :: Gamma) ((x,A) :: (x,A) :: Gamma).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

(** Prove the following rules for value and expression typing. The proofs
become gradually more complicated. The first ones are just one or two lines,
whereas later proofs require more involved reasoning. *)

Lemma val_subsumption v A1 A2 :
  subty A1 A2 ->
  val_typed v A1 ->
  val_typed v A2.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma Unit_typed :
  val_typed VUnit TUnit.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma Bool_typed b :
  val_typed (VBool b) TBool.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma Nat_typed n :
  val_typed (VNat n) TNat.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma subsumption Gamma1 Gamma2 e A1 A2 :
  subctx Gamma2 Gamma1 ->
  subty A1 A2 ->
  typed Gamma1 e A1 ->
  typed Gamma2 e A2.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

Lemma Val_typed A v :
  val_typed v A ->
  typed [] (EVal v) A.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Var_typed x A :
  typed [(x,A)] (EVar x) A.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma Alloc_typed Gamma e A :
  typed Gamma e A ->
  typed Gamma (EAlloc e) (TRef A).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

(** Make sure to NOT unfold [LinLoad], but to use [iApply LinLoad_wp]. *)

Lemma LinLoad_typed Gamma e A :
  typed Gamma e (TRef A) ->
  typed Gamma (ELinLoad e) (TProd (TRef TMoved) A).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

(** Make sure to NOT unfold [LinLoad], but to use [iApply LinLoad_wp]. *)

Lemma LinLoad_copy_typed Gamma e A :
  copy A ->
  typed Gamma e (TRef A) ->
  typed Gamma (ELinLoad e) (TProd (TRef A) A).
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.

Lemma Store_typed Gamma1 Gamma2 e1 e2 A :
  typed Gamma1 e1 (TRef TMoved) ->
  typed Gamma2 e2 A ->
  typed (Gamma1 ++ Gamma2) (EStore e1 e2) (TRef A).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma Free_typed Gamma e A :
  typed Gamma e (TRef A) ->
  typed Gamma (EFree e) A.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma Pair_typed Gamma1 Gamma2 e1 e2 A1 A2 :
  typed Gamma1 e1 A1 ->
  typed Gamma2 e2 A2 ->
  typed (Gamma1 ++ Gamma2) (EPair e1 e2) (TProd A1 A2).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

(** In the next two proofs you need the lemma [subst_map_insert] and other
properties of finite maps to bring the [subst]s into the right shape so that you
can [iApply] your premises. See also the proof of [Lam_typed]. *)

Lemma Let_typed Gamma1 Gamma2 e1 x e2 A B :
  ~In x (ctx_dom Gamma2) ->
  typed Gamma1 e1 A ->
  typed ((x,A) :: Gamma2) e2 B ->
  typed (Gamma1 ++ Gamma2) (ELet x e1 e2) B.
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.

(** Make sure to use [iApply LetPair_wp] *)

Lemma LetPair_typed Gamma1 Gamma2 e1 x1 x2 e2 A1 A2 B :
  ~In x1 (ctx_dom Gamma2) ->
  ~In x2 (ctx_dom Gamma2) ->
  x1 <> x2 ->
  typed Gamma1 e1 (TProd A1 A2) ->
  typed ((x1,A1) :: (x2,A2) :: Gamma2) e2 B ->
  typed (Gamma1 ++ Gamma2) (ELetPair x1 x2 e1 e2) B.
Proof. (* FILL IN HERE (10 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Exercise: Sum types *)
(* ########################################################################## *)

(** In this exercise you will give a semantic definition of sum types and prove
that the typing rules for sums hold w.r.t. your definition. *)

Definition TSum (A1 A2 : ty) : ty. (* FILL IN HERE *) Admitted.

Lemma Sum_copy A1 A2 :
  copy A1 ->
  copy A2 ->
  copy (TSum A1 A2).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma Sum_subty A1 A2 B1 B2 :
  subty A1 A2 ->
  subty B1 B2 ->
  subty (TSum A1 B1) (TSum A2 B2).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma InjL_typed Gamma e A1 A2 :
  typed Gamma e A1 ->
  typed Gamma (EInjL e) (TSum A1 A2).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma InjR_typed Gamma e A1 A2 :
  typed Gamma e A2 ->
  typed Gamma (EInjR e) (TSum A1 A2).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma Match_typed Gamma1 Gamma2 e x1 e1 x2 e2 A1 A2 B :
  ~In x1 (ctx_dom Gamma2) ->
  ~In x2 (ctx_dom Gamma2) ->
  typed Gamma1 e (TSum A1 A2) ->
  typed ((x1,A1) :: Gamma2) e1 B ->
  typed ((x2,A2) :: Gamma2) e2 B ->
  typed (Gamma1 ++ Gamma2) (EMatch e x1 e1 x2 e2) B.
Proof. (* FILL IN HERE (15 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Exercise: WP adequacy and type soundness *)
(* ########################################################################## *)

(** In this exercise you will prove the soundness theorem. Since the semantic
typing judgments is defined in terms of the weakest precondition, you first
need to prove adequacy for WP. The adequacy statement for WP follows almost
immediately from the definition. *)

Section wp_adequacy.
  (** To prove adequacy of WP, we need to go into the model of our separation
  logic. Hence we locally make [sepEntails] transparent. *)

  Transparent sepEntails.

  Lemma wp_adequacy phi e :
    (EMP |~ wp e (fun v => @[ phi v ])) ->
    exists v, big_step e NatMap.empty v NatMap.empty /\ phi v.
  Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.
End wp_adequacy.

Definition terminates_without_leaks (e : expr) :=
  exists v, big_step e NatMap.empty v NatMap.empty.

(** Hint: It might be useful to first [assert] a WP that fits the premise of
[wp_adequacy], and then [apply wp_adequacy in] your asserted hypothesis. *)

Lemma type_soundness e A :
  copy A ->
  typed [] e A ->
  terminates_without_leaks e.
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Challenge: Introduction rule for unrestricted functions *)
(* ########################################################################## *)

(** In this challenge you need to prove the introduction rule for unrestricted
function types. Hint: You might need to prove a lemma about [ctx_typed]. *)

Lemma Lam_copy_typed Gamma e x A1 A2 :
  ~In x (ctx_dom Gamma) ->
  Forall (fun xA => copy (snd xA)) Gamma ->
  typed ((x,A1) :: Gamma) e A2 ->
  typed Gamma (ELam x e) (TFun A1 A2).
Proof. (* FILL IN HERE (13 LOC helpers and 7 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Challenge: Operator typing *)
(* ########################################################################## *)

(** In this challenge you will define the typing judgment [bin_op_typed] for
operators and prove the typing rules for operators. *)

Definition bin_op_typed (op : bin_op) (A1 A2 B : ty) : Prop. (* FILL IN HERE *) Admitted.

Lemma AddOp_typed :
  bin_op_typed AddOp TNat TNat TNat.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma SubOp_typed :
  bin_op_typed SubOp TNat TNat TNat.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma LeOp_typed :
  bin_op_typed LeOp TNat TNat TBool.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma LtOp_typed :
  bin_op_typed LtOp TNat TNat TBool.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma EqOp_Unit_typed :
  bin_op_typed EqOp TUnit TUnit TBool.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma EqOp_Nat_typed :
  bin_op_typed EqOp TNat TNat TBool.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.
Lemma EqOp_Bool_typed :
  bin_op_typed EqOp TBool TBool TBool.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Op_typed Gamma1 Gamma2 op e1 e2 A1 A2 B :
  bin_op_typed op A1 A2 B ->
  typed Gamma1 e1 A1 ->
  typed Gamma2 e2 A2 ->
  typed (Gamma1 ++ Gamma2) (EOp op e1 e2) B.
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Challenge: Typing of projections *)
(* ########################################################################## *)

(** The goal of this challenge exercise is to define typing rules [Fst_typed]
and [Snd_typed] that are both sound (meaning you can prove them) and strong
enough so that you can make a type derivation for [ELetPair]. That is, you
should be able to prove [LetPair_typed'] without unfolding the definition of
[typed], and solely using [App_typed], [Lam_typed], [Closure_typed],
[Val_typed], the subtyping and copy rules, and your new [Fst_typed] and
[Snd_typed] rules. You are also allowed (and encouraged) to prove a new
subtyping rule for product types. *)

(** We make [typed] opaque to ensure it is not accidentally unfolded in the
proof below. *)

Opaque typed.

Lemma LetPair_typed' Gamma1 Gamma2 e1 x1 x2 e2 A1 A2 B :
  ~In x1 (ctx_dom Gamma2) ->
  ~In x2 (ctx_dom Gamma2) ->
  x1 <> x2 ->
  typed Gamma1 e1 (TProd A1 A2) ->
  typed ((x1,A1) :: (x2,A2) :: Gamma2) e2 B ->
  typed (Gamma1 ++ Gamma2) (ELetPair x1 x2 e1 e2) B.
Proof. (* FILL IN HERE (29 LOC helpers and 19 LOC proof) *) Admitted.

(** * Lecture 10: Separation logic (Hoare triples and weakest preconditions) *)
From pv Require Export week9.

(* REMINDER:

          #####################################################
          ###  PLEASE DO NOT DISTRIBUTE SOLUTIONS PUBLICLY  ###
          #####################################################

*)

(* ########################################################################## *)
(** * Introduction *)
(* ########################################################################## *)

(** See the file week10.md in the repository for the theoretical background
of this lecture. *)


(* ########################################################################## *)
(** * Extended call-by-value lambda calculus with references *)
(* ########################################################################## *)

(** We extend our call-by-value lambda calculus with references from week 6 with
pairing, recursive functions, sequencing, and a free operation. The (informal)
grammar of the extended language is as follows:

  Values:        v ::= ... | recclosure f x => e | (v1,v2)
  Expressions:   e ::= ... | rec f x => e | (e1,e2) | fst e | snd e
                           | e1; e2 | free e

We give a big-step operational semantics to this language, which we will use
to define the semantics/model of weakest preconditions/Hoare triples. The
definition of the language follows the usual recipe: We define the syntax,
substitution, and the big-step operational semantics. *)

Inductive bin_op :=
  | AddOp
  | SubOp
  | LeOp
  | LtOp
  | EqOp.

Inductive expr :=
  | EVal : val -> expr
  | EVar : string -> expr
  | ELam : string -> expr -> expr
  | ERec : string -> string -> expr -> expr (* NEW *)
  | EApp : expr -> expr -> expr
  | EOp : bin_op -> expr -> expr -> expr
  | EPair : expr -> expr -> expr (* NEW *)
  | EFst : expr -> expr (* NEW *)
  | ESnd : expr -> expr (* NEW *)
  | EIf : expr -> expr -> expr -> expr
  | ESeq : expr -> expr -> expr (* NEW *)
  | EAlloc : expr -> expr
  | ELoad : expr -> expr
  | EStore : expr -> expr -> expr
  | EFree : expr -> expr (* NEW *)
with val :=
  | VUnit : val
  | VBool : bool -> val
  | VNat : nat -> val
  | VClosure : string -> expr -> val
  | VRecClosure : string -> string -> expr -> val (* NEW *)
  | VPair : val -> val -> val (* NEW *)
  | VRef : loc -> val.

Fixpoint subst (x : string) (w : val) (e : expr) : expr :=
  match e with
  | EVal _ => e
  | EVar y => if String.eq_dec y x then EVal w else EVar y
  | ELam y e => if String.eq_dec y x then ELam y e else ELam y (subst x w e)
  | ERec f y e =>
     if String.eq_dec y x then ERec f y e else
     if String.eq_dec f x then ERec f y e else ERec f y (subst x w e)
  | EApp e1 e2 => EApp (subst x w e1) (subst x w e2)
  | EOp op e1 e2 => EOp op (subst x w e1) (subst x w e2)
  | EPair e1 e2 => EPair (subst x w e1) (subst x w e2)
  | EFst e => EFst (subst x w e)
  | ESnd e => ESnd (subst x w e)
  | EIf e1 e2 e3 => EIf (subst x w e1) (subst x w e2) (subst x w e3)
  | ESeq e1 e2 => ESeq (subst x w e1) (subst x w e2)
  | EAlloc e => EAlloc (subst x w e)
  | EStore e1 e2 => EStore (subst x w e1) (subst x w e2)
  | ELoad e => ELoad (subst x w e)
  | EFree e => EFree (subst x w e)
  end.

Definition eval_bin_op (op : bin_op) (v1 v2 : val) : option val :=
  match op, v1, v2 with
  | AddOp, VNat n1, VNat n2 => Some (VNat (n1 + n2))
  | SubOp, VNat n1, VNat n2 => Some (VNat (n1 - n2))
  | LeOp, VNat n1, VNat n2 => Some (VBool (Nat.leb n1 n2))
  | LtOp, VNat n1, VNat n2 => Some (VBool (Nat.ltb n1 n2))
  | EqOp, VUnit, VUnit => Some (VBool true)
  | EqOp, VNat n1, VNat n2 => Some (VBool (Nat.eqb n1 n2))
  | EqOp, VBool n1, VBool n2 => Some (VBool (Bool.eqb n1 n2))
  | EqOp, VRef l1, VRef l2 => Some (VBool (Nat.eqb l1 l2))
  | _, _, _ => None
  end.

Notation heap := (natmap val).
Import NatMap.

Inductive big_step : expr -> heap -> val -> heap -> Prop :=
  | Val_big_step h v :
     big_step (EVal v) h v h
  | Lam_big_step h y e :
     big_step (ELam y e) h (VClosure y e) h
  | Rec_big_step h f y e :
     big_step (ERec f y e) h (VRecClosure f y e) h
  | App_big_step h1 h2 h3 h4 y e1 e1' e2 v2 v :
     big_step e2 h1 v2 h2 ->
     big_step e1 h2 (VClosure y e1') h3 ->
     big_step (subst y v2 e1') h3 v h4 ->
     big_step (EApp e1 e2) h1 v h4
  | AppRec_big_step h1 h2 h3 h4 f y e1 e1' e2 v2 v :
     big_step e2 h1 v2 h2 ->
     big_step e1 h2 (VRecClosure f y e1') h3 ->
     big_step (subst y v2 (subst f (VRecClosure f y e1') e1')) h3 v h4 ->
     big_step (EApp e1 e2) h1 v h4
  | Op_big_step h1 h2 h3 e1 e2 op v1 v2 v :
     big_step e2 h1 v2 h2 ->
     big_step e1 h2 v1 h3 ->
     eval_bin_op op v1 v2 = Some v ->
     big_step (EOp op e1 e2) h1 v h3
  | Pair_big_step h1 h2 h3 e1 e2 v1 v2 :
     big_step e2 h1 v2 h2 ->
     big_step e1 h2 v1 h3 ->
     big_step (EPair e1 e2) h1 (VPair v1 v2) h3
  | Fst_big_step h1 h2 e v1 v2 :
     big_step e h1 (VPair v1 v2) h2 ->
     big_step (EFst e) h1 v1 h2
  | Snd_big_step h1 h2 e v1 v2 :
     big_step e h1 (VPair v1 v2) h2 ->
     big_step (ESnd e) h1 v2 h2
  | If_big_step_true h1 h2 h3 e1 e2 e3 v :
     big_step e1 h1 (VBool true) h2 ->
     big_step e2 h2 v h3 ->
     big_step (EIf e1 e2 e3) h1 v h3
  | If_big_step_false h1 h2 h3 e1 e2 e3 v :
     big_step e1 h1 (VBool false) h2 ->
     big_step e3 h2 v h3 ->
     big_step (EIf e1 e2 e3) h1 v h3
  | Seq_big_step h1 h2 h3 e1 e2 v1 v2 :
     big_step e1 h1 v1 h2 ->
     big_step e2 h2 v2 h3 ->
     big_step (ESeq e1 e2) h1 v2 h3
  | Alloc_big_step h1 h2 e v :
     big_step e h1 v h2 ->
     (** We use [fresh] to pick an unused location *)
     big_step (EAlloc e) h1 (VRef (fresh h2)) (insert (fresh h2) v h2)
  | Load_big_step h1 h2 e l v :
     big_step e h1 (VRef l) h2 ->
     lookup h2 l = Some v ->
     big_step (ELoad e) h1 v h2
  | Store_big_step h1 h2 h3 e1 e2 l v :
     big_step e2 h1 v h2 ->
     big_step e1 h2 (VRef l) h3 ->
     lookup h3 l <> None ->
     (** Make sure that we only assign to locations that actually exist! *)
     big_step (EStore e1 e2) h1 (VRef l) (insert l v h3)
  | Free_big_step h1 h2 e l v :
     big_step e h1 (VRef l) h2 ->
     lookup h2 l = Some v ->
     (** Make sure that we only free locations that actually exist! *)
     big_step (EFree e) h1 v (delete l h2).


(* ########################################################################## *)
(** * Hoare triples/weakest preconditions *)
(* ########################################################################## *)

(** In week 9, we defined the propositions [sepProp] of separation logic as
heap predicates. We carried out that formalization abstractly. Instead of fixing
the type of values [V], we made [sepProp V] parametric in it. Since we define
weakest preconditions and Hoare triples for a concrete language, we fix the
value type with the types of values [val] of our language. We do that by
defining a notation [sepProp] that shadows the [sepProp] from [week9.v]. *)

Notation sepProp := (sepProp val).

(** We defined Hoare triples directly (as a shallow embedding) in the lecture
notes. In Coq, we do that in two steps. First, we define weakest preconditions,
and in the second step use that to define Hoare triples. This approach makes our
proofs more concise since weakest preconditions are easier to reason about.

Recall that the intuitive semantics (ignoring frame quantification) of the Hoare
triple [hoare P e Phi] is: If the precondition [P] holds for the initial heap,
then the expression [e] results in a value [v], such that [Phi v] holds for the
final heap.

The weakest precondition [wp e Phi] does away with the precondition [P].
Instead, [wp e Phi] is a [sepProp], i.e., a heap predicate instead of a [Prop].
Intuitively, [wp e Phi] describes the initial heaps so that [e] results in a
value [v], and [Phi v] holds for the final heap (again, ignoring frame
quantification). Hoare triples are then defined using entailment as
[P |~ wp e Phi]. You should convince yourself that the semantics of Hoare
triples via weakest preconditions is the same as the direct-style semantics
that we have given in the lecture notes. *)

Definition wp (e : expr) (Phi : val -> sepProp) : sepProp := fun h =>
  forall hf, disjoint h hf ->
    exists vret h',
      disjoint h' hf /\
      big_step e (union h hf) vret (union h' hf) /\
      Phi vret h'.

Definition hoare (P : sepProp) (e : expr) (Phi : val -> sepProp) : Prop :=
  P |~ wp e Phi.

(** Let us prove some rules. The first one is the value rule. This rule should
be familiar from the one in week 5. *)

Lemma Val_wp Phi v : Phi v |~ wp (EVal v) Phi.
Proof.
  intros h HPhi hf Hh.
  exists v, h.
  eauto using big_step.
Qed.

(** Second, we derive the Hoare rule from the weakest precondition rule. *)

Lemma Val_hoare v :
  hoare EMP (EVal v) (fun vret => @[ vret = v ]).
Proof.
  unfold hoare.
  eapply sepEntails_trans; [|apply Val_wp].
  by apply sepPure_intro.
Qed.

(** We now prove the frame rule for weakest preconditions. This rule says
that we can move a proposition [R] into the postcondition. *)

(** The proof is a bit subtle, as we need to use properties about [union] and
[disjoint] (from the library file) many times. To make life a little bit easier,
we define a hint that automatically applies [disjoint_comm]. We add this hint
with level 0 to the hint database [core] so that [done] and [by] use it. *)

Global Hint Extern 0 (disjoint _ _) =>
  apply disjoint_comm; assumption : core.

Lemma wp_frame Phi R e :
  wp e Phi ** R |~ wp e (fun vret => Phi vret ** R).
Proof.
  intros h (h1 & h2 & -> & Hdisj & Hwp & HR) hf Hdisj'.
  (** [apply lemma in hyp as intro_pat] often comes in handy. *)
  apply disjoint_union in Hdisj' as [??].
  destruct (Hwp (union h2 hf)) as (vret & h' & Hdisj' & Hbig & HPhi).
  { by apply disjoint_comm, disjoint_union. }
  apply disjoint_comm, disjoint_union in Hdisj' as [??].
  exists vret, (union h' h2). split.
  { by apply disjoint_union. }
  split.
  { by rewrite <-!union_assoc. }
  by exists h', h2.
Qed.

(** The frame rule for weakest preconditions allows us to derive the frame
rule for Hoare triples. By "deriving" we mean that we only unfold [hoare] and
not [wp]. *)

Lemma hoare_frame P Phi R e :
  hoare P e Phi ->
  hoare (P ** R) e (fun vret => Phi vret ** R).
Proof.
  unfold hoare. intros Hhoare.
  eapply sepEntails_trans; [|apply wp_frame].
  by apply sepSep_mono_l.
Qed.

(** The rules for binary operators. *)

Lemma Op_wp Phi op v1 v2 v :
  eval_bin_op op v1 v2 = Some v ->
  Phi v |~ wp (EOp op (EVal v1) (EVal v2)) Phi.
Proof.
  intros Hop h HPhi hf Hdisj. exists v, h. eauto using big_step.
Qed.

Lemma Op_hoare op v1 v2 v :
  eval_bin_op op v1 v2 = Some v ->
  hoare EMP (EOp op (EVal v1) (EVal v2)) (fun vret => @[ vret = v ]).
Proof.
  intros. unfold hoare.
  eapply sepEntails_trans; [|by apply Op_wp].
  by apply sepPure_intro.
Qed.


(* ########################################################################## *)
(** * Exercise: Basic rules for hoare triples/weakest preconditions *)
(* ########################################################################## *)

(** Prove the rules below: all of these follow the same structure as the proofs
above. *)

Lemma wp_mono Phi Psi e :
  (forall vret, Phi vret |~ Psi vret) ->
  wp e Phi |~ wp e Psi.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

(** Derive the consequence rule for Hoare triples from [wp_mono]. That is, do
_not_ unfold [wp]. Hint, you need to [eapply] the lemma [sepEntails_trans]
multiple times. *)

Lemma hoare_consequence P Q Phi Psi e :
  (Q |~ P) ->
  (forall vret, Phi vret |~ Psi vret) ->
  hoare P e Phi ->
  hoare Q e Psi.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

Lemma Seq_wp Phi e1 e2 :
  wp e1 (fun _ => wp e2 Phi) |~ wp (ESeq e1 e2) Phi.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

(** Derive the consequence rule for Hoare triples from [Seq_wp]. That is, do
not unfold [wp]. Hint, you need to ([e])[apply] the lemma [sepEntails_trans]
multiple times. *)

Lemma Seq_hoare P Q Phi e1 e2 :
  hoare P e1 (fun _ => Q) ->
  hoare Q e2 Phi ->
  hoare P (ESeq e1 e2) Phi.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

(** For the rules below, we leave out the Hoare version. You can try to write
those down and derive them yourself. *)

Lemma App_wp Phi x e v :
  wp (subst x v e) Phi |~ wp (EApp (EVal (VClosure x e)) (EVal v)) Phi.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma Lam_wp Phi x e :
  Phi (VClosure x e) |~ wp (ELam x e) Phi.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma AppRec_wp Phi f x e v :
  wp (subst x v (subst f (VRecClosure f x e) e)) Phi
  |~ wp (EApp (EVal (VRecClosure f x e)) (EVal v)) Phi.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma Rec_wp Phi f x e :
  Phi (VRecClosure f x e) |~ wp (ERec f x e) Phi.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Pair_wp Phi v1 v2 :
  Phi (VPair v1 v2) |~ wp (EPair (EVal v1) (EVal v2)) Phi.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Fst_wp Phi v1 v2 :
  Phi v1 |~ wp (EFst (EVal (VPair v1 v2))) Phi.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Snd_wp Phi v1 v2 :
  Phi v2 |~ wp (ESnd (EVal (VPair v1 v2))) Phi.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma If_true_wp Phi e2 e3 :
  wp e2 Phi |~ wp (EIf (EVal (VBool true)) e2 e3) Phi.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

Lemma If_false_wp Phi e2 e3 :
  wp e3 Phi |~ wp (EIf (EVal (VBool false)) e2 e3) Phi.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Proof rules for heap operations *)
(* ########################################################################## *)

Lemma Store_wp l v w :
  l ~> v |~ wp (EStore (EVal (VRef l)) (EVal w))
               (fun vret => @[ vret = VRef l ] ** l ~> w).
Proof.
  intros h Hl hf Hdisj.
  unfold points_to in Hl; subst h.

  (** It is always a good idea to start your proof by simplifying the
  hypotheses involving disjointness. *)
  apply disjoint_singleton in Hdisj.

  exists (VRef l), (singleton l w). split.
  { by apply disjoint_singleton. }
  split.
  { (** We need to get the resulting heap in the right shape so that the big
    step rule fits. We use [replace] and [map_solver] to achieve that. *)
    replace (union (singleton l w) hf) with
      (insert l w (union (singleton l v) hf)) by map_solver.
    eapply Store_big_step; [eapply Val_big_step..|].
    by rewrite lookup_union, lookup_singleton, Nat.eq_dec_eq. }
  exists NatMap.empty, (singleton l w).
  split; [map_solver|].
  split; [apply NatMap.disjoint_empty|].
  by split.
Qed.

(** The Hoare rule follows immediately from the WP rule. *)

Lemma Store_hoare l v w :
  hoare (l ~> v)
        (EStore (EVal (VRef l)) (EVal w))
        (fun vret => @[ vret = VRef l ] ** l ~> w).
Proof.
  (** Start by unfolding the hoare triple, so that we see what is going on *)
  unfold hoare.
  apply Store_wp.
Qed.


(* ########################################################################## *)
(** * Exercise: Proof rules for heap operations *)
(* ########################################################################## *)

(** Prove the weakest precondition rules for load, alloc, and free. These proofs
follow roughly the same pattern as the proof of [Store_wp] above, so make sure
you have stepped through [Store_wp] carefully. You will need to use plenty of
lemmas about finite maps from the library. *)

Lemma Load_wp l v :
  l ~> v |~ wp (ELoad (EVal (VRef l))) (fun vret => @[ vret = v ] ** l ~> v).
Proof. (* FILL IN HERE (10 LOC proof) *) Admitted.

Lemma Alloc_wp v :
  EMP |~ wp (EAlloc (EVal v)) (fun vret => Ex l, @[ vret = VRef l ] ** l ~> v).
Proof. (* FILL IN HERE (11 LOC proof) *) Admitted.

Lemma Free_wp l v :
  l ~> v |~ wp (EFree (EVal (VRef l))) (fun vret => @[ vret = v ]).
Proof. (* FILL IN HERE (9 LOC proof) *) Admitted.

(** Prove the Hoare rule for load, alloc, free. You should derive these rules
from the ones for weakest preconditions. That is, you should _not_ unfold the
definition of [wp] when proving [c_hoare], but use [c_wp] instead. *)

Lemma Load_hoare l v :
  hoare (l ~> v)
        (ELoad (EVal (VRef l)))
        (fun vret => @[ vret = v ] ** l ~> v).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Alloc_hoare v :
  hoare EMP
        (EAlloc (EVal v))
        (fun vret => Ex l, @[ vret = VRef l ] ** l ~> v).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma Free_hoare l v :
  hoare (l ~> v)
        (EFree (EVal (VRef l)))
        (fun vret => @[ vret = v ]).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(* ########################################################################## *)
(** * Exercise: Context rule *)
(* ########################################################################## *)

(** Rules like [Load_wp] and [Store_wp] can only be used if their argument are
exactly values. They cannot be used if the argument is a compound expression,
say [ELoad (ELoad ...)]. To address that issue, we prove the "context rule":

  wp e (fun vret => wp (k (EVal vret)) Phi) |~ wp (k e) Phi.

This rule says that giving an evaluation context [k], if we want to prove a WP
for [k e], we can first prove a WP for [e].

We define evaluation contexts in the same way as we have done in week 6, but add
constructors for the new operations. *)

Inductive ctx : (expr -> expr) -> Prop :=
  | App_r_ctx e : ctx (EApp e)
  | App_l_ctx v : ctx (fun x => EApp x (EVal v))
  | Op_r_ctx op e : ctx (EOp op e)
  | Op_l_ctx op v : ctx (fun x => EOp op x (EVal v))
  | Pair_l_ctx e : ctx (EPair e)
  | Pair_r_ctx v : ctx (fun x => EPair x (EVal v))
  | Fst_ctx : ctx EFst
  | Snd_ctx : ctx ESnd
  | If_ctx e2 e3 : ctx (fun x => EIf x e2 e3)
  | Seq_ctx e : ctx (fun x => ESeq x e)
  | Alloc_ctx : ctx EAlloc
  | Load_ctx : ctx ELoad
  | Store_ctx_r e : ctx (EStore e)
  | Store_ctx_l v : ctx (fun x => EStore x (EVal v))
  | Free_ctx : ctx EFree
  | Id_ctx : ctx (fun x => x)
  | Compose_ctx k1 k2 : ctx k1 -> ctx k2 -> ctx (fun x => k1 (k2 x)).

(** To carry out our proofs, we define the [inv_big] tactic. This tactic is
similar to the one we have seen in week 5, but extended for the new constructs
of our language. The tactic can be used to repeatedly perform inversion on
[big_step] hypotheses where the first argument is a constructor. *)

Ltac inv_big :=
  repeat match goal with
  | _ => progress simplify_eq
  | H : big_step (EVal _) _ _ _ |- _ => inv H
  | H : big_step (ELam _ _) _ _ _ |- _ => inv H
  | H : big_step (EApp _ _) _ _ _ |- _ => inv H
  | H : big_step (EOp _ _ _) _ _ _ |- _ => inv H
  | H : big_step (EPair _ _) _ _ _ |- _ => inv H
  | H : big_step (EFst _) _ _ _ |- _ => inv H
  | H : big_step (ESnd _) _ _ _ |- _ => inv H
  | H : big_step (EIf _ _ _) _ _ _ |- _ => inv H
  | H : big_step (ESeq _ _) _ _ _ |- _ => inv H
  | H : big_step (EAlloc _) _ _ _ |- _ => inv H
  | H : big_step (ELoad _) _ _ _ |- _ => inv H
  | H : big_step (EStore _ _) _ _ _ |- _ => inv H
  | H : big_step (EFree _) _ _ _ |- _ => inv H
  end.

(** The following lemma is the key to proving the context rule. It says that we
can decompose the big-step semantics for [k e]. We have given your proof, but
convince yourself why we have done the [induction] before the [split]. *)

Lemma big_step_ctx k e v3 h1 h3 :
  ctx k ->
  big_step (k e) h1 v3 h3 <->
  exists v2 h2,
    big_step e h1 v2 h2 /\
    big_step (k (EVal v2)) h2 v3 h3.
Proof.
  intros Hk. revert e v3 h1 h3.
  induction Hk; intros e' v3 h1 h3;
    (split; [intros ?|intros (?&?&?&?)]); inv_big; eauto using big_step.
  - apply IHHk1 in H as (?&?&?&?). apply IHHk2 in H as (?&?&?&?).
    eexists _, _; split; [done|]. apply IHHk1. eauto.
  - apply IHHk1 in H0 as (?&?&?&?).
    apply IHHk1. eexists _, _; split; [apply IHHk2|]; eauto.
Qed.

Lemma wp_ctx Phi k e :
  ctx k ->
  wp e (fun vret => wp (k (EVal vret)) Phi) |~ wp (k e) Phi.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

(** Our language does not have let-expressions as a built-in. We define those
in terms of lambdas. *)

Notation ELet x e1 e2  := (EApp (ELam x e2) e1).

(** You should derive the rule for let expressions. In this proof, it will come
in handy to use the version [sepEntails_trans'] of the transitivity lemma with
the order of the premises swapped. *)

Lemma sepEntails_trans' (P Q R : sepProp) :
  (Q |~ R) -> (P |~ Q) -> P |~ R.
Proof. intros. by eapply sepEntails_trans. Qed.

(** Make [wp] opaque to ensure you do not accidentally unfold it. *)

Opaque wp.

Lemma Let_wp Phi x e1 e2 :
  wp e1 (fun vret => wp (subst x vret e2) Phi) |~ wp (ELet x e1 e2) Phi.
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.

(** Now derive the Hoare version from [Let_wp]. This proof is similar to
[Seq_hoare]. *)

Lemma Let_hoare P Phi Psi x e1 e2 :
  hoare P e1 Phi ->
  (forall v, hoare (Phi v) (subst x v e2) Psi) ->
  hoare P (ELet x e1 e2) Psi.
Proof. (* FILL IN HERE (4 LOC proof) *) Admitted.

(* ########################################################################## *)
(** * Challenge: Alternative Weakest preconditions and Hoare triples *)
(* ########################################################################## *)

(** We can define weakest preconditions in terms of Hoare triples as follows. *)

Definition wp_alt (e : expr) (Phi : val -> sepProp) : sepProp :=
  Ex R, @[ hoare R e Phi ] ** R.

(** Prove that [wp_alt] and [wp] are equivalent. That is, prove entailments in
both directions. You can unfold the definition of [wp_alt] and [hoare], but
should not unfold the definition of [wp]. *)

Lemma wp_alt_1 e Phi :
  wp_alt e Phi |~ wp e Phi.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma wp_alt_2 e Phi :
  wp e Phi |~ wp_alt e Phi.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Hoare triples of programs are often written as:

  hoare P e (fun vret => Ex x : A, @[ vret = f x ] ** Psi x).

See [Load_wp], [Store_wp], [Alloc_wp] and [Free_wp] as examples.

Below we give an alternative definition of Hoare triple that generalizes
this format. *)

Definition hoare_alt {A}
    (P : sepProp) (e : expr) (f : A -> val) (Psi : A -> sepProp) : Prop :=
  forall Phi, P ** (All x, Psi x -** Phi (f x)) |~ wp e Phi.

(** Prove that [hoare_alt] and [hoare] are equivalent. You can unfold the
definition of [hoare_alt] and [hoare], but should not unfold the definition of
[wp]. *)

Lemma hoare_alt_1 {A} P e (f : A -> val) (Psi : A -> sepProp) :
  hoare_alt P e f Psi ->
  hoare P e (fun vret => Ex x : A, @[ vret = f x ] ** Psi x).
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

Lemma hoare_alt_2 {A} P e (f : A -> val) (Psi : A -> sepProp) :
  hoare P e (fun vret => Ex x, @[ vret = f x ] ** Psi x) ->
  hoare_alt P e f Psi.
Proof. (* FILL IN HERE (11 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Challenge: Verification of a concrete program *)
(* ########################################################################## *)

(** Finally, we will look at an example. We take the simplest program that
manipulates pointers, the swap function:

  swap x y :=
    let tmp := !x in
    x := !y;
    y := tmp
*)

Definition swap : val :=
  VClosure "x" (ELam "y" (
    ELet "tmp" (ELoad (EVar "x"))
      (ESeq (EStore (EVar "x") (ELoad (EVar "y")))
            (EStore (EVar "y") (EVar "tmp"))))).

(** Prove that [swap] satisfies the Hoare-style spec. *)

(** DISCLAIMER: This is an exercise in being pedantic. As you will see, the
proof of this lemma is very verbose. Next week we see how tactics can be used
to significantly shorten this proof. By doing this proof by hand, you will get
insights into the steps that our tactics will automate:

- Use transitivity of entailment to use the weakest precondition rules.
- Reorder the premises on the left-hand side of the entailment so that you can
  use monotonicity of the separating conjunction.
- Reorder the goal so that you can apply the framing rule [wp_frame].
- Pick evaluation contexts to apply the context rule [wp_ctx]. *)

Lemma swap_hoare l1 l2 v1 v2 :
  hoare
    (l1 ~> v1 ** l2 ~> v2)
    (EApp (EApp (EVal swap) (EVal (VRef l1))) (EVal (VRef l2)))
    (fun vret => @[ vret = VRef l2 ] ** l1 ~> v2 ** l2 ~> v1).
Proof. (* FILL IN HERE (41 LOC proof) *) Admitted.

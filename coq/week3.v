(** * Lecture 3: Equality, finite maps, finite sets, and higher-order
functions in Coq *)
From pv Require Export library.

(* REMINDER:

          #####################################################
          ###  PLEASE DO NOT DISTRIBUTE SOLUTIONS PUBLICLY  ###
          #####################################################

*)

(** ######################################################################### *)
(** * Introduction *)
(** ######################################################################### *)

(** The main topic of this week concerns finite maps and finite sets. These
notions play an important role in the formalization of programming languages.
For example, to formalize a language with mutable state (i.e.,
pointers/references) such Rust, ML, Java or C, we need to model the heap. This
is generally done using a finite map from addresses (represented as natural
numbers) to bytes, values, or objects. In addition to finite maps and finite
sets, we cover the following other topics:

- Equality: In Coq there are various ways to reason about equality. We discuss
  and compare these approaches.
- Higher-order functions: Coq is higher order, which means that functions are
  treated as first-class citizen. In particular, functions can take functions as
  arguments.*)


(* ########################################################################## *)
(** * Equality and [sumbool] *)
(* ########################################################################## *)

(** In week 2, we saw the difference between Coq's type of logical propositions
[Prop] and Booleans [bool]. The essential difference is that [bool] supports
automatic computation, but does _not_ support quantifiers, while [Prop] supports
quantifiers, but does _not_ support automatic computation. There is another
difference: equality. *)

Module sumbool.
  (** [Prop] has the polymorphic equality [eq : forall {A}, A -> A -> Prop]
  (notation [=]) that we can use to compare values of any type [A]. This is in
  contrast to [bool], where we have to program a specific equality comparison
  function for any type. For example, for the natural numbers, we would program
  the following recursive function. *)

  Fixpoint nat_eqb (n m : nat) : bool :=
    match n, m with
    | 0, 0 => true
    | S n, S m => n =? m
    | _, _ => false
    end
  where "n =? m" := (nat_eqb n m).

  (** To relate truth of our function and equality [=] in [Prop] we prove a
  lemma. *)

  Lemma nat_eqb_eq n m : (n =? m) = true <-> n = m.
  Proof.
    split.
    - revert m. induction n as [|n IH];
        intros [|m] [=]; f_equal; [done|by apply IH].
    - intros ->. induction m as [|m IH]; simpl; by f_equal.
  Qed.

  (** We have to repeat this task for any type whose values we wish to compare
  (Booleans, expressions, lists, etc). Would it not be convenient if we could
  combine the comparison function and the proof into one definition? Would it be
  not be convenient if Coq could automatically generate this definition? For
  that we will use [sumbool]-equality comparison functions. *)

  (** As a first example, we give [sumbool]-equality comparison functions for
  Booleans and natural numbers. *)

  Definition bool_dec (b1 b2 : bool) : { b1 = b2 } + { b1 <> b2 }.
  Proof. decide equality. Defined.
  Definition nat_eq_dec (n1 n2 : nat) : { n1 = n2 } + { n1 <> n2 }.
  Proof. decide equality. Defined.

  (** These lemmas involve a number of new concepts, so let us go over them.
  First, [{ P } + { Q }] is notation for [sumbool P Q], which is defined as: *)

  Print "{ _ } + { _ }".

  (**
    Inductive sumbool (P Q : Prop) : Type :=
      | left : P -> sumbool P Q
      | right : Q -> sumbool P Q
  *)

  (** The [sumbool] type is similar to [bool], but it is annotated with
  propositions [P] and [Q] in the [true] (= [left]) and [false] (= [right])
  constructor. The definitions [bool_eq_dec] and [nat_eq_dec] should thus be
  read as returning a Boolean annotated with a proof of equality or
  inequality. *)

  (** Second, instead of giving an implementation of these functions by hand, we
  let Coq generate the implementation automatically using the tactic
  [decide equality]. We use [Defined] instead of [Qed] to tell Coq that it is an
  algorithm (that should be used for computation) and not a proof. (Try
  changing [Defined] to [Qed], then the [Compute]s below do not give sensible
  answers because they get stuck.) *)

  (** We can compute with [sumbool]s in a way similar to normal Booleans. *)

  Compute (nat_eq_dec 10 10). (** [left ..] *)
  Compute (nat_eq_dec 1 10). (** [right ..] *)

  (** Instead of a mere [true]/[false], we now see [left]/[right] with a proof
  of equality/inequality as the argument. *)

  (** We can use [sumbool]s in [if .. then .. else ..]. *)

  Compute (if nat_eq_dec 10 10 then "yes" else "no"). (** "yes" *)
  Compute (if nat_eq_dec 1 10 then "yes" else "no"). (** "no" *)

  (** We can also parameterize [sumbool]-equality comparison functions. Let us
  see an example for the type [list A] with elements [A]. To compare lists, we
  need to be able to compare elements. *)

  Lemma list_eq_dec {A} (dec : forall x y : A, { x = y } + { x <> y })
    (xs ys : list A) : { xs = ys } + { xs <> ys }.
  Proof. decide equality. Defined.

  (** Computation with [list_eq_dec] works in the way as expected. *)

  Compute (list_eq_dec nat_eq_dec [10] [10]). (** [left ...] *)
  Compute (list_eq_dec nat_eq_dec [] [10]). (** [right ...] *)
  Compute (list_eq_dec (list_eq_dec nat_eq_dec) [[10]] [[10]]). (** [left ...] *)

  (** Aside from computation, we can use [sumbool]s in proofs. *)

  (** Since Coq is a constructive logic, we do not have excluded middle, so
  we cannot make arbitrary case distinctions between whether a proposition [P]
  is true or not ([~P]). However, in many cases, a [sumbool]-equality comparison
  function allows us to make such case distinctions: *)

  Lemma nat_eq_or_neq (n m : nat) : n = m \/ n <> m.
  Proof.
    (** Similar to Booleans, we can use [destruct] on a [sumbool]. We get a
    hypothesis [Heq]/[Hneq] in the case for equality/inequality. *)
    destruct (nat_eq_dec n m) as [Heq|Hneq].
    - by left.
    - by right.
  Qed.
  Lemma list_nat_eq_or_neq (ns ms : list nat) : ns = ms \/ ns <> ms.
  Proof. destruct (list_eq_dec nat_eq_dec ns ms); tauto. Qed.

  (**  In practice, you would not make an explicit [P \/ ~P] lemma, but just
  [destruct] on a [sumbool] in your proof when you need to make a case
  distinction. *)

  (** Let us define a Boolean function that checks if an element [y] is in a
  list [xs]. We do this by recursively going through the list, and comparing
  each element [x] with [y] using [dec]. *)

  Fixpoint inb {A} (dec : forall x y : A, { x = y } + { x <> y })
      (y : A) (xs : list A) : bool :=
    match xs with
    | [] => false
    | x :: xs => if dec x y then true else inb dec y xs
    end.

  (** To prove that this function has the intended behavior, we should prove:

    inb dec y xs = true <-> In y xs.

  We focus on the left-to-right direction (the other direction you can do
  yourself). *)

  Lemma inb_true {A} (dec : forall x y : A, { x = y } + { x <> y })
      (y : A) (xs : list A) :
    inb dec y xs = true -> In y xs.
  Proof.
    intros Hin. induction xs as [|x xs IH]; simpl in *; [done|].
    (** We have the hypothesis

      Hin : (if dec x y then true else inb dec y xs) = true

    The [destruct] tactic will turn this into two cases and add [Heq : x = y]
    and [Hneq : x <> y], respectively, and simplify the hypothesis. *)
    destruct (dec x y) as [Heq|Hneq].
    - by left.
    - right. by apply IH.
  Qed.

  (** We can take this one step further, why not let [inb] return a [sumbool]
  instead of a [bool]? This is possible, but programming [sumbool] functions
  ourselves is out of scope for this course. Coq's standard library does contain
  this version: *)

  About in_dec.
  (**
    in_dec {A} :
      (forall x y : A, {x = y} + {x <> y}) ->
      forall (a : A) (l : list A), {In a l} + {~ In a l}
  *)

  Compute (in_dec nat_eq_dec 10 [1;2;10]). (* [left ..] *)
  Compute (in_dec nat_eq_dec 10 []). (* [right ..] *)
End sumbool.

(** Many of the above functions are present in the Coq standard library. To find
them, use [Search] with [sumbool] in your query. *)

(** Side note: The [sumbool] type is an example of a "dependent type". If you
want to learn more about dependent types, enroll in the course "Type Theory and
Coq". *)


(* ########################################################################## *)
(** * Exercises about equality *)
(* ########################################################################## *)

(** Define a [sumbool]-equality comparison function for [option A]:

  Lemma option_eq_dec {A} (* FILL IN *)
  Proof. decide equality. Defined.

Then, use your equality comparison function to prove the lemmas
[option_nat_eq_or_neq] and [list_option_nat_eq_or_neq]. The proofs of these
lemmas should be done by a single [destruct ..; tauto], where [..] is an
appropriate [sumbool]-equality comparison function.

You should use of [Nat.eq_dec] and [list_eq_dec]. *)

Lemma option_nat_eq_or_neq (mn mm : option nat) : mn = mm \/ mn <> mm.
Proof. (* FILL IN HERE (4 LOC helpers and 1 LOC proof) *) Admitted.

Lemma list_option_nat_eq_or_neq (mns mms : list (option nat)) :
  mns = mms \/ mns <> mms.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** Consider the function [inb] from above. Prove the [<->] version of the
lemma [inb_true]. *)

Fixpoint inb {A} (dec : forall x y : A, { x = y } + { x <> y })
    (y : A) (xs : list A) : bool :=
  match xs with
  | [] => false
  | x :: xs => if dec x y then true else inb dec y xs
  end.

Lemma inb_true {A} (dec : forall x y : A, { x = y } + { x <> y })
    (y : A) (xs : list A) :
  inb dec y xs = true <-> In y xs.
Proof. (* FILL IN HERE (9 LOC proof) *) Admitted.


(* ########################################################################## *)
(** * Finite maps *)
(* ########################################################################## *)

(** A finite map (also called a finite partial function, dictionary, or
association list) associates values to keys. As you hopefully remember from your
course on data structures, finite maps can be implemented in many possible ways
(e.g., using lists, arrays, AVL trees, Red black trees, hash maps). Depending
on the implementation, one gets different computational properties (e.g., [O(1)]
expected or [O(log n)] guaranteed lookup, fast access to the elements in sorted
order), but when using finite maps in specifications and proofs it should not
matter what implementation is used. In this course, we thus treat finite maps
as black boxes. We only look at their specification, and not at their
implementation. (In the definition of finite maps in [library.v] we use Coq's
module system to make this distinction clear.)

We consider the type [natmap A] of finite maps with natural numbers as keys and
values of type [A]. The most important operations on this type are:

- [NatMap.lookup : forall {A}, natmap A -> nat -> option A]
  The function [NatMap.lookup m k] returns the value with key [k]. It returns
  [Some] if the key [k] exists and [None] otherwise.
- [NatMap.empty : forall {A}, natmap A]
  The function [NatMap.empty] gives the empty map, i.e., a map in which there
  are no key/value pairs yet.
- [NatMap.insert : forall {A}, nat -> A -> natmap A -> natmap A]
  The map [NatMap.insert k x m] extends [m] with a pair with key [k] and value
  [x]. If key [k] is already in map [m], its value will be overwritten.
- [NatMap.delete : forall {A}, nat -> natmap A -> natmap A]
  The map [NatMap.delete k m] shrinks [m] by deleting key [k] and its associated
  value. If key [k] is not in map [m], the original map [m] will be returned.

The first and most important lemma of the specification of finite maps is:

  NatMap.map_eq {A} (m1 m2 : matmap A),
    m1 = m2 <-> forall k, NatMap.lookup m1 k = NatMap.lookup m2 k

This lemma states that two maps are equal iff they contain the same key/value
pairs. You will need this property for proving equalities between finite maps.

For every operation on maps, there is a lemma that uniquely describes the
behavior of lookup. For the above operations, these lemmas are:

  Lemma NatMap.lookup_empty {A} (k : nat) :
    NatMap.lookup NatMap.empty k = @None A
  Lemma NatMap.lookup_insert {A} (m1 m2 : natmap A) (k k' : nat) (x : A) :
    NatMap.lookup (NatMap.insert k y m) k' =
      if Nat.eq_dec k' k then Some y else NatMap.lookup m k'
  Lemma NatMap.lookup_delete {A} (m : natmap A) (k k' : nat) :
    NatMap.lookup (NatMap.delete k m) k' =
      if Nat.eq_dec k' k then None else NatMap.lookup m k'

These lemmas respectively say:

- The lookup of the empty map is always [None].
- When inserting an element, we obtain the new value.
- When deleting an element, we obtain [None].

The last two lemmas make use of [Nat.eq_dec] to make a distinction between
[k = k'] and [k <> k']. *)

(** Let us see finite maps in action. First we import [NatMap] so that we do
not have to type [NatMap.] all the time. We do this in a section to keep the
effect local.*)

Section maps.
Import NatMap.

Lemma lookup_singleton {A} (k k' : nat) (x : A) :
  lookup (singleton k x) k' = if Nat.eq_dec k' k then Some x else None.
Proof.
  (** [singleton k x] is defined as [insert k x empty], so let us unfold the
  definition. *)
  unfold singleton.
  (** On the RHS we have a lookup of [insert] and [empty], so let us use the
  corresponding lookup lemmas. *)
  rewrite lookup_insert, lookup_empty.
  (** Both sides are equal, so we are done. *)
  done.
Qed.

Lemma insert_singleton {A} k (y1 y2 : A) :
  insert k y2 (singleton k y1) = singleton k y2.
Proof.
  (** To prove an equality the maps, we use [map_eq] so we can prove that the
  maps are equal for all keys [k']. *)
  apply map_eq; intros k'.

  (** Now we make use of the lookup lemmas for insert and singleton. *)
  rewrite lookup_insert, !lookup_singleton.

  (** Both sides are not equal, there are a some equality tests between [k]
  and [k'] that were generated by the lookup lemmas. We use [destruct] to make
  a case distinction. Both cases are trivial, so we use [done]. (We could have
  used [by destruct (Nat.eq_dec k' k)], but for didactic purposes it might be
  useful to look at the goals that Coq generates.) *)
  destruct (Nat.eq_dec k' k).
  - done.
  - done.
Qed.

Lemma union_empty_l {A} (m : natmap A) :
  union empty m = m.
Proof.
  (** To prove an equality the maps, we again use [map_eq] so we can prove that
  the maps are equal for all keys [k']. *)
  apply map_eq; intros k'.

  (** This lemma involves the [union] operation, which computes the left-based
  union. We use the lookup lemma from the library. *)
  rewrite lookup_union.
  rewrite lookup_empty.
  (** We now get a goal involving [option_union]. Use [Print] to check out
  the definition. Since [option_union] is defined by pattern matching on the
  first argument, we solve the goal by computation via [done]. *)
  done.
Qed.

(** The [union m1 m2] operation is left-biased, which means that it contains all
key/value maps in [m1], and those in [m2] whose key is not in [m1]. For example,
[union (singleton k x1) (singleton k x2)] will be [singleton k x1]. This shows
that in general [union] is not commutative. However, it is commutative when the
domains of [m1] and [m2] are disjoint (i.e., do not overlap). *)

Lemma disjoint_union_comm {A} (m1 m2 : natmap A) :
  disjoint m1 m2 ->
  union m1 m2 = union m2 m1.
Proof.
  intros Hdisj.

  (** To prove an equality the maps, we again use [map_eq]. *)
  apply map_eq; intros k'.

  (** And proceed with the lookup lemmas. *)
  rewrite !lookup_union.

  (** At this point we need disjointness, so let us look up the definition. *)
  unfold disjoint in Hdisj.

  (** It says that for each key [k] either a lookup in [m1] or [m2] is None. We
  let [k] be our [k'] and make a case analysis. *)
  destruct (Hdisj k') as [Hm1|Hm2].
  - rewrite Hm1. simpl. by destruct (lookup m2 k').
  - rewrite Hm2. simpl. by destruct (lookup m1 k').
Restart.
  (** We can make the proof a bit shorter by avoiding an unfold, using the
  introduction patterns [->], and chaining the [destruct] by putting in wild
  cards. *)
  intros Hdisj. apply map_eq; intros k.
  rewrite !lookup_union.
  destruct (Hdisj k) as [->| ->]; simpl; by destruct (lookup _ _).
Qed.

(** Our library provides a tactic [map_solver] that solves equalities over
maps. It more or less applies [map_eq] and repeatedly rewrites with the lookup
lemmas, and finally performs the needed case distinctions. *)

Lemma union_empty_l_automated {A} (m : natmap A) :
  union empty m = m.
Proof. map_solver. Qed.

Lemma insert_singleton_automated {A} k (y1 y2 : A) :
  insert k y2 (singleton k y1) = singleton k y2.
Proof. map_solver. Qed.
End maps.


(* ########################################################################## *)
(** * Exercises about finite maps *)
(* ########################################################################## *)

Section map_exercises.
Import NatMap.

(** Prove the following lemmas. The first five lemmas can be proved using the
tactic [map_solver] (those proofs are also in the library), but make sure to
construct a manual proof yourself to become familiar with the lemmas about
finite maps, as well as proofs involving [sumbool]s. *)

Lemma union_assoc {A} (m1 m2 m3 : natmap A) :
  union m1 (union m2 m3) = union (union m1 m2) m3.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma union_empty_r {A} (m : natmap A) :
  union m empty = m.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma insert_union {A} (m1 m2 : natmap A) k x :
  insert k x (union m1 m2) = union (insert k x m1) (insert k x m2).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma delete_union {A} (m1 m2 : natmap A) k :
  delete k (union m1 m2) = union (delete k m1) (delete k m2).
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

Lemma delete_insert {A} (m : natmap A) k1 k2 y1 :
  delete k2 (insert k1 y1 m) =
    if Nat.eq_dec k1 k2 then delete k2 m else insert k1 y1 (delete k2 m).
Proof. (* FILL IN HERE (6 LOC proof) *) Admitted.

(** The left-to-right direction is most difficult since you need to come up
with a witness [m3]. Think of a way how you can determine this witness. Hint:
use [Search] and look through all functions on [natmap]. *)

Lemma incl_union {A} (m1 m2 : natmap A) :
  incl m1 m2 <-> exists m3, m2 = union m1 m3 /\ disjoint m1 m3.
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.
End map_exercises.


(* ########################################################################## *)
(** * Exercises about finite sets *)
(* ########################################################################## *)

Section set_exercises.
Import NatMap.

(** With an implementation of finite maps at hand, we can easily define a finite
sets. This is conventionally done by letting the values be of unit type. We
thus define sets of naturals numbers as such. *)

Definition natset := natmap unit.

(** The only value of the [unit] type is [tt]. (Recall, you can use [Print unit]
to see that). *)

(** We define set membership, the empty set, singleton set, and union as
follows. *)

Definition elem_of (k : nat) (X : natset) := lookup X k = Some tt.

Definition natset_empty : natset := empty.
Definition natset_singleton (k : nat) : natset := singleton k tt.
Definition natset_union (X1 X2 : natset) : natset := union X1 X2.

(** Prove that two sets are equal iff they contain the same elements. *)

Lemma natset_eq X1 X2 :
  X1 = X2 <-> forall k, elem_of k X1 <-> elem_of k X2.
Proof. (* FILL IN HERE (7 LOC proof) *) Admitted.

(** Prove the following properties that show that the empty set is indeed
empty, the singleton indeed contains the singleton, and the union indeed gives
the union. *)

Lemma elem_of_empty k :
  elem_of k natset_empty <-> False.
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

Lemma elem_of_singleton k k' :
  elem_of k (natset_singleton k') <-> k = k'.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

Lemma elem_of_union k X1 X2 :
  elem_of k (natset_union X1 X2) <-> elem_of k X1 \/ elem_of k X2.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Define the intersection operation on sets and prove that it indeed computes
the intersection. To define this operation, you will need to use the [merge]
function on maps. Hint: Use [About] to look up the type of [merge], and use
[Search] to find lemmas about [merge]. *)

Definition natset_intersection (X1 X2 : natset) : natset. (* FILL IN HERE *) Admitted.

Definition elem_of_intersection k X1 X2 :
  elem_of k (natset_intersection X1 X2) <-> elem_of k X1 /\ elem_of k X2.
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.
End set_exercises.


(** ######################################################################### *)
(** * Higher-order functions *)
(** ######################################################################### *)

(** An important feature of Coq is that it is "higher order", which means that
functions are first-class citizen and can be used as parameters of any
definition, including algorithms/functions, but also mathematical properties. *)

Module ho_defs.
  (** Let us define the familiar [map] function that transform a list
  point-wise, or if you like Haskell or category theory, show that list is
  a functor. *)

  Fixpoint map {A B} (f : A -> B) (xs : list A) : list B :=
    match xs with
    | [] => []
    | x :: xs => f x :: map f xs
    end.

  (** And let use define the familiar identity function and function
  composition. *)

  Definition id {A} : A -> A := fun x => x.

  Definition compose {A B C} (f : B -> C) (g : A -> B) : A -> C :=
    fun x => f (g x).
End ho_defs.

(** Note: If you have done [Import NatMap] (outside a section, or in the current
section), [map] will refer to the map operation on finite maps. This might
happen if you forgot to close the sections above. To disambiguate, you can use
[List.map] to refer to the map operation on lists. You can use [About name] to
see all fully qualified versions of [name]. *)

Locate map.

(** An advantage of Coq over a conventional functional language like Haskell is
that we can prove the "functor" laws. *)

Lemma map_id {A} (xs : list A) :
  map id xs = xs.
Proof.
  induction xs; simpl; by f_equal.
Qed.

Lemma map_compose {A B C} (f : B -> C) (g : A -> B) (xs : list A) :
  map f (map g xs) = map (compose f g) xs.
Proof.
  induction xs; simpl; congruence.
Qed.

(** And let us prove some more properties of [map]. *)

Lemma map_app {A B} (f : A -> B) (xs1 xs2 : list A) :
  map f (xs1 ++ xs2) = map f xs1 ++ map f xs2.
Proof.
  induction xs1; simpl; congruence.
Qed.

Lemma rev_map {A B} (f : A -> B) (xs : list A) :
  rev (map f xs) = map f (rev xs).
Proof.
  induction xs as [|x xs IH]; simpl; [done|].
  by rewrite map_app, IH.
Qed.

(** Aside from writing algorithms that are parameterized by functions, Coq also
allows us to parameterize mathematical definitions/properties by functions. The
definition defines what it means for a function to be injective. *)

Definition injective {A B} (f : A -> B) : Prop :=
  forall x1 x2, f x1 = f x2 -> x1 = x2.

(** Let us prove that the identity function is injective. Note that this lemma
should work for the identify function on any type [A]. We use the [@] syntax to
specify that it is about the identify function [@id A] at type [A]. *)

Lemma injective_id {A} :
  injective (@id A).
Proof.
  unfold injective. done.
Qed.

(** Let us now prove that the following functions on natural numbers and
Booleans are injective. *)

Lemma injective_S :
  injective S.
Proof.
  intros i1 i2 Hi. congruence.
Qed.

Lemma injective_negb :
  injective negb.
Proof.
  by intros [] [].
Qed.

Definition swap : nat -> nat :=
  fun i =>
    match i with
    | 0 => 1
    | 1 => 0
    | _ => i
    end.

Lemma injective_swap :
  injective swap.
Proof.
  (** We need to distinguish a number of cases, namely for all combinations of
  [i = 0], [i = 1], [i = S (S i')] and [j = 0], [j = 1], [j = S (S i')]. We use
  introduction patterns to make these case distinctions, and then use [lia] to
  solve all subgoals automatically. *)
  intros [|[|i']] [|[|j']]; simpl; lia.
Qed.


(** ######################################################################### *)
(** * Exercises about higher-order functions *)
(** ######################################################################### *)

Lemma in_map {A B} (f : A -> B) (xs : list A) x :
  In x xs ->
  In (f x) (map f xs).
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** The lemma [in_map] does not hold in reverse direction. We can convince
ourselves of that by proving its negation. *)

Lemma not_in_map_inv :
  ~ forall A B (f : A -> B) xs x, In x xs <-> In (f x) (map f xs).
Proof. (* FILL IN HERE (3 LOC proof) *) Admitted.

(** We can turn the lemma [in_map] into a bidirectional ([<->]) lemma as
follows. *)

Lemma in_map_iff {A B} (f : A -> B) (xs : list A) y :
  In y (map f xs) <-> exists x, y = f x /\ In x xs.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

(** The reverse direction of [in_map] does hold if we restrict ourselves to
injective functions. *)

Lemma in_map_inv {A B} (f : A -> B) (xs : list A) x :
  injective f ->
  In (f x) (map f xs) -> In x xs.
Proof. (* FILL IN HERE (2 LOC proof) *) Admitted.

(** Define the following Boolean function that checks if a natural number [n]
is in a list [ns] of natural numbers. You should _not_ use a [Fixpoint], but use
existing definitions from Coq's standard library. (Hint: Use [Search].) *)

Definition nat_in (n : nat) : list nat -> bool. (* FILL IN HERE *) Admitted.

(** Prove the following lemma to verify that your definition is correct. Instead
of proving the lemma by [induction], try to [Search] Coq's library for lemmas
about the functions you used in your definition. *)

Lemma nat_in_true n ns :
  nat_in n ns = true <-> In n ns.
Proof. (* FILL IN HERE (5 LOC proof) *) Admitted.

(** Prove function composition preserves injectivity. *)

Lemma injective_compose {A B C} (f1 : B -> C) (f2 : A -> B) :
  injective f1 ->
  injective f2 ->
  injective (compose f1 f2).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

(** The function [shift f] maps [0] to [0], and uses [f] for all other mappings,
but moved one place. Prove that [shift] preserves injectivity. *)

Definition shift (f : nat -> nat) : nat -> nat :=
  fun i =>
    match i with
    | 0 => 0
    | S i => S (f i)
    end.

Lemma injective_shift f :
  injective f ->
  injective (shift f).
Proof. (* FILL IN HERE (1 LOC proof) *) Admitted.

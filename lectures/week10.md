Lecture 10: Separation logic (Hoare triples and weakest preconditions)
======================================================================

In the previous lecture, we have introduced separation logic as a system for
specification and proofs of programs that manipulate the heap. We looked at the
propositions of separation logic and gave them a semantics using a shallow
embedding as heap predicates. In this lecture, we take a look at the semantics
of Hoare triples and the proof rules for these.

## Semantics of Hoare triples

Let us start by recapitulating the intuitive semantics of Hoare triples
`[ P ] e [ v. Q ]`. (Keep in mind that separation logic propositions `P` and `Q`
describe heaps). Hoare triples express the following:

- Given any initial heap `h`,
- If the precondition `P h` holds beforehand, then,
- Given any other heap `hf` called the _frame_ that disjoint to `h` (notation
  `h ## hf`),
- Execution of the program `e` in `h ∪ hf` in is guaranteed to terminate with
  a value `v` in a final heap `h' ∪ hf` with `h' ## hf`,
- Such that the postcondition `Q h'` holds.

In logic, this leads to the following definition:

```
[ P ] e [ v. Q ] := ∀ h hf,
  h ## hf →
  P h →
  ∃ v h',
    (e, h ∪ hf) ⇓ (v, h' ∪ hf)  ∧  h' ## hf  ∧  Q h'
```

# Proof rules of separation logic

With the semantics of Hoare triples at hand, we will now look at the proof
rules. Let us start with the proof rules for the load/store/free/alloc:

```
         [ emp ] alloc v [ vret. ∃ l, (vret = l) * l ↦ v ]
       [ l ↦ v ]   !l    [ vret. (vret = v) ∗ l ↦ v ]
[ ∃ v', l ↦ v' ]  l ← v  [ vret. (vret = l) ∗ l ↦ v ]
       [ l ↦ v ] free l  [ vret. (vret = v) ]
```

**Note 1** In the operational semantics of our language, we have decided to let
store return the location, and to let free return the old value. Hence, the
Hoare rules reflect that. This is a choice, of course—we could also have decided
to let store return the unit value. If we would have made such other choice, we
need to ensure that the operational semantics and the Hoare rules are
consistent, otherwise the Hoare logic would not be sound.

**Note 2** Existentials/equalities like `∃ l, vret = l` might look a bit funny,
but they are needed because `vret` is a value and `l` is a location (i.e., a
natural number). In Coq, such an equality would be `vret = VRef l` where
`VRef : nat -> val` is the embedding of locations into values. Similarly,
`alloc v` would actually be `EAlloc (EVal v)`. On paper, we often omit these
embeddings for brevity, and even sometimes just omit the existentials/equalities
altogether `[ emp ] alloc v [ vret. vret ↦ v ]`.

While concise and intuitive, these Hoare rules are not too useful on their own
because:

1. They can only be used if the program contains exactly one instruction.
2. They can only be used if the pre- and postconditions do not involve any other
   memory locations.

To address issue (1), separation logic comes with the **frame rule**, which
allows one to extend the pre- and postcondition with a proposition `R` that
describes the remaining part of the heap:

```
    [ P ] e [ vret. Q ]
———————————————————————————
[ P ∗ R ] e [ vret. Q ∗ R ]
```

For example, using the frame rule we can derive the following Hoare triple:

```
[ l ↦ v ∗ k ↦ u ] !l [ wret. (wret = v) ∗ l ↦ v ∗ k ↦ u ]
```

The frame rule is the key feature of separation logic. It allows for so-called
**small-footprint** specifications that only mention the parts of the heap that
are relevant for the programs in question, instead of having to quantify over
the rest of the heap in every specification.

Another important rule is the **sequencing rule**:

```
[ P ] e₁ [ v. Q ]    v ∉ FV(Q)     [ Q ] e₂ [ vret. R ]
———————————————————————————————————————————————————————
          [ P ] e₁; e₂ [ vret. R ]
```

Since sequencing `e₁; e₂` ignores the return value of `e₁`, it is crucial that
`v` does not appear in the postcondition `Q` of `e₁`.

The rule for let-expressions generalizes the rule for sequencing by taking the
return value of `e₁` into account. In the second premise we consider the program
`e₂[x:=v]` for _any_ value `v` that satisfies the postcondition `Q` of `e₁`.

```
[ P ] e₁ [ v. Q ]      (∀ v. [ Q ] e₂[x:=v] [ vret. R ])
———————————————————————————————————————---——————————————
          [ P ] let x := e₁ in e₂ [ vret. R ]
```

The last rule we consider is the **rule of consequence**, which allows one to
weaken the pre- and postconditions according to logical entailment `⊢`:

```
P ⊢ P'   [ P' ] e [ vret. Q' ]    ∀ vret. Q' ⊢ Q
————————————————————————————————————————————————
          [ P ] e [ vret. Q ]
```

# Soundness of the separation-logic rules

Since we use a shallow embedding, the rules of separation logic are proved sound
by proving them as lemmas. This proceeds by unfolding the definition of Hoare
triples, and requires some reasoning about disjointness of heaps and the
operational semantics.

In Coq you will prove soundness of all the separation-logic rules. To
streamline our proofs, we will not define Hoare triples directly, but define
them in terms of **weakest preconditions**.
 
# Proving programs using separation logic

Let us reconsider the following program from previous week:

```coq
Definition foo (l : ref nat) (k : ref nat) : ref nat :=
  let x = !l in
  let y = x + 2 in
  k ← y
```

**Remark on notation** I am using Coq syntax to improve readability, but this
is not Coq; it is a language embedded in Coq. This language is untyped, but I
add types for clarity's sake.

In the previous lecture, we have given a specification of the above program
using the following Hoare triple:

```
[ l ↦ i ∗ k ↦ j ]  foo l k  [ l ↦ i ∗ k ↦(i + 2) ]
```

Using the rules of separation logic that we have shown above, you can try to
make a natural deduction-style derivation of the above Hoare triple. It is a
good exercise to have an attempt at doing this at home. While doing it, you will
notice that we always take the following steps:

- Use the rules for let and sequencing to decompose proving the whole program
  into Hoare triples for each individual primitive instruction, such as load,
  store, binary operator, etc.
- For each primitive instruction, use the rules for framing and consequence.
- Use the small-footprint rules for each primitive instruction.

Carrying out such proofs with the level of detail that is required in a natural
deduction-style derivation easily becomes tedious. For very small programs you
will already quickly run out of paper.

In this lecture, we discuss a more compact way of writing separation logic
proofs using **proof outlines**.

In the next lecture, we see how we can do such proofs in Coq using the Iris
Proof Mode.

# Proof outlines

We will not present proof outlines formally. Instead, we give the basic
intuition by means of examples.

**Note** The conventions that are used to write proof outlines vary a lot
throughout the literature. As such, we do not present the rules in a formal
manner. It is important that you write proof outlines in such a way that they
convey enough information to the reader: it should be clear how each individual
steps follows from the previous one.

The basic idea of proof outlines is very simple. You start by writing the
precondition at the top of the program, the postcondition at the bottom of the
program. In between any pair of primitive instructions, you then write the
intermediate verification conditions between `[ ... ]`:

```coq
Definition foo (l : ref nat) (k : ref nat) : unit :=
[ l ↦ i ∗ k ↦ j ]
  let x = !l in
[ l ↦ i ∗ k ↦ j ]                                     x := i
  let y = x + 2 in
[ l ↦ i ∗ k ↦ j ]                                     y := i + 2
  k ← y
[ l ↦ i ∗ k ↦(i + 2) ]
```

As we see in this example, each primitive instruction has its own precondition
above it, and its own postcondition below it. For each individual instruction we
should then prove the Hoare triple. To do that, we typically use a combination
of framing, the rule of consequence, and the small-footprint rules. We leave
implicit how framing and the rule of consequence are used exactly.

We keep track of the substitutions we have to perform on the right-hand side.
For example, after `x = !l` we know that `x` becomes `i` because we had `l ↦ i`
in the precondition.

# Linked data structures and representation predicates

Proof outlines become more interesting for functions that operate on linked
data structures. But before doing that, we will discuss how we can use
separation logic to specify programs that operate on such data structures.

The true power of separation logic comes into play when dealing with linked
data structures such as linked lists, linked trees, and so on. As an example,
let us consider the following program. (Recall that we use pseudo Coq to write
programs/ASTs in our embedded language with references. For clarity, we add
types while the embedded language with references we use in Coq is in fact
untyped.) 

```coq
Inductive llist A :=
  | lnil : llist A
  | lcons : A → ref (llist A) → llist A. 

Fixpoint sum_list (x : ref nat) (l : llist nat) : unit :=
  match l with
  | lnil => ()
  | lcons hd tl =>
     x ← !x + hd;
     let k := !tl in
     sum_list x k
  end.
```

Note we _do not_ use the usual purely functional lists as in Coq, but we use
linked lists as you have seen in imperative programming language like Java,
where there is a reference `ref` to each subsequent cell:

```
cons x₁ ⋅    cons x₂ ⋅    cons x₃ ⋅            nil
        │    ↑       │    ↑       │            ↑
        └────┘       └────┘       └───------───┘
```

Let us turn to specifying this program in separation logic. The first thing we
need to do is to describe how _mathematical lists_ are laid out in memory. For
that we use the following so called **representation predicate**:

```coq
Fixpoint is_list (l : llist A) (xs : list A) : sepProp :=
  match xs with
  | [] => l = lnil
  | hd :: xs' => ∃ tl l',
     (l = lcons hd tl) ∗
     (tl ↦ l') ∗
     is_list l' xs'
  end.
```

The list representation predicate `is_list l xs` expresses that linked list `l`
represents a mathematical (i.e., purely functional) list `xs`. Using this
predicate, the specification for the `sum_list` function is as follows:

```coq
[ x ↦ i ∗ is_list l js ]
  sum_list x l
[ x ↦ (i + Σ js) ∗ is_list l js ]
```

Here, `Σ : list nat → nat` is a mathematical (i.e., purely functional) function
that describes the sum of a mathematical list.

Let us consider some other programs and specifications.

## Disposing a linked list

Consider the following program that frees all nodes of a linked-list:

```coq
Fixpoint free_list (l : llist A) : unit :=
  match l with
  | lnil => ()
  | lcons hd tl =>
     let l' := free tl in
     free_list l'
  end.
```

The specification is as follows:

```coq
[ is_list l js ]
  free_list l
[ emp ]
```

We use `emp` in the postcondition to signify that the linked list has been
deallocated.

**Convention** We sometimes omit the `vret` binder in the postcondition if the
return value is not interesting (i.e., if the function returns unit).

## Cloning a list

Consider the following program that clones all nodes of a linked-list (i.e.,
makes a shallow copy):

```coq
Fixpoint copy_list (l : llist A) : llist A :=
  match l with
  | lnil => lnil
  | lcons hd tl =>
     let l' := copy_list (!tl) in
     let tl' := alloc l' in
     lcons hd tl'
  end.
```

The specification is as follows:

```coq
[ is_list l js ]
  copy_list l
[ k. is_list l js * is_list k js ]
```

We use `is_list` twice in the postcondition to signify that all references
in the linked list have been cloned.

Note that if the elements of the list would be pointers themselves, you may
be interested in making a deep copy. In that case, both the implementation and
the specification will be different.

# Proofs outlines for linked data structures

## Summing a list

Below you find a proof outline for the `sum_list` program:

```coq
Fixpoint sum_list (x : ref nat) (l : llist nat) : unit :=

[ x ↦ i ∗ is_list l js ]

  match l with
  | lnil => 
     [ x ↦ i ∗ is_list lnil js ]                      js := []
     [ x ↦ i ]                                        (* unfold is_list *)
       ()
     [ x ↦ i ]
     [ x ↦ i ∗ is_list lnil js ]                      (* fold is_list *)
  | lcons hd tl =>
     [ x ↦ i ∗ is_list (lcons hd tl) js ]             js := hd :: js2
     [ x ↦ i ∗ tl ↦ ltl ∗ is_list ltl js2 ]           (* unfold is_list *)
       x ← !x + hd
     [ x ↦ (i + hd) ∗ tl ↦ ltl ∗ is_list ltl js2 ]
       let k ← !tl in
     [ x ↦ (i + hd) ∗ tl ↦ ltl ∗ is_list ltl js2 ]    k := ltl
       sum_list x k
     [ x ↦ (i + hd + Σ js2) ∗ tl ↦ ltl ∗ is_list ltl js2 ]
     [ x ↦ (i + Σ js) ∗ is_list (lcons hd tl) js ]    (* fold is_list *)
  end.

[ x ↦ (i + Σ js) ∗ is_list l js ]

```

There are a couple of things that you should take into account:

- In the branches of the pattern match, we get to know that `l = lnil` or
  `l = lcons hd tl`.
- In these cases, we get more precise information about the `is_list`
  representation predicate. We typically proceed by unfolding the definition of
  the representation predicate. In particular, in the second branch of the
  pattern match, we turn `is_list (lcons hd tl) js` into
  `tl ↦ ltl ∗ is_list ltl js2` with `js := hd :: js2`.
- These proofs are by induction. When we get to the recursive call, we should
  make use of the specification of the function we try to prove.
- Towards the end of the proof outline, we typically fold the definition of
  the representation predicate. In particular, in the second branch of the
  pattern match,  we turn `tl ↦ ltl ∗ is_list ltl js2` into
  `is_list (lcons hd tl) js`.
- If we want to emphasize how we have used the rule of consequence, we typically
  put multiple `[ ... ]` conditions after each other. We often do this when we
  unfold or fold a representation predicate.

## Disposing a linked list

Below you find a proof outline for the program that frees all nodes of a
linked list:

```coq
Fixpoint free_list (l : llist A) : unit :=

[ is_list l js ]

  match l with
  | lnil =>
     [ is_list lnil js ]                              js := []
     [ emp ]                                          (* unfold is_list *)
       ()
     [ emp ]
  | lcons hd tl =>
     [ is_list (lcons hd tl) js ]                     js := hd :: js2
     [ tl ↦ ltl ∗ is_list ltl js2 ]                   (* unfold is_list *)
       let l' := free tl in
     [ is_list ltl js2 ]                              l' := ltl
       free_list l'
     [ emp ]
  end.

[ emp ]
```

The proof outline follows the same pattern we have seen before.

## Cloning a list

Below you find a proof outline for the program that clones all nodes of a
linked list (i.e., makes a shallow copy):

```coq
Fixpoint copy_list (l : llist A) : llist A :=

[ is_list l js ]

  match l with
  | lnil =>
     [ is_list lnil js ]                              js := []
       lnil
     [ is_list lnil js ∗ is_list lnil js ]            (* fold is_list *)
  | lcons hd tl =>
     [ is_list (lcons hd tl) js ]                     js := hd :: js2
     [ tl ↦ ltl ∗ is_list ltl js2 ]
       let l' := copy_list (!tl) in
     [ tl ↦ ltl ∗ is_list ltl js2 ∗ is_list l' js2 ]
       let tl' := alloc l' in
     [ tl ↦ ltl ∗ is_list ltl js2 ∗ tl' ↦ l' ∗ is_list l2' js2 ]
       lcons hd tl'
     [ tl ↦ ltl ∗ is_list ltl js2 ∗ tl' ↦ l' ∗ is_list l2' js2 ]
     [ is_list (lcons hd tl) js ∗ is_list (lcons hd tl') js ]
  end.

[ k. is_list l js * is_list k js ]
```

# Conclusion

In this lecture we have illustrated the basic principle of carrying out
separation logic proofs using proof outline. These proof outlines follow the
following structure:

- Use the rules for let and sequencing to decompose proving the whole program
  into Hoare triples for each individual primitive instruction, such as load,
  store, binary operator, etc.
- For each primitive instruction, use the rules for framing and consequence.
- Use the small-footprint rules for each primitive instruction.

For many programs, constructing a separation logic proof (by writing a proof
outline) is a very mechanical process. As such, it should come as no surprise
that one can implement separation logic in an automated or interactive tool. As
a matter of fact, this is an active research field, with many different tools
out there. For example:

- **Interactive tools**, like Iris, VST, or CFML  
  In these tools, one can construct separation logic proofs interactively using
  tactics. These tools are built on top of a proof assistant like Coq.
  **We will use Iris in the lecture of next week**
- **Semi-automated tools**, like Verifast, Viper, Bedrock, RefinedC, Diaframe  
  In these tools, one can guide the prover by writing down intermediate
  verification conditions (much like we have done in proof outlines). Some of
  these tools are standonline tools, others are built on top of a proof
  assistant like Coq.
- **Automated tools**, like those based on the Smallfoot family  
  These tools require users to write down pre- and postconditions, and can then
  fully automatically prove them. To make automation possible, these tools are
  usually restricted to a subset of separation logic.
- **Static analysis tools**, like Facebook's Infer  
  These tools do not require the user to write down pre- and postconditions,
  but infer them using a technique called "bi-abduction". To make this technique
  work, these tools are typically limited to properties like memory safety.

Apart from the availability of a wide range of tools, separation logic scales
to a wide variety of programming language features like concurrency,
higher-order functions, and GPU programs. It has been applied to many actual
programming languages like C, JavaScript, Java, Rust, ...

If you are interested, you may want to take a look at the following papers:

- Peter O'Hearn  
  Separation Logic  
  In Communications of the ACM, February 2019  
  https://cacm.acm.org/magazines/2019/2/234356-separation-logic/pdf  
  
  This is a nice review paper that gives an overview of the history, key ideas,
  and key applications of separation logic. The paper comes with an
  [appendix](https://dl.acm.org/ft_gateway.cfm?id=3211968&type=pdf&path=%2F3220000%2F3211968%2Fsupp%2Fp86%2Do%5Fhearn%2Dsupp%2Epdf&supp=1&dwn=1)
  that gives an overview of separation-logic based tools.

- Peter O'Hearn  
  A Primer on Separation Logic (and Automatic Program Verification and Analysis)  
  Marktoberdorf PhD summer school in 2011  
  http://www.cs.ucl.ac.uk/staff/p.ohearn/papers/Marktoberdorf11LectureNotes.pdf  
  
  These are course notes from a summer school. They focus on the application of
  separation logic for automatic verification. Note that the formalism in that
  paper is slightly different from ours. Notably, in this lecture we used a
  substitution based semantics, whereas they use a semantics with explicit
  stores for local variables.


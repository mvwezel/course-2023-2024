Lecture 9: Separation logic (propositions)
==========================================

In the previous lectures we considered how to prove properties of whole
programming languages (in particular, type safety and termination). In this
lecture, we consider how to prove properties of individual programs in
programming languages embedded in Coq. Before we start, we first recap how to
specify programs written in Coq's purely functional programming language.

# Specifications of purely functional programs

Consider the following properties (some of these properties you have proved in
Coq in week 2):

```coq
∀ x y. (nat_eqb x y = true) ↔ (x = y)
∀ x. (evenb x = true)       ↔ (∃ k, x = 2 * k)
∀ x. (primeb x = true)      ↔ 1 < x ∧ ∀ y. (y | x) → y = x
```

On the LHS we have a program (e.g., `nat_eqb`, `evenb`, or `primeb`), and on the
RHS we have a specification in mathematics. In other words, on the LHS we have
something we can **run** and on the RHS we have a statement in mathematics that
we can **reason** about.

The nice property about these specification is that they fully specify the
behavior of a program, which has a couple of advantages:

- Once we have proved the specification, we never have to look at the code of
  the program again.
- We can freely change the implementation to something more efficient, provided
  our proofs only rely on the specification.

**Exercise** Consider a sorting function `sort : list nat → list nat`. Give a
specification in the form as those stated above.

```coq
∀ l k. sort l = k ↔ Permutation l k ∧ Sorted k
```

In Coq, one would define `Permutation` and `Sorted` using (a combination of)
logical quantifiers and inductive definitions. These mathematical definitions
concisely describe **what** is the result of a sorting function without saying
**how** it should actually perform its job.

The crucial part of the above specification is that it holds for _any_ sorting
algorithm, whether it is bubble sort, insertion sort, quick sort, merge sort,
etc. As a result, assuming we have a function `f` that uses `sort`, then if you
switch to a different implementation of `sort`, you do not have to modify the
proof of `f`. This kind of encapsulation of proofs by proving strong
specifications is crucial to scale verification methods to larger projects.

**Exercise** If we have `sort : list A → list A` where `(A,≤)` is an arbitrary
preorder (i.e. `≤` is a reflexive and transitive relation on `A`), does the
specification hold too?

If `≤` is not anti-symmetric, which would happen if we sort say the first
projection of a tuple, the result of sorting is not necessarily unique. In that
case we get at most the left-to-right direction of the `↔`:

```
∀ l k. sort l = k → Permutation l k ∧ Sorted k
```

This direction is however adequate for most of the cases. If it is not, one can
give a stronger specification that uniquely describes the result of a stable
sorting algorithm.

# Specifications of imperative programs

The above approach of programming and specifying in Coq works very well for
functional programs, but for imperative programs in our embedded language things
become more difficult.

Let us consider the following imperative program:

```coq
Definition foo (l : ref nat) (k : ref nat) : ref nat :=
  let x = !l in
  let y = x + 2 in
  k ← y
```

**Remark on notation** I am using a Coq syntax to improve readability, but this
is not Coq; it is a language embedded in Coq. This language is untyped, but I
add types in the style of week 6 for clarity's sake.

**Exercise** Think for a minute how you would specify this program in terms
of the big-step operational semantics:

```
_, _ ⇓ _, _ : expr → heap → val → heap → Prop
```

**First attempt**

```
∀ l k i j.
  (foo l k), [ l := i, k := j ]  ⇓  k,  [ l := i, k := i + 2 ]
```

This specification has two issues:

- It is _wrong_, we need the additional premise that `l` and `k` are disjoint,
  locations, i.e., `l ≠ k`.
- It is _too weak_, it will only hold if we start in a heap containing exactly
  the locations `l` and `k`, but we may want to call the program `foo` in any
  heap containing the locations `l` and `k`.

**Second attempt**

The specification below fixes both issues. We add a premise `l ≠ k` and we
quantify over all heaps `h` to make sure the specification is usable in any
heap.

```
∀ h l k i j,
  h(l) = i →
  h(k) = j →
  l ≠ k → 
  foo l k, h ⇓ k,  h[ k := i + 2 ]
```

This specification fixes both issues. There are still some problems:

- Once we have programs with more pointers, we will have a quadratic number of
  disjoint conditions (such as `l ≠ k`).
- It is not very nice to write down, we have to state it explicitly in terms of
  the operational semantics, and we have to quantify explicitly over all
  possible heaps `h`.
- It will be very hard to prove, since we have to reason explicitly in terms of
  the operational semantics.

# Outline/key points of the lecture

First-order logic (or variants of higher-order logic like Coq) are good for
specifying and reasoning and purely functional programs.

If we want to reason about imperative programs that manipulate pointers, we need
a better suited logic. Similar to programming language design, where it is often
desirable to use a high-level language or a Domain Specific Programming Language
(DSL), if we want to do proofs about programs in a certain domain (like programs
that manipulate state), it is desirable to have a **Domain Specific Logic**.

A powerful domain specific logic for mutable state is **separation logic**
(invented by Peter O'Hearn, John Reynolds, and Hongseok Yang in 2001). In this
lecture we will:

- Discuss how to specify and prove programs in separation logic.
- Describe how to embed separation logic into a proof assistant like Coq.
- Go a bit more into detail about the proof rules of separation logic.

# Separation logic propositions

**Separation logic** is an extension of **Hoare logic** specifically tailored
for reasoning about programs that manipulate pointers. Specifications are
**Hoare triples**, which are of the following form:

```
[ P ] e [ v. Q ]
```

Intuitively, Hoare triples express the following:

- If the **precondition** `P` holds beforehand, then,
- Execution of the program `e` is guaranteed to terminate and result in a
  value `v`,
- Such that the **postcondition** `Q` holds.

**Remark on notation** In the above notation, `v` is a binder for the return
value. If we do not care about the return value, we typically omit the binder
and write just `[ P ] e [ Q ]`. So, formally `[ P ] e [ Q ]` is syntactic sugar
for `[ P ] e [ v. Q ]`, where `v` is an unused free variable in `Q`.

The precondition `P` and `Q` are propositions of separation logic, whose syntax
is as follows:

```
P, Q ∈ sepProp ::=
  | True       (* truth *)
  | False      (* falsum *)
  | t₁ = t₂    (* equality *)
  | P → Q      (* implication *)
  | P ∧ Q      (* conjunction *)
  | P ∨ Q      (* disjunction *)
  | ∀ x:A. P   (* universal quantification *)
  | ∃ x:A. P   (* existential quantification *)
  | l ↦ v      (* NEW: points-to connective *)
  | P ∗ Q      (* NEW: separating conjunction *)
  | emp        (* NEW: the empty heap connective *)
```

The syntax of separation logic propositions is much like that of first-order
logic, we have the usual connectives for truth, implication, quantification,
etc. However, there are also a couple of new connectives.

While the syntax of separation logic appears similar to first-order logic, the
**semantics of propositions** of separation logic is very different. In
first-order logic, propositions describe **truth** or **knowledge**, i.e., if a
proposition holds, it will hold forever, i.e., persistently. Propositions of
separation logic describe **ownership**, they describe which parts of the heap
we own. Let us take a look at the intuitive semantics of the new connectives to
get a better idea of that:

- **The points-to connective** `l ↦ v`.
  This connective describes that the heap contains _exactly one_ location
  `l` with value `v` and expresses that we we have unique ownership of that
  location in the heap.
- **The separating conjunction** `P ∗ Q`.
  This connective describes that the heap can be split into two _disjoint_
  parts, so that the first satisfies `P`, and the second satisfies `Q`.
- **The empty heap connective** `emp`.
  This connective describes that the heap is empty, i.e., we do not own any
  locations.

Using these connectives, one can give very precise descriptions of memory
footprints, for example:

- `l₁ ↦ 6 ∗ l₂ ↦ 8`
- `∃ v. l₁ ↦ v ∗ l₂ ↦ (2 * v)`

The first example describes a heap that consists of **exactly** two locations
`l₁` and `l₂` that respectively contain the values 6 and 8. Since the separation
conjunction `P ∗ Q` ensures that the parts of the heap described by `P` and
`Q` are disjoint, we know that `l₁` and `l₂` are different (i.e., they do not
alias). This makes separating conjunction very different from conjunction
`P ∧ Q`, which says that `P` and `Q` both hold for the same heap. 

The second example describes the set of heaps that contain two different
locations `l₁` and `l₂`, so that the value of `l₂` is twice that of `l₁`.

Let us take a look at two other separation logic propositions:

- `l₁ ↦ 6 ∗ l₂ ↦ 8 ∗ True`
- `∃ x, l₁ ↦ 6 ∧ l₂ ↦ x`

The first example describes a heap that consists of **at least** two locations
`l₁` and `l₂`, which respectively contain the values 6 and 8. The `∗ True`
describes that the heap may contain an arbitrary number of other locations.

The second example uses the ordinary conjunction `P ∧ Q`, which states that
`P` and `Q` should both hold. As such this proposition describes a heap that
contains exactly one location `l₁`, which is equal to `l₂`, and contains the
value 6.

# Specifying programs using separation logic

Making use of separating conjunction, we can give concise specifications of
programs that manipulate pointers. For example, take the following Hoare logic
specification of the `foo` function:

```coq
Definition foo (l : ref nat) (k : ref nat) : unit :=
  let x = !l in
  let y = x + 2 in
  k ← y
```

Its specification is:

```
∀ l k i j.
  [ l ↦ i ∗ k ↦ j ]  foo l k  [ vret. vret = k ∗ l ↦ i ∗ k ↦ (i + 2) ]
```

This specification expresses that `foo` is indeed assigning the value of `l`
increased by 2 to `k`. Crucially, compared to the ad-hoc specification it fixes
all of the issues we have pointed out:

- It is implicitly captured by the `∗` that the locations `l` and `k` are
  disjoint. Note that this will scale to any number of locations, e.g.,
  `l₁ ↦ v₁ ∗ l₂ ↦ v₂ ∗ ... lₙ ↦ vₙ` implicitly describes that `lᵢ ≠ lₖ` for
  any `i ≠ k`.
- We do not need to explicitly talk about the operational semantics.
- We do we need to explicitly quantify over some heap `h`. Instead, we only need
  to refer to the parts of the heap that the program actually accesses. This is
  called a **small footprint specification**.

In order to make the last point a more precise, let us make the intuitive
semantics of Hoare triples a bit more formal (we will the precise definition
in week 10). For this, it is crucial to keep into mind that separation logic
propositions `P` and `Q` describe heaps.

```
[ P ] e [ v. Q ]
```

Hoare triples express the following:

- Given any initial heap `h`,
- If the precondition `P h` holds beforehand, then,
- Given any other heap `hf` called the _frame_ that disjoint to `h` (notation
  `h ## hf`),
- Execution of the program `e` in `h ∪ hf` in is guaranteed to terminate with
  a value `v` in a final heap `h' ∪ hf` with `h' ## hf`,
- Such that the postcondition `Q h'` holds.

Let us consider another program:

```coq
Definition bar (x : nat) : ref nat :=
  let l = alloc(x) in
  l ← !l + !l;
  l
```

**Exercise** What would be the separation logic spec of this program?

```
∀ n.
  [ emp ]  bar x  [ vret. vret ↦ (x + x) ]
```

# The semantics of separation logic

## Shallow embedding versus deep embedding

In week 4, we have seen how we could model a language by:

- Defining a _syntax_ in terms of an inductive type,
- Giving an operational semantics for this syntax.

The approach of first defining a syntax is called a **deep embedding**.

In this lecture, we proceed differently. We are not going to define an explicit
syntax for separation logic. Instead, we are going to define the propositions
of our separation logic directly by their **semantic interpretation**. The
operators will then be a number of combinators in the host language (Coq). This
is called a **shallow embedding**.

## Semantics of separation logic propositions

The first question that we need to answer is: what will be the semantic
interpretation of the connectives of separation logic? As we have just seen,
the propositions of separation logic describe sets of heaps. The natural way to
describe the propositions `sepProp` of separation logic is by considering them
to be predicates over heaps:

```coq
Definition sepProp := heap → Prop
```

Recall, in Coq predicates are functions to `Prop`. So to define predicates, we
should use a λ-abstraction. 

```
(l ↦ v) := λ h, h = {[ l := v ]}
(P ∗ Q) := λ h, ∃ h1 h2, h1 ## h2  ∧  h = h1 ∪ h2  ∧  P h1  ∧ Q h2
    emp := λ h, h = ∅
(x = y) := λ h, h = ∅ ∧ (x = y)
```

The above definitions make the intuitive semantics formal:

- The points-to connective `l ↦ v` describes the heaps `h` that are equal to
  `{[ l := v ]}`.
- The separating conjunction `P ∗ Q` describes the heaps `h` that can be split
  into disjoint parts `h1` and `h2`, such that `P` holds in `h1` and `Q` holds
  in `h2`.
- The empty heap connective `emp` describes the heaps `h` that are equal to
  `∅`, i.e., it describes the empty heap.

**Note** The semantics of the equality `x = y` implicitly captures that the
heap should be empty (it contains `m = ∅`). An alternative semantics, that is
often used in the literature, is `(x = y) := λ h, (x = y)`. Our version has the
upshot that it can easily be used in specifications, e.g., `(x = y) ∗ (l ↦ x)`
describes heaps that contain exactly one cell. With the alternative semantics,
you need to write `(x = y ∧ emp) ∗ (l ↦ x)` or `(x = y) ∧ (l ↦ x)`.

The semantics of the other connectives is straightforward, we lift the
connectives on `Prop` to those on `sepProp`.

```
      True := λ h. True
     False := λ h. False
   (P → Q) := λ h, P h → Q h
   (P ∧ Q) := λ h, P h ∧ Q h
   (P ∨ Q) := λ h, P h ∨ Q h
(∀ x:A, P) := λ h, ∀ x:A, P h
(∃ x:A, P) := λ h, ∃ x:A, P h
```

**Important** Keep in mind that the logical connectives on the left-hand side
are those of the shallowly embedded object logic (i.e., separation logic), and
the ones on the right hand side are those of our meta logic (i.e., Coq). On
paper we typically overload the syntax, but when mechanizing separation logic
in Coq we will be very explicit about the difference between the two.


# Proof rules of separation logic

Separation logic would not be a logic if there were no proof rules. To express
the proof rules of separation logic, we need a notion of entailment `P ⊢ Q`,
which says that `Q` follows from `P`:

```
P ⊢ Q := ∀ h, P h → Q h
```

The entailment `P ⊢ Q` expresses that `P` implies `Q` for any heap `h`.

The quintessential proof rules of separation logic are the following:

- **Monotonicity**: If `P1 ⊢ P2` and `Q1 ⊢ Q2` then `P1 ∗ Q1 ⊢ P2 ∗ Q2`.
- **Associativity**: `P ∗ (Q ∗ R) ⊢ (P ∗ Q) ∗ R`
- **Commutativity**: `P ∗ Q ⊢ Q ∗ P`
- **Identities**: `P ∗ emp ⊢ P` and `P ⊢ P ∗ emp`

In addition, separation logic enjoys the usual rules of first-order logic for
introduction and elimination of the ordinary logical connectives. For example:

```
 R ⊢ P   R ⊢ Q
---------------∧I        -----------∧El      -----------∧Er
  R ⊢ P ∧ Q               P ∧ Q ⊢ P           P ∧ Q ⊢ Q
```

# Conclusion

In the last part of this lecture, we gave a very brief teaser to the rules for
proving separation logic entailments. In the next two lectures, we take a look
at the semantics of Hoare-triples and separation-logic based proof rules. We
study how they can be used to verify programs, including programs that make use
of linked data structures (like linked lists, linked trees, etc.), and how you
can write down separation logic proofs.

In the Coq exercises of this week, you will work on the shallow embedding of
separation logic propositions in Coq and verify a number of proof rules for
entailments. We consider two kinds of proof rules:

- **Primitive rules** that are proved by unfolding the definitions of the
  separation-logic connectives.
- **Derived rules** that are proved by composing primitive proof rules.

